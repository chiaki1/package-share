import {
  ExtensionConfig,
  ExtensionCustomizeType,
  ExtensionDataConfig,
  UiComponent,
} from '../../types';
import React, { useMemo } from 'react';
import _ from 'lodash';
import { UiExtension } from './UiExtension';

export const withExtensionHook = (
  OrgComponent: UiComponent<any>,
  extensionConfig: ExtensionConfig
): UiComponent<any> => {
  return React.memo((props) => {
    const extensionData = useMemo(() => {
      const _extensionData = props.extensionData || {};
      if (
        // extensionDataConfig được pass vào từ url-rewrite, chính là data của extension
        !props.hasOwnProperty('extensionDataConfig') ||
        !Array.isArray(extensionConfig.structure) ||
        extensionConfig.customizeType !== ExtensionCustomizeType.HOOK
      ) {
        return _extensionData;
      }
      _extensionData['hookCpt'] = {};

      _.forEach(extensionConfig.structure, (s: any) => {
        const mayBeExtensionDataConfig: ExtensionDataConfig | undefined =
          _.find(
            props.extensionDataConfig.extensionDataConfigs,
            (c) => c && s && c['forHookId'] === s['hookId']
          );

        if (mayBeExtensionDataConfig) {
          _extensionData['hookCpt'][s['hookId']] = (
            <UiExtension
              uiId={s.uiId}
              extensionDataConfig={mayBeExtensionDataConfig}
            />
          );
        } else {
          // logger.warn('Could not found hook config for id ' + s['hookId']);
        }
      });

      return _extensionData;
    }, [extensionConfig.uiId]);

    return <OrgComponent {...props} extensionData={extensionData} />;
  });
};
