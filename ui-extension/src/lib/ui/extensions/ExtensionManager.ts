import { Map } from 'immutable';
import _ from 'lodash';
import { ExtensionConfig, UiComponent, UiHOC } from '../../types';
import { HOCManager } from '../hoc';

export class ExtensionManager {
  static COMPONENTS: Map<string, ExtensionConfig> = Map<any, any>();
  protected static INSTANCE: any;
  static RESOLVED_COMPONENTS: Map<string, UiComponent<any>> = Map<any, any>();

  static getInstance(): ExtensionManager {
    if (typeof ExtensionManager.INSTANCE === 'undefined') {
      console.info(
        '______________________________________ Create ExtensionManager ______________________________________'
      );
      ExtensionManager.INSTANCE = new ExtensionManager();
    }

    return ExtensionManager.INSTANCE;
  }

  config(configs: ExtensionConfig | ExtensionConfig[]): ExtensionManager {
    if (!Array.isArray(configs)) {
      configs = [configs];
    }
    _.forEach(configs, (c: ExtensionConfig) => {
      // merge data if have more than one config
      if (ExtensionManager.COMPONENTS.has(c.uiId)) {
        const sortedToMerge: any[] = _.chain([
          c,
          ExtensionManager.COMPONENTS.get(c.uiId),
        ])
          .sortBy((c: any) => {
            if (c.hasOwnProperty('priority')) {
              if (typeof c.priority === 'number') {
                return c.priority;
              } else if (typeof c.priority === 'function') {
                return c.priority();
              }
            }

            if (c.hasOwnProperty('priorityFn')) {
              if (typeof c.priorityFn === 'number') {
                return c.priorityFn;
              } else if (typeof c.priorityFn === 'function') {
                return c.priorityFn();
              }
            }

            return 0;
          })
          .reverse()
          .value();

        const hocs: any[] = [
          ...(c.hoc ?? []),
          // ...(ExtensionManager.COMPONENTS.get(c.uiId)!.hoc ?? []),
        ];
        // @ts-ignore
        c = _.merge(...sortedToMerge);
        c.hoc = hocs;
      }

      /*
       * resolve HOC data
       */
      c.hoc = HOCManager.getInstance().resolveExtensionHOCs(c.hoc);

      ExtensionManager.COMPONENTS = ExtensionManager.COMPONENTS.set(c.uiId, c);
    });

    return this;
  }

  cptCfg(uiId: string): ExtensionConfig | undefined {
    return ExtensionManager.COMPONENTS.get(uiId);
  }

  getHoc(hocId: string): UiHOC | any {
    return null;
  }
}
