import { HttpLink } from 'apollo-link-http';

export const ApiLink = (uri: string) => {
  return new HttpLink({
    uri,
    credentials: 'omit', // 'same-origin'
    fetch,
  });
};
