import {
  InMemoryCache,
  IntrospectionFragmentMatcher,
} from 'apollo-cache-inmemory';
import { cacheKeyFromType } from './apolloCache';
import result from '../graphql/generated/_generated-fragment-types';

/**
 * To improve initial load time, create an apollo cache object as soon as
 * this module is executed, since it doesn't depend on any component props.
 * The tradeoff is that we may be creating an instance we don't end up needing.
 *
 * @see https://www.npmjs.com/package/apollo-cache-inmemory
 */
export const preInstantiatedCache = new InMemoryCache({
  dataIdFromObject: cacheKeyFromType,
  fragmentMatcher: new IntrospectionFragmentMatcher({
    introspectionQueryResultData: result,
  }),
});
