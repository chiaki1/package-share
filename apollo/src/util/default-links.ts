import { ApolloLink } from 'apollo-link';
import {
  ApiLink,
  AuthLink,
  ErrorLink,
  RetryLinkWithDefaultBehavior,
  StoreLink,
} from './links';

export const DefaultLink = (
  api: string,
  getStoreCode: () => Promise<string>,
  getToken: () => Promise<string>
): ApolloLink => {
  // TODO: should split ApiLink with 2 case Batched and non-Batched

  return ApolloLink.from([
    RetryLinkWithDefaultBehavior(),
    StoreLink(getStoreCode),
    AuthLink(getToken),
    ErrorLink,
    ApiLink(api),
  ]);
};
