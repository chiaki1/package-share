import { createUiHOC } from '@vjcspy/ui-extension';
import { useCmsPage } from '../hook/useCmsPage';

export const withCmsPage = createUiHOC((props) => {
  return useCmsPage();
}, 'withCmsPage');
