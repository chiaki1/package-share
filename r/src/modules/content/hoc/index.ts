import { withHomeActions } from './withHomeActions';
import { withHomeData } from './withHomeData';
import { withCmsPage } from './withCmsPage';
import { withNavigator } from './withNavigator';
import { withContentAddressActions } from './withContentAddressActions';
import { withContentAddressData } from './withContentAddressData';
import { withAppNavigator } from './withAppNavigator';
import { withHomeBrandData } from './withHomeBrandData';

export const R_CONTENT_HOCS = [
  withHomeActions,
  withHomeData,
  withCmsPage,
  withNavigator,
  withContentAddressActions,
  withContentAddressData,
  withAppNavigator,
  withHomeBrandData,
];
