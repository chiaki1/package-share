import { createUiHOC } from '@vjcspy/ui-extension';
import { useContentAddressData } from '../hook/useContentAddressData';

export const withContentAddressData = createUiHOC(
  () => useContentAddressData(),
  'withContentAddressData'
);
