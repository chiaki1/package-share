import { createUiHOC } from '@vjcspy/ui-extension';
import { useContentAddressActions } from '../hook/useContentAddressActions';

export const withContentAddressActions = createUiHOC(
  () => useContentAddressActions(),
  'withContentAddressActions'
);
