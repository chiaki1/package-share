import { createUiHOC } from '@vjcspy/ui-extension';
import { useHomeBrandData } from '../hook/useHomeBrandData';

export const withHomeBrandData = createUiHOC(
  () => useHomeBrandData(),
  'withHomeBrandData'
);
