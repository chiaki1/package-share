import { createUiHOC } from '@vjcspy/ui-extension';
import { useHomeData } from '../hook/useHomeData';

export const withHomeData = createUiHOC(() => useHomeData(), 'withHomeData');
