import { createUiHOC } from '@vjcspy/ui-extension';
import { useNavigator } from '../hook/useNavigator';

export const withAppNavigator = createUiHOC(
  () => useNavigator('APP_NAVIGATOR'),
  'withAppNavigator'
);
