import { createUiHOC } from '@vjcspy/ui-extension';
import { useNavigator } from '../hook/useNavigator';

export const withNavigator = createUiHOC(() => useNavigator(), 'withNavigator');
