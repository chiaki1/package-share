import { createUiHOC } from '@vjcspy/ui-extension';
import { useHomeActions } from '../hook/useHomeActions';

export const withHomeActions = createUiHOC(() => {
  return useHomeActions();
}, 'withHomeActions');
