import { ContentState } from './state';

export const selectBanners = (state: { content: ContentState }) =>
  state.content.banners;

export const selectHomeBrand = (state: { content: ContentState }) =>
  state.content.homeBrand;

export const selectBrandCampaign = (state: { content: ContentState }) =>
  state.content.brandCampaign;

export const selectBestSeller = (state: { content: ContentState }) =>
  state.content.bestSeller;
