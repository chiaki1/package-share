import { createBuilderCallback } from '../../../../util';
import { ContentState } from '../state';
import { getContentAddressDataAfterAction } from './actions';

export const addressBuilderCallback = createBuilderCallback<ContentState>(
  (builder) => {
    builder.addCase(getContentAddressDataAfterAction, (state) => {
      state.isLoadedAddressData = true;
    });
  }
);
