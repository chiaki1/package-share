import { ContentState } from '../state';

export const selectIsLoadedContentAddressData = (state: {
  content: ContentState;
}) => state.content.isLoadedAddressData;
