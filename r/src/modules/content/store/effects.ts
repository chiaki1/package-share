import { R_CONTENT_BANNER_EFFECTS } from './banner';
import { R_CONTENT_ADDRESS_EFFECTS } from './address';

export const R_CONTENT_EFFECTS = [
  ...R_CONTENT_BANNER_EFFECTS,
  ...R_CONTENT_ADDRESS_EFFECTS,
];
