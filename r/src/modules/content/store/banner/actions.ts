import { createAction } from '../../../../util';

const PREFIX = 'CONTENT';

const GET_BANNER = 'GET_BANNER';
export const getBannerAction = createAction(GET_BANNER, PREFIX);

const GET_BANNER_AFTER = 'GET_BANNER_AFTER';
export const getBannerAfterAction = createAction<{
  banners: any[];
}>(GET_BANNER_AFTER, PREFIX);

const GET_BANNER_ERROR = 'GET_BANNER_ERROR';
export const getBannerErrorAction = createAction<{
  error: Error;
}>(GET_BANNER_ERROR, PREFIX);

const GOT_CMS_PAGE = 'GET_CMS_PAGE';
export const gotCmsPageData = createAction<{
  cmsPage: any;
}>(GOT_CMS_PAGE, PREFIX);

const GET_HOME_BRAND = 'GET_HOME_BRAND';
export const getHomeBrand = createAction(GET_HOME_BRAND, PREFIX);

const GET_HOME_BRAND_AFTER = 'GET_HOME_BRAND_AFTER';
export const getHomeBrandAfter = createAction<{
  payload: any;
}>(GET_HOME_BRAND_AFTER, PREFIX);

const GET_HOME_BRAND_ERROR = 'GET_HOME_BRAND_ERROR';
export const getHomeBrandError = createAction<{
  error: Error;
}>(GET_HOME_BRAND_ERROR, PREFIX);

const GET_BEST_SELLER = 'GET_BEST_SELLER';
export const getBestSeller = createAction(GET_BEST_SELLER, PREFIX);

const GET_BEST_SELLER_AFTER = 'GET_BEST_SELLER_AFTER';
export const getBestSellerAfter = createAction<{
  data: any[];
}>(GET_BEST_SELLER_AFTER, PREFIX);

const GET_BEST_SELLER_ERROR = 'GET_BEST_SELLER_ERROR';
export const getBestSellerError = createAction<{
  error: Error;
}>(GET_BEST_SELLER_ERROR, PREFIX);
