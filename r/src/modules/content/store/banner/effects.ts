import {
  getBannerAction,
  getBannerAfterAction,
  getBannerErrorAction,
  getBestSeller,
  getBestSellerAfter,
  getBestSellerError,
  getHomeBrand,
  getHomeBrandAfter,
  getHomeBrandError,
} from './actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { of } from 'rxjs';
import {
  createEffect,
  graphqlFetch,
  ofType,
  proxyFetch,
} from '../../../../util';

const whenGetBanner$ = createEffect((action$) =>
  action$.pipe(
    ofType(getBannerAction),
    switchMap(() =>
      fromPromise(proxyFetch({ type: 'get-enable-home-banner' })).pipe(
        map((banners) => {
          // check banners server valid
          return getBannerAfterAction({ banners });
        }),
        catchError((error) => of(getBannerErrorAction({ error })))
      )
    )
  )
);

const whenGetHomeBrand$ = createEffect((action$) =>
  action$.pipe(
    ofType(getHomeBrand),
    switchMap(() =>
      fromPromise(proxyFetch({ type: 'get-home-brand' })).pipe(
        map((homeBrand) => {
          return getHomeBrandAfter(homeBrand);
        }),
        catchError((error) => of(getHomeBrandError({ error })))
      )
    )
  )
);

const whenGetBestSeller$ = createEffect((action$) =>
  action$.pipe(
    ofType(getBestSeller),
    switchMap(() =>
      fromPromise(
        graphqlFetch({
          query: `query bestSellerProduct{
              bestSellerProduct(
                pageSize: 2
                currentPage: 1
              ) {
                items{
                  url_key
                  image{
                    url
                    label
                  }
                }
              }
            }`,
        })
      ).pipe(
        map((data) => {
          return getBestSellerAfter({ data });
        }),
        catchError((error) => of(getBestSellerError({ error })))
      )
    )
  )
);

export const R_CONTENT_BANNER_EFFECTS = [
  whenGetBanner$,
  whenGetHomeBrand$,
  whenGetBestSeller$,
];
