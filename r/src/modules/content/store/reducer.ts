import { createReducer } from '@reduxjs/toolkit';
import { ContentStateFactory } from './state';
import { addressBuilderCallback } from './address';

export const contentReducer = createReducer(
  ContentStateFactory(),
  (builder) => {
    addressBuilderCallback(builder);
  }
);
