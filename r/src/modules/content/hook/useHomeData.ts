import {
  useGetBestSellerLazyQuery,
  useGetCategoryListForHomeLazyQuery,
  useGetChiakiConfigQuery,
  useGetHomeBrandDealsLazyQuery,
  useGetHomeCampaignLazyQuery,
} from '@vjcspy/apollo';
import { useEffect, useState } from 'react';
import _ from 'lodash';
import { useDomainContext } from '../../domain';
import { useStoreContext } from '../../store';

export const useHomeData = () => {
  const domainContextValue = useDomainContext();
  const storeContextValue = useStoreContext();
  const [listCategory, setListCategory] = useState<any>();
  const [dataCategory, setDataCategory] = useState();
  const [banners, setBanners] = useState<any[]>([]);
  const [brandDeals, setBrandDeals] = useState<any[]>([]);
  const [bestSellerProduct, setBestSellerProduct] = useState<any[]>([]);

  const { data, error } = useGetChiakiConfigQuery({
    variables: {
      storeId: storeContextValue.storeData!.storeId,
      userId: domainContextValue.domainData.shopOwnerId,
      key: 'CONFIG_HOME_DATA',
    },
    fetchPolicy: 'network-only',
  });

  useEffect(() => {
    if (error) {
      console.error(
        'could not get useGetChiakiConfigQuery with key: CONFIG_HOME_DATA'
      );
    }
    if (data) {
      if (_.isArray(data?.chiakiConfig)) {
        const jsonData: any = _.first(data?.chiakiConfig);
        try {
          if (jsonData) {
            setListCategory(JSON.parse(jsonData.value));
          }
        } catch (e) {
          console.error(
            'Could not parse APP_NAVIGATOR_CFG data from chiaki config'
          );
        }
      } else {
        console.warn('Wrong data format APP_NAVIGATOR_CFG from chiaki config');
      }
    }
  }, [data, error]);

  const [
    getCategoryListData,
    getCategoryListDataRes,
  ] = useGetCategoryListForHomeLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (listCategory?.homePageCategoryList) {
      getCategoryListData({
        variables: {
          filters: {
            ids: {
              in: _.map(listCategory.homePageCategoryList, (t) => t + ''),
            },
          },
        },
      });
    }
  }, [listCategory]);

  useEffect(() => {
    if (getCategoryListDataRes.error) {
    }

    if (getCategoryListDataRes.data?.categoryList) {
      // @ts-ignore
      setDataCategory(getCategoryListDataRes.data!.categoryList);
    }
  }, [getCategoryListDataRes.data, getCategoryListDataRes.error]);

  const [getBannerQuery, getBannerRes] = useGetHomeCampaignLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    getBannerQuery();
  }, []);

  useEffect(() => {
    if (getBannerRes.error) {
      console.log('get banner data error', getBannerRes.error);
    }

    if (getBannerRes.data?.brandCampaign) {
      setBanners(getBannerRes!.data!.brandCampaign);
    }
  }, [getBannerRes.data, getBannerRes.error]);

  const [
    getHomeBrandDealsQuery,
    getHomeBrandDealsRes,
  ] = useGetHomeBrandDealsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    getHomeBrandDealsQuery();
  }, []);

  useEffect(() => {
    if (getHomeBrandDealsRes.error) {
      console.log('get banner data error', getBannerRes.error);
    }

    if (getHomeBrandDealsRes.data?.brandCampaign) {
      setBrandDeals(getHomeBrandDealsRes!.data!.brandCampaign);
    }
  }, [getHomeBrandDealsRes.data, getHomeBrandDealsRes.error]);

  const [getBestSellerQuery, getBestSellerRes] = useGetBestSellerLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    getBestSellerQuery();
  }, []);

  useEffect(() => {
    if (getBestSellerRes.error) {
      console.log('get banner data error', getBannerRes.error);
    }

    if (getBestSellerRes.data?.bestSellerProduct?.items) {
      setBestSellerProduct(getBestSellerRes!.data!.bestSellerProduct?.items);
    }
  }, [getBestSellerRes.data, getBestSellerRes.error]);

  return {
    state: {
      banners,
      dataCategory,
      listCategory,
      brandDeals,
      bestSellerProduct,
    },
  };
};
