import { useDomainContext } from '../../domain';
import { useStoreContext } from '../../store';
import { useGetChiakiConfigQuery } from '@vjcspy/apollo';
import { useEffect, useState } from 'react';
import _ from 'lodash';

export const useNavigator = (key = 'navigator') => {
  const domainContextValue = useDomainContext();
  const storeContextValue = useStoreContext();
  const [navigator, setNavigator] = useState();

  const { data, error } = useGetChiakiConfigQuery({
    variables: {
      storeId: storeContextValue.storeData!.storeId,
      userId: domainContextValue.domainData.shopOwnerId,
      key,
    },
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (error) {
      console.error('Could not load navigator data');
    }
    if (data) {
      if (_.isArray(data?.chiakiConfig)) {
        const navigatorData = _.first(data?.chiakiConfig);
        try {
          if (navigatorData) {
            setNavigator(JSON.parse(navigatorData.value));
          }
        } catch (e) {
          console.error('Could not parse navigator data from chiaki config');
        }
      } else {
        console.error('Wrong data format navigator from chiaki config');
      }
    }
  }, [error, data]);

  return { state: { navigator } };
};
