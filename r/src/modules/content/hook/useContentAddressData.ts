import { useSelector } from 'react-redux';
import { selectIsLoadedContentAddressData } from '../store/address/selector';
import { useMemo } from 'react';
import { Registry } from '@vjcspy/chitility';
import { ContentConstant } from '../util';
import _ from 'lodash';

export const useContentAddressData = () => {
  const isLoadedContentAdd = useSelector(selectIsLoadedContentAddressData);

  const addressData = useMemo(() => {
    if (isLoadedContentAdd) {
      return Registry.getInstance().registry(ContentConstant.ADDRESS_DATA_KEY);
    }

    return null;
  }, [isLoadedContentAdd]);

  const ProvinceOptionsFactory = useMemo(() => {
    return (value = 'value', label = 'label') => {
      return Array.isArray(addressData.provinces)
        ? _.map(addressData.provinces, (p) => {
            const data: any = {};
            data[value] = p['name'];
            data[label] = p['name'];
            return data;
          })
        : [];
    };
  }, [addressData]);

  const DistrictOptionsFactory = useMemo(() => {
    return (provinceName: string, value = 'value', label = 'label') => {
      const province = _.find(
        addressData.provinces,
        (p) => p['name'] === provinceName
      );

      if (province) {
        return Array.isArray(addressData.districts[province.id])
          ? _.map(addressData.districts[province.id], (p) => {
              const data: any = {};
              data[value] = p['name'];
              data[label] = p['name'];
              return data;
            })
          : [];
      }
      return [];
    };
  }, [addressData]);

  const WardOptionsFactory = useMemo(() => {
    return (districtName: string, value = 'value', label = 'label') => {
      const district = _.find(
        _.reduce(addressData.districts, (re, dt) => {
          re = [...re, ...dt];
          return re;
        }),
        (d: any) => d['name'] === districtName
      );

      if (district) {
        return Array.isArray(addressData.wards[district.id])
          ? _.map(addressData.wards[district.id], (p) => {
              const data: any = {};
              data[value] = p['name'];
              data[label] = p['name'];
              return data;
            })
          : [];
      }
      return [];
    };
  }, [addressData]);

  return {
    state: { addressData },
    data: {
      ProvinceOptionsFactory,
      DistrictOptionsFactory,
      WardOptionsFactory,
    },
  };
};
