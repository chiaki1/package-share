import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { getContentAddressDataAction } from '../store';

export const useContentAddressActions = () => {
  const dispatch = useDispatch();
  const getProvinceDistrictWardData = useCallback(() => {
    dispatch(getContentAddressDataAction());
  }, []);

  return {
    actions: { getProvinceDistrictWardData },
  };
};
