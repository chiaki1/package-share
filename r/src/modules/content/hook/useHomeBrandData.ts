import { useSelector } from 'react-redux';
import {
  selectBanners,
  selectBestSeller,
  selectBrandCampaign,
  selectHomeBrand,
} from '../store';

export const useHomeBrandData = () => {
  const homeBrand = useSelector(selectHomeBrand);
  const brandCampaign = useSelector(selectBrandCampaign);
  const bannerHome = useSelector(selectBanners);
  const bestSeller = useSelector(selectBestSeller);
  return {
    state: {
      homeBrand,
      brandCampaign,
      bannerHome,
      bestSeller,
    },
  };
};
