import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useGetCmsPageLazyQuery } from '@vjcspy/apollo';
import { gotCmsPageData } from '../store';

export const useCmsPage = () => {
  const dispatch = useDispatch();

  const [
    getCmsPageInfoQuery,
    { data, error, loading },
  ] = useGetCmsPageLazyQuery();

  const getCmsPageData = useCallback((id) => {
    getCmsPageInfoQuery({
      variables: {
        id: id,
      },
    });
  }, []);

  useEffect(() => {
    if (error) {
      console.warn('Could not get cms page information');
    }
    if (data) {
      dispatch(
        gotCmsPageData({
          cmsPage: data?.cmsPage,
        })
      );
    }
  }, [data, error]);

  return {
    state: { data, loading },
    actions: {
      getCmsPageData,
    },
  };
};
