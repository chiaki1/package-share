import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { getBannerAction } from '../store';

export const useHomeActions = () => {
  const dispatch = useDispatch();
  const getBanner = useCallback(() => {
    dispatch(getBannerAction({}));
  }, []);

  return {
    actions: {
      getBanner,
    },
  };
};
