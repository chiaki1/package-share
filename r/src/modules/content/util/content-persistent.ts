import { AsyncPersistent } from '../../../util';

export const ContentPersistent = new AsyncPersistent(
  undefined,
  'chiaki_content'
);
