import { createAction } from '../../../util';

const PREFIX = 'APP';
const INCREASE_COUNT = 'INCREASE_COUNT';
export const increaseCount = createAction<{ number: number }>(INCREASE_COUNT);

const DECREASE_COUNT = 'DECREASE_COUNT';
export const decreaseCount = createAction<{ number: number }>(DECREASE_COUNT);

const APP_RUN_TIME_ERROR = 'APP_RUN_TIME_ERROR';
export const appRunTimeError = createAction<{
  error: Error;
}>(APP_RUN_TIME_ERROR, PREFIX);

const APP_SHOW_ERROR_MESSAGES = 'APP_SHOW_ERROR_MESSAGE';
export const appShowErrorMessages = createAction<{
  messages: string;
}>(APP_SHOW_ERROR_MESSAGES);
