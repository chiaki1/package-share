import { mapTo, tap, withLatestFrom } from 'rxjs/operators';
import { increaseCount } from './app.actions';
import { createEffect, ofType } from '../../../util';

const whenIncrease$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(increaseCount),
    withLatestFrom(state$),
    tap(([action, state]) =>
      console.log('Run effect: ' + action.type, state.app)
    ),
    mapTo({ type: 'AFTER_INDECREASE_COUNT' })
  )
);

export const APP_EFFECTS = [whenIncrease$];
