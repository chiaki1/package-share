import { PayloadActionCreator } from '@reduxjs/toolkit';
import { map } from 'rxjs/operators';
import _ from 'lodash';
import { EMPTY } from 'rxjs';
import { appShowErrorMessages } from '../store/app.actions';
import { createEffect, ofType } from '../../../util';

export const createEffectHandleErrorMessage = (
  ...allowedTypes: Array<string | PayloadActionCreator<any>>
) => {
  return createEffect((action$) =>
    action$.pipe(
      ofType(...allowedTypes),
      map((action: any) => {
        if (
          action['payload'] &&
          action.payload['error'] &&
          _.isString(action.payload.error['message'])
        ) {
          return appShowErrorMessages({
            messages: action.payload.error['message'],
          });
        } else {
          return EMPTY;
        }
      })
    )
  );
};
