import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import {
  getCustomerReviewAction,
  getRewardPointAction,
  getStoreCreditAction,
} from '../store';

export const useCustomerActions = () => {
  const dispatch = useDispatch();

  const getRewardPoint = useCallback(() => {
    dispatch(getRewardPointAction());
  }, []);

  const getCustomerReviews = useCallback(() => {
    dispatch(getCustomerReviewAction());
  }, []);

  const getStoreCredit = useCallback(() => {
    dispatch(getStoreCreditAction());
  }, []);

  return {
    actions: {
      getRewardPoint,
      getCustomerReviews,
      getStoreCredit,
    },
  };
};
