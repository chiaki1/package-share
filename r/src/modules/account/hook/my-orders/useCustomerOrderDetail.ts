import { useGetCustomerOrdersLazyQuery } from '@vjcspy/apollo';
import _ from 'lodash';
import { useCallback, useEffect, useState } from 'react';

export const useCustomerOrderDetail = () => {
  const [orderDetail, setOrderDetail] = useState<any>();
  const [
    getOrderDetailQuery,
    getOrderDetailRes,
  ] = useGetCustomerOrdersLazyQuery();

  const getOrderDetail = useCallback((order_id: any) => {
    console.log('getOrderDetail', order_id);
    getOrderDetailQuery({
      variables: {
        filter: {
          number: {
            eq: order_id,
          },
        },
      },
    });
  }, []);

  useEffect(() => {
    if (getOrderDetailRes.error) {
      console.warn(getOrderDetailRes.error);
    }
    if (
      _.isArray(getOrderDetailRes.data?.customer?.orders?.items) &&
      _.size(getOrderDetailRes.data?.customer?.orders?.items) === 1
    ) {
      setOrderDetail(_.first(getOrderDetailRes.data?.customer?.orders?.items));
    }
  }, [getOrderDetailRes.data, getOrderDetailRes.error]);

  return {
    actions: {
      getOrderDetail,
    },
    state: {
      orderDetail,
    },
  };
};
