import { useGetCustomerOrdersLazyQuery } from '@vjcspy/apollo';
import _ from 'lodash';
import { useCallback, useEffect, useMemo, useState } from 'react';

export const useCustomerOrders = () => {
  const [filters, setFilters] = useState({
    status: 'all',
  });
  const [orders, setOrders] = useState<any>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(-1);
  const [forceRefresh, setForceRefresh] = useState(false);

  const [getOrdersQuery, getOrdersRes] = useGetCustomerOrdersLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const setFilterStatus = useCallback(
    (status: string) => {
      const _f = { ...filters };
      if (filters.status == status || _.isEmpty(status)) {
        _f.status = 'all';
      } else {
        _f.status = status;
      }
      setFilters(_f);
      setCurrentPage(1);
      setOrders([]);
    },
    [filters]
  );

  const debounceRunQueryGetList = useMemo(() => {
    return _.debounce((variables) => {
      getOrdersQuery({ variables });
    }, 250);
  }, []);

  const debounceLoadMorePage = useMemo(() => {
    return _.debounce(() => {
      setCurrentPage(currentPage + 1);
    }, 250);
  }, [currentPage]);

  const handleLoadMorePage = useCallback(() => {
    if (currentPage < totalPage || totalPage < 0) {
      if (!getOrdersRes.loading) {
        debounceLoadMorePage();
      }
    }
  }, [currentPage, totalPage, getOrdersRes.loading]);

  useEffect(() => {
    debounceRunQueryGetList({
      pageSize: 10,
      currentPage: currentPage,
      filter:
        filters?.status === 'all'
          ? undefined
          : {
              status: {
                eq: filters.status,
              },
            },
    });
  }, [filters, currentPage]);

  useEffect(() => {
    if (forceRefresh) {
      debounceRunQueryGetList({
        pageSize: 10,
        currentPage: currentPage,
        filter:
          filters?.status === 'all'
            ? undefined
            : {
                status: {
                  eq: filters.status,
                },
              },
      });
    }
  }, [filters, currentPage, forceRefresh]);

  useEffect(() => {
    if (getOrdersRes.error) {
      setForceRefresh(false);
      console.error('Could not query getCustomerOrders');
    }
    if (getOrdersRes.data) {
      setForceRefresh(false);
      try {
        if (!!getOrdersRes.data?.customer?.orders) {
          if (
            _.isNumber(getOrdersRes.data.customer.orders.page_info?.total_pages)
          ) {
            setTotalPage(
              getOrdersRes.data.customer.orders.page_info!.total_pages
            );
          }
          setOrders(getOrdersRes.data.customer.orders.items);
          // dispatch(
          //   gotCustomerOrder({
          //     orders: getOrdersRes.data.customer.orders.items,
          //     mergeWithExisting: true,
          //     pageInfo: getOrdersRes.data.customer.orders.page_info!,
          //   })
          // );
        }
      } catch (e) {}
    }
  }, [getOrdersRes.data, getOrdersRes.error]);

  return {
    state: {
      orders,
      currentPage,
      currentStatus: filters.status,
      totalPage,
      isLoading:
        currentPage < totalPage || totalPage == -1 || getOrdersRes.loading,
      isRefreshing: forceRefresh,
    },
    actions: {
      handleLoadMorePage,
      setFilterStatus,
      handleRefresh: () => setForceRefresh(true),
    },
  };
};
