import { useSelector } from 'react-redux';
import { selectAccount } from '../store/account.selector';

export const useAccountState = () => {
  const accountState = useSelector(selectAccount);

  return {
    state: {
      accountState,
    },
  };
};
