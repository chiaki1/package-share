import { useDispatch } from 'react-redux';
import {
  removeWishListAction,
  removeWishListAfterAction,
  removeWishListErrorAction,
} from '../../store';
import { useCallback, useEffect } from 'react';
import { useRemoveProductsFromWishlistMutation } from '@vjcspy/apollo';

export const useCustomerWishlistActions = () => {
  const dispatch = useDispatch();
  const [removeWishlistMutation, removeWishlistRes] =
    useRemoveProductsFromWishlistMutation();

  const removeWishlistItem = useCallback(
    async (wishlistId: any, wishlistItemId: any) => {
      dispatch(
        removeWishListAction({
          wishlistId,
          wishlistItemId,
        })
      );
      try {
        await removeWishlistMutation({
          variables: {
            wishlistId,
            wishlistItemsIds: [wishlistItemId],
          },
        });
      } catch (e) {}
    },
    []
  );

  useEffect(() => {
    if (removeWishlistRes.error) {
      dispatch(
        removeWishListErrorAction({
          error: removeWishlistRes.error,
        })
      );
    }

    if (removeWishlistRes.data?.removeProductsFromWishlist?.wishlist) {
      removeWishListAfterAction({
        wishlist: removeWishlistRes.data?.removeProductsFromWishlist?.wishlist,
      });
    }
  }, [removeWishlistRes]);

  return {
    actions: {
      removeWishlistItem,
    },
  };
};
