import { useDispatch, useSelector } from 'react-redux';
import {
  getWishListAction,
  getWishListAfterAction,
  getWishListErrorAction,
  selectCustomer,
  selectIsLoadingWishlist,
  selectCustomerWishlist,
} from '../../store';
import { useGetWishlistDetailLazyQuery } from '@vjcspy/apollo';
import { useEffect } from 'react';

export const useCustomerWishlistData = () => {
  const wishlists = useSelector(selectCustomerWishlist);
  const isLoading = useSelector(selectIsLoadingWishlist);
  const customer = useSelector(selectCustomer);
  const dispatch = useDispatch();
  const [getWishlistQuery, getWishlistRes] = useGetWishlistDetailLazyQuery({
    fetchPolicy: 'cache-and-network',
  });
  useEffect(() => {
    if (!!customer) {
      dispatch(getWishListAction());
      getWishlistQuery({});
    }
  }, [customer]);

  useEffect(() => {
    if (getWishlistRes.error) {
      dispatch(
        getWishListErrorAction({
          error: getWishlistRes.error,
        })
      );
    }

    if (getWishlistRes?.data?.customer?.wishlists) {
      dispatch(
        getWishListAfterAction({
          wishlists: getWishlistRes?.data?.customer?.wishlists,
        })
      );
    }
  }, [getWishlistRes?.data?.customer?.wishlists, getWishlistRes.error]);

  return {
    state: {
      wishlists,
      isLoading,
    },
  };
};
