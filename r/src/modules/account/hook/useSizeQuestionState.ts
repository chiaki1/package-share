import { useSelector } from 'react-redux';
import { selectLoadingState } from '../store/account.selector';
import {
  selectCustomerSizes,
  selectSizeQuestions,
} from '../store/my-size/selector';

export const useSizeQuestions = () => {
  const questions = useSelector(selectSizeQuestions);
  const loading = useSelector(selectLoadingState);
  const customer_size = useSelector(selectCustomerSizes);

  const isLoadingCustomerSizes = loading.getCustomerSize;

  return {
    state: {
      questions,
      isLoadingCustomerSizes,
      customer_size,
    },
  };
};
