import { useDispatch } from 'react-redux';
import { useCallback, useEffect } from 'react';
import {
  CustomerUpdateInput,
  useUpdateCustomerInfoMutation,
} from '@vjcspy/apollo';
import {
  updateCustomerInfoAction,
  updateCustomerInfoAfterAction,
  updateCustomerInfoErrorAction,
} from '../store/customer-info/customer.info.actions';

export const useCustomerInfoActions = () => {
  const dispatch = useDispatch();
  const [
    updateCustomerMutation,
    updateCustomerRes,
  ] = useUpdateCustomerInfoMutation();

  const updateCustomerInfo = useCallback(async (info: CustomerUpdateInput) => {
    dispatch(
      updateCustomerInfoAction({
        info,
      })
    );
    try {
      await updateCustomerMutation({
        variables: {
          input: info,
        },
      });
    } catch (e) {}
  }, []);

  useEffect(() => {
    if (updateCustomerRes.error) {
      dispatch(
        updateCustomerInfoErrorAction({
          error: updateCustomerRes.error,
        })
      );
    }

    if (updateCustomerRes?.data?.updateCustomerV2?.customer) {
      dispatch(
        updateCustomerInfoAfterAction({
          customer: updateCustomerRes?.data?.updateCustomerV2?.customer,
        })
      );
    }
  }, [
    updateCustomerRes.data?.updateCustomerV2?.customer,
    updateCustomerRes.error,
  ]);

  return {
    actions: { updateCustomerInfo },
  };
};
