import { useSelector } from 'react-redux';
import { selectStoreCredit } from '../store/account.selector';

export const useStoreCredit = () => {
  const store_credit = useSelector(selectStoreCredit);

  return {
    state: {
      store_credit,
    },
  };
};
