import { useSelector } from 'react-redux';
import { selectCustomerReviews } from '../store/account.selector';

export const useCustomerReviews = () => {
  const reviews = useSelector(selectCustomerReviews);

  return {
    state: {
      reviews,
    },
  };
};
