import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import {
  getCustomerSizesActions,
  getFlowSizeQuestionAction,
  saveAnswerAction,
  selectActiveSizeAction,
} from '../store';

export const useSizeQuestionActions = () => {
  const dispatch = useDispatch();

  const getSizeQuestion = useCallback(() => {
    dispatch(getFlowSizeQuestionAction());
  }, []);

  const saveAnswer = useCallback((name: string, answers: any[]) => {
    dispatch(
      saveAnswerAction({
        name,
        answers,
      })
    );
  }, []);

  const selectActiveSize = useCallback(
    (size_id: string, pants_size: string, shirt_size: string) => {
      dispatch(
        selectActiveSizeAction({
          size_id,
          pants_size,
          shirt_size,
        })
      );
    },
    []
  );

  const getCustomerSizes = useCallback(() => {
    dispatch(getCustomerSizesActions());
  }, []);

  return {
    actions: {
      getSizeQuestion,
      saveAnswer,
      selectActiveSize,
      getCustomerSizes,
    },
  };
};
