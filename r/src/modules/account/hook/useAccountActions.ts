import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import {
  checkCustomerIsLogged,
  generateCustomerTokenAction,
  registerCustomerByEmailPassword,
  socialLoginAction,
  clearCustomerToken,
} from '../store';

export const useAccountActions = () => {
  const dispatch = useDispatch();

  const generateCustomerToken = useCallback(
    (email: string, password: string) => {
      dispatch(
        generateCustomerTokenAction({
          email,
          password,
        })
      );
    },
    []
  );

  const onLoginSocial = useCallback((provider: string, token: string) => {
    dispatch(
      socialLoginAction({
        provider,
        token,
      })
    );
  }, []);

  const registerCustomerAccount = useCallback((customerData: any) => {
    dispatch(registerCustomerByEmailPassword({ ...customerData }));
  }, []);

  const dispatchCheckCustomerIsLogged = useCallback(() => {
    dispatch(checkCustomerIsLogged({}));
  }, []);

  const logout = useCallback(() => {
    dispatch(clearCustomerToken());
  }, []);

  return {
    actions: {
      generateCustomerToken,
      registerCustomerAccount,
      dispatchCheckCustomerIsLogged,
      onLoginSocial,
      logout,
    },
  };
};
