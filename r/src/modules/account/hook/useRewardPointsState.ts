import { useSelector } from 'react-redux';
import { selectRewardPoints } from '../store';

export const useRewardPoints = () => {
  const reward_points = useSelector(selectRewardPoints);

  return {
    state: {
      reward_points,
    },
  };
};
