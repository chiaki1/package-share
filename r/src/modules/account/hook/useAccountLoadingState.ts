import { useSelector } from 'react-redux';
import { selectAccount } from '../store';

export const useAccountLoadingState = () => {
  const accountState = useSelector(selectAccount);

  const isGeneratingToken = accountState.loadingState.generateToken;
  const isCustomerLogged = !!accountState.token;
  const isShowLoginForm =
    accountState.isResolvedCustomerState && !accountState.token;

  return { state: { isGeneratingToken, isCustomerLogged, isShowLoginForm } };
};
