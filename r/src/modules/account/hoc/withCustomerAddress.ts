import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerAddress } from '../hook';

export const withCustomerAddress = createUiHOC(
  () => useCustomerAddress(),
  'withCustomerAddress'
);
