import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerWishlistActions } from '../../hook';

export const withCustomerWishlistActions = createUiHOC(
  () => useCustomerWishlistActions(),
  'withCustomerWishlistActions'
);
