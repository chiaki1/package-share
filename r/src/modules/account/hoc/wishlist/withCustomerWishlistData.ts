import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerWishlistData } from '../../hook';

export const withCustomerWishlistData = createUiHOC(
  () => useCustomerWishlistData(),
  'withCustomerWishlistData'
);
