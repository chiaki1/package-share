import { withCustomerWishlistData } from './withCustomerWishlistData';
import { withCustomerWishlistActions } from './withCustomerWishlistActions';

export const WISHLIST_HOCS = [
  withCustomerWishlistData,
  withCustomerWishlistActions,
];
