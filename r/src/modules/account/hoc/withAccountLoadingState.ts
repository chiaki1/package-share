import { createUiHOC } from '@vjcspy/ui-extension';
import { useAccountLoadingState } from '../hook';

export const withAccountLoadingState = createUiHOC(() => {
  return useAccountLoadingState();
}, 'withAccountLoadingState');
