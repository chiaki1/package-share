import { createUiHOC } from '@vjcspy/ui-extension';
import { useStoreCredit } from '../hook';

export const withStoreCredit = createUiHOC(
  () => useStoreCredit(),
  'withStoreCredit'
);
