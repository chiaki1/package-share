import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';
import { selectCustomer } from '../store';
import { selectIsUpdatingCustomerInfo } from '../store/customer-info/customer-info.selector';

export const withCustomer = createUiHOC(() => {
  const customer = useSelector(selectCustomer);
  const isUpdatingCustomerInfo = useSelector(selectIsUpdatingCustomerInfo);
  return {
    state: {
      customer,
      isUpdatingCustomerInfo,
    },
  };
}, 'withCustomer');
