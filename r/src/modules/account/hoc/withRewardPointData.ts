import { createUiHOC } from '@vjcspy/ui-extension';
import { useRewardPoints } from '../hook';

export const withRewardPoints = createUiHOC(
  () => useRewardPoints(),
  'withRewardPoints'
);

export const withRewardPointData = createUiHOC(
  () => useRewardPoints(),
  'withRewardPointData'
);
