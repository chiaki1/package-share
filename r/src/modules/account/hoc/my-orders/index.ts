import { withCustomerOrders } from './withCustomerOrders';
import { withCustomerOrderDetail } from './withCustomerOrderDetail';
import { withMyOrdersPaging } from './withMyOrdersPaging';

export const MY_ORDER_HOCS = [
  withCustomerOrders,
  withCustomerOrderDetail,
  withMyOrdersPaging,
];
