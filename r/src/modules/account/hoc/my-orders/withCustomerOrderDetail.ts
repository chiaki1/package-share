import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerOrderDetail } from '../../hook';

export const withCustomerOrderDetail = createUiHOC(() => {
  return useCustomerOrderDetail();
}, 'withCustomerOrderDetail');
