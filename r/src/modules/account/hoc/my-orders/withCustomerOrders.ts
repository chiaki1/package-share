import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerOrders } from '../../hook';

export const withCustomerOrders = createUiHOC(() => {
  return useCustomerOrders();
}, 'withCustomerOrders');
