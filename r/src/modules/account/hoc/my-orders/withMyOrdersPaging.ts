import { createUiHOC } from '@vjcspy/ui-extension';
import { useMyOrdersPaging } from '../../hook/my-orders/useMyOrdersPaging';

export const withMyOrdersPaging = createUiHOC(
  () => useMyOrdersPaging(),
  'withMyOrdersPaging'
);
