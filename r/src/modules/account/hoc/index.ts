import { withAccountActions } from './withAccountActions';
import { withAccountState } from './withAccountState';
import { withCustomerAddress } from './withCustomerAddress';
import { withInitAccountState } from './withInitAccountState';
import { withCustomer } from './withCustomer';
import { withAccountLoadingState } from './withAccountLoadingState';
import { withSizeQuestionState } from './withSizeQuestionState';
import { withSizeQuestionActions } from './withSizeQuestionActions';
import { withCustomerActions } from './withCustomerActions';
import { withCustomerReviews } from './withCustomerReviews';
import { withRewardPointData, withRewardPoints } from './withRewardPointData';
import { withStoreCredit } from './withStoreCredit';
import { withCreateAccount } from './withCreateAccount';
import { withCustomerAddressActions } from './withCustomerAddressActions';
import { MY_ORDER_HOCS } from './my-orders';
import { WISHLIST_HOCS } from './wishlist';
import { withUpdateCustomerInfoActions } from './withUpdateCustomerInfoActions';
import { PHONE_HOCS } from './phone';

export const R_ACCOUNT_HOCS = [
  withAccountActions,
  withAccountState,
  withCustomerAddress,
  withInitAccountState,
  withCustomer,
  withCustomerAddressActions,
  withAccountLoadingState,
  withSizeQuestionState,
  withSizeQuestionActions,
  withCustomerActions,
  withCustomerReviews,
  withRewardPoints,
  withStoreCredit,
  withCreateAccount,
  withRewardPointData,
  withUpdateCustomerInfoActions,

  ...MY_ORDER_HOCS,
  ...WISHLIST_HOCS,
  ...PHONE_HOCS,
];
