import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerActions } from '../hook';

export const withCustomerActions = createUiHOC(() => {
  return useCustomerActions();
}, 'withCustomerActions');
