import { createUiHOC } from '@vjcspy/ui-extension';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { checkCustomerIsLogged, selectResolvedAccountState } from '../store';

export const withInitAccountState = createUiHOC(() => {
  const dispatch = useDispatch();
  const isResolvedAccountState = useSelector(selectResolvedAccountState);
  useEffect(() => {
    dispatch(checkCustomerIsLogged({}));
  }, []);

  return {
    state: {
      isResolvedAccountState,
    },
  };
}, 'withInitAccountState');
