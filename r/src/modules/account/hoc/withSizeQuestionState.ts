import { createUiHOC } from '@vjcspy/ui-extension';
import { useSizeQuestions } from '../hook';

export const withSizeQuestionState = createUiHOC(
  () => useSizeQuestions(),
  'withSizeQuestionState'
);
