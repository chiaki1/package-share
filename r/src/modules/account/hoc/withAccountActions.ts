import { useAccountActions } from '../hook';
import { createUiHOC } from '@vjcspy/ui-extension';

export const withAccountActions = createUiHOC(() => {
  return useAccountActions();
}, 'withAccountActions');
