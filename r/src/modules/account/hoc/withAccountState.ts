import { createUiHOC } from '@vjcspy/ui-extension';
import { useAccountState } from '../hook';

export const withAccountState = createUiHOC(() => {
  return useAccountState();
}, 'withAccountState');
