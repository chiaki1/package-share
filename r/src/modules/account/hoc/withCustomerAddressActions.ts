import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerAddActions } from '../hook';

export const withCustomerAddressActions = createUiHOC(
  () => useCustomerAddActions(),
  'withCustomerAddressActions'
);
