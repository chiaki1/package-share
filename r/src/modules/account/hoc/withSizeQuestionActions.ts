import { createUiHOC } from '@vjcspy/ui-extension';
import { useSizeQuestionActions } from '../hook';

export const withSizeQuestionActions = createUiHOC(() => {
  return useSizeQuestionActions();
}, 'withSizeQuestionActions');
