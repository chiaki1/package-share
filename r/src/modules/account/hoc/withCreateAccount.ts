import { createUiHOC } from '@vjcspy/ui-extension';
import { useCreateAccount } from '../hook';

export const withCreateAccount = createUiHOC(() => {
  return useCreateAccount();
}, 'withCreateAccount');
