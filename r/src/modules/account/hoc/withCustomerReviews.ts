import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerReviews } from '../hook';

export const withCustomerReviews = createUiHOC(
  () => useCustomerReviews(),
  'withCustomerReviews'
);
