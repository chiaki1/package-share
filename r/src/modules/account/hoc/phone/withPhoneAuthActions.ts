import { createUiHOC } from '@vjcspy/ui-extension';
import { usePhoneAuthActions } from '../../hook/phone/usePhoneAuthActions';

export const withPhoneAuthActions = createUiHOC(
  () => usePhoneAuthActions(),
  'withPhoneAuthActions'
);
