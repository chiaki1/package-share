import { createUiHOC } from '@vjcspy/ui-extension';
import { useCustomerInfoActions } from '../hook';

export const withUpdateCustomerInfoActions = createUiHOC(
  () => useCustomerInfoActions(),
  'withUpdateCustomerInfoActions'
);
