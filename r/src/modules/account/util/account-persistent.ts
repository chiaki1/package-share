import { AsyncPersistent } from '../../../util';

export const AccountPersistent = new AsyncPersistent(
  undefined,
  'chiaki_account'
);
