import { graphQLFetchWithAdditionHeaders } from '../../../util';

export const graphqlFetchForCustomer = (opts: {
  query: string;
  variables?: any;
}) => {
  return graphQLFetchWithAdditionHeaders(opts, ['Authorization', 'Store']);
};
