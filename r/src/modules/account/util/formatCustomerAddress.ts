import _ from 'lodash';

export const formatCustomerAddress = (address: any) => {
  return `${address['firstname']} ${address['lastname']},
 ${_.join(address['street'], ' ')},
  ${address['iz_address_ward']},
  ${address['iz_address_district']},
  ${address['iz_address_province']}
  `;
};
