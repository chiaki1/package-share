import _ from 'lodash';

export const getCustomerStreet = (customer: { street: string[] }) => {
  return _.first(customer.street);
};
