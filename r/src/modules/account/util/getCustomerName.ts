import _ from 'lodash';

export const getCustomerName = (customer: {
  firstname: string;
  lastname: string;
  middlename: string;
}) => {
  return `${customer.lastname} ${customer.firstname}`;
};

export const parseCustomerName = (customerName: string) => {
  const nameAsArray = customerName.split(' ');
  if (_.size(nameAsArray) < 2) {
    console.warn('Customer name invalid');
    return undefined;
  }
  const lastname = nameAsArray[0];
  const firstname = _.join(_.drop(nameAsArray), ' ');

  return {
    lastname,
    firstname,
  };
};
