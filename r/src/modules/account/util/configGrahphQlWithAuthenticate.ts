import { Registry } from '@vjcspy/chitility';
import { AccountConstant } from './constant';
import { R_DEFAULT_VALUE } from '../../../values';
import { AccountPersistent } from './account-persistent';

export const configGraphQlWithAuthenticate = () => {
  const additionalHeaderResolved = Registry.getInstance().registry(
    R_DEFAULT_VALUE.GRAPHQL_RESOLVE_ADDITIONAL_HEADER_KEY
  );

  const resolveAuth = async () => {
    const token = await AccountPersistent.getItem(AccountConstant.TOKEN_KEY);
    if (token) {
      return `Bearer ${token}`;
    } else {
      return undefined;
    }
  };

  const withAuthenticateHeader = Object.assign(
    {},
    { ...additionalHeaderResolved },
    {
      Authorization: resolveAuth,
    }
  );

  Registry.getInstance().register(
    R_DEFAULT_VALUE.GRAPHQL_RESOLVE_ADDITIONAL_HEADER_KEY,
    withAuthenticateHeader
  );
};
