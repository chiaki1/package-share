import { createBuilderCallback } from '../../../../util';
import { AccountState } from '../account.state';
import {
  getWishListAction,
  getWishListAfterAction,
  getWishListErrorAction,
  removeWishListAction,
  removeWishListAfterAction,
  removeWishListErrorAction,
} from './wishlist.actions';

export const myWishlistBuilderCallback = createBuilderCallback<AccountState>(
  (builder) => {
    builder
      .addCase(getWishListAction, (state) => {
        state.loadingState.getWishlist = true;
      })
      .addCase(getWishListAfterAction, (state, action) => {
        state.loadingState.getWishlist = false;
        state.wishlist = action.payload.wishlists;
      })
      .addCase(getWishListErrorAction, (state) => {
        state.loadingState.getWishlist = false;
      })
      .addCase(removeWishListAction, (state) => {
        state.loadingState.getWishlist = true;
      })
      .addCase(removeWishListAfterAction, (state, action) => {
        state.loadingState.getWishlist = false;
      })
      .addCase(removeWishListErrorAction, (state) => {
        state.loadingState.getWishlist = false;
      });
  }
);
