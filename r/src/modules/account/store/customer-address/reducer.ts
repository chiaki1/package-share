import { createBuilderCallback } from '../../../../util';
import { AccountState } from '../account.state';
import {
  deleteCustomerAddressAction,
  deleteCustomerAddressAfterAction,
  deleteCustomerAddressErrorAction,
  getCustomerAddressAfterAction,
} from './actions';

export const customerAddressBuilderCallBack = createBuilderCallback<AccountState>(
  (builder) => {
    builder
      .addCase(getCustomerAddressAfterAction, (state, action) => {
        if (state.customer) {
          state.customer.addresses = action.payload.addresses;
          if (action.payload.customer) {
            state.customer = action.payload.customer;
          }
        }
      })
      .addCase(deleteCustomerAddressAction, (state, action) => {
        state.deletingAddressId = action.payload.id;
      })
      .addCase(deleteCustomerAddressAfterAction, (state) => {
        state.deletingAddressId = undefined;
      })
      .addCase(deleteCustomerAddressErrorAction, (state) => {
        state.deletingAddressId = undefined;
      });
  }
);
