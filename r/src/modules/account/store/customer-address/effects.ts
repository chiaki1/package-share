import { createEffect, ofType } from '../../../../util';
import {
  createNewCustomerAddressAfterAction,
  deleteCustomerAddressAfterAction,
  getCustomerAddressAction,
  getCustomerAddressAfterAction,
  getCustomerAddressErrorAction,
  updateCustomerAddressAfterAction,
} from './actions';
import { catchError, map, mapTo, switchMap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { graphqlFetchForCustomer } from '../../util';
import { RuntimeError } from '@vjcspy/chitility';
import { of } from 'rxjs';
import CustomerDetail from '../../../graphql/schema/CustomerDetail';

const whenGetCustomerAddress$ = createEffect((action$) =>
  action$.pipe(
    ofType(getCustomerAddressAction),
    switchMap(() =>
      fromPromise(
        graphqlFetchForCustomer({
          query: `
          query getCustomer {
    customer {
        ${CustomerDetail}
    }
}
          `,
        })
      ).pipe(
        map((data: any) => {
          if (data && data.customer) {
            return getCustomerAddressAfterAction({
              addresses: data.customer.addresses,
              customer: data.customer,
            });
          }

          return getCustomerAddressErrorAction({
            error: new RuntimeError('customer response invalid'),
          });
        }),
        catchError((error) =>
          of(
            getCustomerAddressErrorAction({
              error,
            })
          )
        )
      )
    )
  )
);

const updateCustomerDetailAfterUpdateAdd$ = createEffect((action$) =>
  action$.pipe(
    ofType(
      updateCustomerAddressAfterAction,
      createNewCustomerAddressAfterAction,
      deleteCustomerAddressAfterAction
    ),
    mapTo(getCustomerAddressAction())
  )
);

export const R_CUSTOMER_ADDRESS_EFFECTS = [
  whenGetCustomerAddress$,
  updateCustomerDetailAfterUpdateAdd$,
];
