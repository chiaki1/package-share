import { Customer } from '@vjcspy/apollo';

export interface AccountState {
  /*
   * Chỉ khi resolve customer state thì mới hiện các component liên quan đến đăng nhập/đăng xuất
   * */
  isResolvedCustomerState: boolean;
  token?: string;
  customer?: Customer;
  loadingState: {
    generateToken?: boolean;
    getTokenFromFb?: boolean;
    getCustomerSize?: boolean;
    getWishlist?: boolean;
    info?: boolean;
    createAccount?: boolean;
  };
  deletingAddressId?: number;
  referer?: string;
  reward_points?: any;
  reviews?: any[];
  store_credit?: any;
  size_questions?: any[];
  orders: any[];
  orderDetail: any;
  orderFilter?: any;
  customer_size: any[];
  wishlist?: any[];
}

export const accountStateFactory = (): AccountState => <AccountState>(<unknown>{
    isResolvedCustomerState: false,
    loadingState: {
      generateToken: false,
      getTokenFromFb: false,
      getCustomerSize: false,
    },
    orders: [],
    customer_size: [],
  });
