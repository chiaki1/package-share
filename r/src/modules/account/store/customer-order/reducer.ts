import _ from 'lodash';
import { createBuilderCallback } from '../../../../util';
import { AccountState } from '../account.state';
import { gotCustomerOrder } from './actions';

export const customerOrderBuilderCallBack = createBuilderCallback<AccountState>(
  (builder) => {
    builder.addCase(gotCustomerOrder, (state, action) => {
      if (action.payload.mergeWithExisting && action.payload.pageInfo) {
        state.orders = _.filter(
          state.orders,
          (p) =>
            p?.pageInfo?.current_page &&
            action.payload!.pageInfo!.current_page &&
            p?.pageInfo?.current_page != action.payload!.pageInfo!.current_page
        );
        state.orders.push(
          ..._.map(action.payload.orders, (o) => {
            o['pageInfo'] = action.payload.pageInfo;

            return o;
          })
        );
      } else {
        state.orders = action.payload.orders;
      }
      _.orderBy(
        state.orders,
        (order) => new Date(order.order_date).getTime(),
        'desc'
      );
    });
  }
);
