import { createAction } from '../../../../util';
import { SearchResultPageInfo } from '@vjcspy/apollo';

const PREFIX = 'CUSTOMER_ORDER';

const GOT_CUSTOMER_ORDERS = 'GOT_CUSTOMER_ORDERS';
export const gotCustomerOrder = createAction<{
  orders: any;
  mergeWithExisting?: boolean;
  pageInfo?: SearchResultPageInfo;
}>(GOT_CUSTOMER_ORDERS, PREFIX);
