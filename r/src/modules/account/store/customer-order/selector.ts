import { AccountState } from '../account.state';

export const selectOrders = (state: { account: AccountState }) =>
  state.account.orders;
