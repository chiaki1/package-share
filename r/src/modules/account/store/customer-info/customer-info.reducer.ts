import { createBuilderCallback } from '../../../../util';
import { AccountState } from '../account.state';
import {
  updateCustomerInfoAction,
  updateCustomerInfoAfterAction,
  updateCustomerInfoErrorAction,
} from './customer.info.actions';

export const customerInfoReducer = createBuilderCallback<AccountState>(
  (builder) => {
    builder
      .addCase(updateCustomerInfoAction, (state) => {
        state.loadingState.info = true;
      })
      .addCase(updateCustomerInfoAfterAction, (state, action) => {
        state.customer = action.payload.customer;
        state.loadingState.info = false;
      })
      .addCase(updateCustomerInfoErrorAction, (state) => {
        state.loadingState.info = false;
      });
  }
);
