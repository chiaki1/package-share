import { AccountState } from '../account.state';

export const selectIsUpdatingCustomerInfo = (state: {
  account: AccountState;
}) => state.account?.loadingState?.info;
