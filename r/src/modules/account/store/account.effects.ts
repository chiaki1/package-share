import {
  checkCustomerIsLogged,
  clearCustomerToken,
  clearCustomerTokenAfter,
  emptyCustomerTokenInStorage,
  generateCustomerTokenAction,
  generateCustomerTokenFail,
  generateCustomerTokenSuccessAction,
  getCustomerDetailFailDueToToken,
  getCustomerReviewAction,
  getCustomerReviewAfterAction,
  getRewardPointAction,
  getRewardPointAfterAction,
  getStoreCreditAction,
  getStoreCreditAfterAction,
  gotCustomerDetail,
  gotCustomerTokenInStorage,
  persistentCustomerTokenSuccess,
  registerCustomerByEmailPassword,
  registerCustomerByEmailPasswordFail,
  registerCustomerByEmailPasswordSuccess,
  socialLoginAction,
  getCustomerDetail,
} from './account.actions';
import {
  catchError,
  map,
  mapTo,
  switchMap,
  debounceTime,
  withLatestFrom,
  filter,
} from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { from, of } from 'rxjs';
import {
  AccountConstant,
  graphqlFetchForCustomer as graphqlFetch,
  AccountPersistent,
} from '../util';
import { RuntimeError } from '@vjcspy/chitility';
import { createEffect, ofType, proxyFetch } from '../../../util';
import { appRunTimeError } from '../../app';
import CustomerDetail from '../../graphql/schema/CustomerDetail';
import { R_CUSTOMER_ADDRESS_EFFECTS } from './customer-address/effects';
import { MY_SIZE_EFFECTS } from './my-size/effects';
import { PHONE_EFFECTS } from './phone/phone.effects';
import { flatMap } from 'rxjs/internal/operators';

const checkCustomerIsLogged$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(checkCustomerIsLogged, clearCustomerTokenAfter),
    debounceTime(500),
    withLatestFrom(state$, (action, state) => [
      action,
      state.account?.isResolvedCustomerState,
    ]),
    filter((data) => data[1] === false),
    switchMap(() =>
      fromPromise(AccountPersistent.getItem(AccountConstant.TOKEN_KEY)).pipe(
        map((token) => {
          return token
            ? gotCustomerTokenInStorage({
                token,
              })
            : emptyCustomerTokenInStorage({});
        })
      )
    )
  )
);

const whenGenerateToken$ = createEffect((action$) =>
  action$.pipe(
    ofType(generateCustomerTokenAction),
    switchMap((value) => {
      return fromPromise(
        proxyFetch({
          type: 'generate-customer-token',
          payload: {
            email: value.payload.email,
            password: value.payload.password,
          },
        })
      ).pipe(
        switchMap((res: any) => {
          if (res && res.hasOwnProperty('token')) {
            return from([
              generateCustomerTokenSuccessAction({
                token: res.token,
              }),
            ]);
          } else {
            return of(
              generateCustomerTokenFail({
                error: new RuntimeError('Wrong data response format'),
              })
            );
          }
        }),
        catchError((error) =>
          of(
            generateCustomerTokenFail({
              error,
            })
          )
        )
      );
    })
  )
);

const saveCustomerTokenToStorage$ = createEffect((action$) =>
  action$.pipe(
    ofType(generateCustomerTokenSuccessAction),
    switchMap((action) =>
      fromPromise(
        AccountPersistent.saveItem(
          AccountConstant.TOKEN_KEY,
          action.payload['token'],
          2592000
        )
      ).pipe(
        map(() => {
          return persistentCustomerTokenSuccess({
            token: action.payload['token'],
          });
        }),
        catchError(() =>
          of(
            appRunTimeError({
              error: new RuntimeError(
                'Could not save customer token to storage'
              ),
            })
          )
        )
      )
    )
  )
);

/**
 * Lưu ý đây là nơi duy nhất để lấy và save customer token vào state
 * @type {(action$: ActionsObservable<{type: string}>, stateObservable: StateObservable<any>) => Observable<{type: string}>}
 */
const gotCustomerToken$ = createEffect((action$) =>
  action$.pipe(
    ofType(persistentCustomerTokenSuccess),
    map((action) =>
      gotCustomerTokenInStorage({
        token: action.payload.token,
      })
    )
  )
);

const getCustomerDetail$ = createEffect((action$) =>
  action$.pipe(
    ofType(gotCustomerTokenInStorage, getCustomerDetail),
    debounceTime(100),
    switchMap(() =>
      fromPromise(
        graphqlFetch({
          query: `# expects bearer header to be set via context to return data
query getCustomer {
    customer {
        ${CustomerDetail}
    }
}

`,
        })
      ).pipe(
        map((data: any) => {
          return gotCustomerDetail({
            customer: data.customer,
          });
        }),
        catchError((error: any) => {
          // trường hợp token hết hạn hoặc invalid
          if (
            error?.response?.data?.customer === null &&
            error?.response?.status === 200
          ) {
            console.warn(
              'Customer token invalid. Will clear customer token data'
            );
            return of(getCustomerDetailFailDueToToken({}));
          }
          console.log(error);
          return of(
            appRunTimeError({
              error,
            })
          );
        })
      )
    )
  )
);

const clearCustomerToken$ = createEffect((action$) =>
  action$.pipe(
    ofType(getCustomerDetailFailDueToToken),
    mapTo(clearCustomerToken({}))
  )
);

const doClearCustomerToken$ = createEffect((action$) =>
  action$.pipe(
    ofType(clearCustomerToken),
    map(() => {
      AccountPersistent.removeItem(AccountConstant.TOKEN_KEY);

      return clearCustomerTokenAfter({});
    })
  )
);

const triggerEmptyToken$ = createEffect((action$) =>
  action$.pipe(
    ofType(clearCustomerTokenAfter),
    mapTo(emptyCustomerTokenInStorage())
  )
);

const registerByEmailPass$ = createEffect((action$) =>
  action$.pipe(
    ofType(registerCustomerByEmailPassword),
    debounceTime(250),
    switchMap((action) =>
      fromPromise(
        proxyFetch({
          type: 'create-customer-account',
          payload: action.payload,
        })
      ).pipe(
        flatMap((data: any) => {
          if (typeof data?.customer?.token !== 'string') {
            return of(
              registerCustomerByEmailPasswordFail({
                error: new RuntimeError('wrong data format'),
              })
            );
          }
          return from([
            registerCustomerByEmailPasswordSuccess({ customer: data }),
            generateCustomerTokenSuccessAction({
              token: data.customer.token,
            }),
          ]);
        }),
        catchError((error) =>
          of(registerCustomerByEmailPasswordFail({ error }))
        )
      )
    )
  )
);

const whenSocialLogin$ = createEffect((action$) =>
  action$.pipe(
    ofType(socialLoginAction),
    switchMap((value) => {
      return fromPromise(
        proxyFetch({
          type: 'social-login',
          payload: {
            provider: value.payload.provider,
            token: value.payload.token,
            expires_in: 2592000,
          },
        })
      ).pipe(
        switchMap((res: any) => {
          if (res && res.hasOwnProperty('token')) {
            return from([
              generateCustomerTokenSuccessAction({
                token: res.token,
              }),
            ]);
          } else {
            return of(
              generateCustomerTokenFail({
                error: new RuntimeError('Wrong data response format'),
              })
            );
          }
        }),
        catchError((error) => {
          return of(
            generateCustomerTokenFail({
              error,
            })
          );
        })
      );
    })
  )
);

const whenGetRewardPoint$ = createEffect((action$) =>
  action$.pipe(
    ofType(getRewardPointAction),
    debounceTime(100),
    switchMap(() =>
      fromPromise(
        graphqlFetch({
          query: `
          query {
            customer {
                reward_points{
                    balance{
                        points
                        money{
                            currency
                            value
                        }
                    }
                }
            }
        }
`,
        })
      ).pipe(
        map((data: any) => {
          return getRewardPointAfterAction({
            reward_points: data,
          });
        }),
        catchError((error: any) => {
          // trường hợp token hết hạn hoặc invalid
          if (
            error?.response?.data?.customer === null &&
            error?.response?.status === 200
          ) {
            console.warn(
              'Customer token invalid. Will clear customer token data'
            );
            return of(getCustomerDetailFailDueToToken({}));
          }
          return of(
            appRunTimeError({
              error,
            })
          );
        })
      )
    )
  )
);

const whenGetStoreCreditAction$ = createEffect((action$) =>
  action$.pipe(
    ofType(getStoreCreditAction),
    debounceTime(100),
    switchMap(() =>
      fromPromise(
        graphqlFetch({
          query: `
          query {
            customer {
              firstname
              lastname
              store_credit {
                enabled
                balance_history(pageSize: 10) {
                  items {
                    action
                    actual_balance {
                      currency
                      value
                    }
                    balance_change {
                      currency
                      value
                    }
                    date_time_changed
                  }
                  page_info {
                    page_size
                    current_page
                    total_pages
                  }
                  total_count
                }
                current_balance {
                  currency
                  value
                }
              }
            }
          }
`,
        })
      ).pipe(
        map((data: any) => {
          return getStoreCreditAfterAction({
            store_credit: data?.customer?.store_credit,
          });
        }),
        catchError((error: any) => {
          // trường hợp token hết hạn hoặc invalid
          if (
            error?.response?.data?.customer === null &&
            error?.response?.status === 200
          ) {
            console.warn(
              'Customer token invalid. Will clear customer token data'
            );
            return of(getCustomerDetailFailDueToToken({}));
          }
          return of(
            appRunTimeError({
              error,
            })
          );
        })
      )
    )
  )
);

const whenGetCustomerReviews$ = createEffect((action$) =>
  action$.pipe(
    ofType(getCustomerReviewAction),
    debounceTime(100),
    switchMap(() =>
      fromPromise(
        graphqlFetch({
          query: `
          query {
            customer {
                reviews(pageSize: 20, currentPage: 1) {
                    items {
                        product {
                            name
                            sku
                        }
                        summary
                        text
                        nickname
                        created_at
                        average_rating
                        ratings_breakdown {
                            name
                            value
                        }
                    }
                    page_info {
                        current_page
                        page_size
                        total_pages
                    }
                }
            }
        }
`,
        })
      ).pipe(
        map((data: any) => {
          return getCustomerReviewAfterAction({
            reviews: data,
          });
        }),
        catchError((error: any) => {
          // trường hợp token hết hạn hoặc invalid
          if (
            error?.response?.data?.customer === null &&
            error?.response?.status === 200
          ) {
            console.warn(
              'Customer token invalid. Will clear customer token data'
            );
            return of(getCustomerDetailFailDueToToken({}));
          }
          return of(
            appRunTimeError({
              error,
            })
          );
        })
      )
    )
  )
);

export const ACCOUNT_EFFECTS = [
  checkCustomerIsLogged$,
  whenGenerateToken$,
  saveCustomerTokenToStorage$,
  registerByEmailPass$,
  gotCustomerToken$,
  getCustomerDetail$,
  clearCustomerToken$,
  doClearCustomerToken$,
  whenSocialLogin$,
  whenGetRewardPoint$,
  whenGetCustomerReviews$,
  whenGetStoreCreditAction$,
  triggerEmptyToken$,

  ...R_CUSTOMER_ADDRESS_EFFECTS,
  ...MY_SIZE_EFFECTS,
  ...PHONE_EFFECTS,
];
