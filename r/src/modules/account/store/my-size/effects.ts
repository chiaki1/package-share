import {
  getFlowSizeQuestionAction,
  getFlowSizeQuestionAfterAction,
  getFlowSizeQuestionErrorAction,
  saveAnswerAction,
  saveAnswerAfterAction,
  saveAnswerErrorAction,
  selectActiveSizeAction,
  selectActiveSizeAfterAction,
  selectActiveSizeErrorAction,
  getCustomerSizesActions,
  getCustomerSizesAfter,
  getCustomerSizesError,
} from './actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { of } from 'rxjs';
import { graphqlFetchForCustomer as graphqlFetch } from '../../util';
import { createEffect, ofType, proxyFetch } from '../../../../util';
import {
  getCustomerDetailFailDueToToken,
  gotCustomerDetail,
} from '../account.actions';

const whenGetSizeQuestion$ = createEffect((action$, stateObservable) =>
  action$.pipe(
    ofType(getFlowSizeQuestionAction),
    switchMap(() =>
      fromPromise(proxyFetch({ type: 'get-set-of-questions' })).pipe(
        map((size_questions) => {
          return getFlowSizeQuestionAfterAction({ size_questions });
        }),
        catchError((error) => of(getFlowSizeQuestionErrorAction({ error })))
      )
    )
  )
);

const whenSaveAnswer$ = createEffect((action$, stateObservable) =>
  action$.pipe(
    ofType(saveAnswerAction),
    switchMap((value) =>
      fromPromise(
        graphqlFetch({
          query: `mutation saveAnswerOfQuestions($name: String!, $answers: [AnswerInput]) {
          saveAnswerOfQuestions(input: {name: $name, answers: $answers}) {
            label
            value
          }
        }
        `,
          variables: {
            name: value.payload.name,
            answers: value.payload.answers,
          },
        })
      ).pipe(
        map((data) => {
          return saveAnswerAfterAction({ data });
        }),
        catchError((error) => of(saveAnswerErrorAction({ error })))
      )
    )
  )
);

const whenSelectActiveSize$ = createEffect((action$) =>
  action$.pipe(
    ofType(selectActiveSizeAction),
    switchMap((value) =>
      fromPromise(
        graphqlFetch({
          query: `mutation setSizesToCustomer($shirt_size: String!, $pants_size: String!, $size_id: String!) {
          setSizesToCustomer(shirt_size: $shirt_size, pants_size: $pants_size, size_id: $size_id) {
            status
          }
        }          
        `,
          variables: {
            shirt_size: value.payload.shirt_size,
            pants_size: value.payload.pants_size,
            size_id: value.payload.size_id,
          },
        })
      ).pipe(
        map((data) => {
          return selectActiveSizeAfterAction({ data });
        }),
        catchError((error) => of(selectActiveSizeErrorAction({ error })))
      )
    )
  )
);

const whenGetCustomerSize$ = createEffect((action$) =>
  action$.pipe(
    ofType(getCustomerSizesActions, gotCustomerDetail),
    switchMap(() =>
      fromPromise(
        graphqlFetch({
          query: `# expects bearer header to be set via context to return data
query getCustomer {
  customer {
      sizes{
        items{
          answer_id
          name
          active
          pants_size
          shirt_size
        }
      }
  }
}
`,
        })
      ).pipe(
        map((data: any) => {
          return getCustomerSizesAfter({
            sizes: data.customer.sizes.items,
          });
        }),
        catchError((error: any) => {
          // trường hợp token hết hạn hoặc invalid
          if (
            error?.response?.data?.customer === null &&
            error?.response?.status === 200
          ) {
            console.warn(
              'Customer token invalid. Will clear customer token data'
            );
            return of(getCustomerDetailFailDueToToken({}));
          }
          console.log(error);
          return of(
            getCustomerSizesError({
              error,
            })
          );
        })
      )
    )
  )
);

const afterSelectActiveSize$ = createEffect((action$) =>
  action$.pipe(
    ofType(selectActiveSizeAfterAction, saveAnswerAfterAction),
    map(() => {
      return getCustomerSizesActions();
    })
  )
);

export const MY_SIZE_EFFECTS = [
  whenGetSizeQuestion$,
  whenSaveAnswer$,
  whenSelectActiveSize$,
  whenGetCustomerSize$,
  afterSelectActiveSize$,
];
