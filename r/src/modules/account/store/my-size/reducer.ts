import { createBuilderCallback } from '../../../../util';
import { AccountState } from '../account.state';
import {
  getCustomerSizesActions,
  getCustomerSizesAfter,
  getCustomerSizesError,
  getFlowSizeQuestionAfterAction,
  saveAnswerAfterAction,
} from './actions';

export const mySizeBuilderCallBack = createBuilderCallback<AccountState>(
  (builder) => {
    builder
      .addCase(saveAnswerAfterAction, (state, action) => {})
      .addCase(getCustomerSizesActions, (state) => {
        state.loadingState.getCustomerSize = true;
      })
      .addCase(getCustomerSizesAfter, (state, action) => {
        state.customer_size = action.payload.sizes;
        state.loadingState.getCustomerSize = false;
      })
      .addCase(getCustomerSizesError, (state, action) => {
        state.loadingState.getCustomerSize = false;
      })
      .addCase(getFlowSizeQuestionAfterAction, (state, action) => {
        state.size_questions = action.payload.size_questions;
      });
  }
);
