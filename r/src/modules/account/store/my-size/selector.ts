import { AccountState } from '../account.state';

export const selectSizeQuestions = (state: { account: AccountState }) =>
  state.account.size_questions;

export const selectCustomerSizes = (state: { account: AccountState }) =>
  state.account.customer_size;
