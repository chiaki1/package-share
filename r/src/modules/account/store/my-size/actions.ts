import { createAction } from '../../../../util';

const PREFIX = 'MY_SIZE';

const GET_FLOW_SIZE_QUESTION = 'GET_FLOW_SIZE_QUESTION';
export const getFlowSizeQuestionAction = createAction(
  GET_FLOW_SIZE_QUESTION,
  PREFIX
);

const GET_FLOW_SIZE_QUESTION_AFTER = 'GET_FLOW_SIZE_QUESTION_AFTER';
export const getFlowSizeQuestionAfterAction = createAction<{
  size_questions: any[];
}>(GET_FLOW_SIZE_QUESTION_AFTER, PREFIX);

const GET_FLOW_SIZE_QUESTION_ERROR = 'GET_FLOW_SIZE_QUESTION_ERROR';
export const getFlowSizeQuestionErrorAction = createAction<{
  error: Error;
}>(GET_FLOW_SIZE_QUESTION_ERROR, PREFIX);

const SAVE_ANSWER = 'SAVE_ANSWER';
export const saveAnswerAction = createAction<{
  name: string;
  answers: any[];
}>(SAVE_ANSWER, PREFIX);

const SAVE_ANSWER_AFTER = 'SAVE_ANSWER_AFTER';
export const saveAnswerAfterAction = createAction<{
  data: any;
}>(SAVE_ANSWER_AFTER, PREFIX);

const SAVE_ANSWER_ERROR = 'SAVE_ANSWER_ERROR';
export const saveAnswerErrorAction = createAction<{
  error: Error;
}>(SAVE_ANSWER_ERROR, PREFIX);

const GET_CUSTOMER_SIZES = 'GET_CUSTOMER_SIZES';
export const getCustomerSizesActions = createAction<{
  name: string;
  answers: any[];
}>(GET_CUSTOMER_SIZES, PREFIX);

const GET_CUSTOMER_SIZES_AFTER = 'GET_CUSTOMER_SIZES_AFTER';
export const getCustomerSizesAfter = createAction<{
  sizes: any;
}>(GET_CUSTOMER_SIZES_AFTER, PREFIX);

const GET_CUSTOMER_SIZES_ERROR = 'GET_CUSTOMER_SIZES_ERROR';
export const getCustomerSizesError = createAction<{
  error: Error;
}>(GET_CUSTOMER_SIZES_ERROR, PREFIX);

const SELECT_ACTIVE_SIZE = 'SELECT_ACTIVE_SIZE';
export const selectActiveSizeAction = createAction<{
  shirt_size: string;
  pants_size: string;
  size_id: string;
}>(SELECT_ACTIVE_SIZE, PREFIX);

const SELECT_ACTIVE_SIZE_AFTER = 'SELECT_ACTIVE_SIZE_AFTER';
export const selectActiveSizeAfterAction = createAction<{
  data: any;
}>(SELECT_ACTIVE_SIZE_AFTER, PREFIX);

const SELECT_ACTIVE_SIZE_ERROR = 'SELECT_ACTIVE_SIZE_ERROR';
export const selectActiveSizeErrorAction = createAction<{
  error: Error;
}>(SELECT_ACTIVE_SIZE_ERROR, PREFIX);
