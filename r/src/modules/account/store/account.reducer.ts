import { createReducer } from '@reduxjs/toolkit';
import { accountStateFactory } from './account.state';
import {
  clearCustomerToken,
  emptyCustomerTokenInStorage,
  generateCustomerTokenAction,
  generateCustomerTokenFail,
  generateCustomerTokenSuccessAction,
  getCustomerReviewAfterAction,
  getRewardPointAfterAction,
  getStoreCreditAfterAction,
  gotCustomerDetail,
  gotCustomerTokenInStorage,
  registerCustomerByEmailPassword,
  registerCustomerByEmailPasswordFail,
  registerCustomerByEmailPasswordSuccess,
  socialLoginAction,
} from './account.actions';
import _ from 'lodash';
import { customerAddressBuilderCallBack } from './customer-address/reducer';
import { customerOrderBuilderCallBack } from './customer-order/reducer';
import { mySizeBuilderCallBack } from './my-size/reducer';
import { myWishlistBuilderCallback } from './wishlisht';
import { customerInfoReducer } from './customer-info/customer-info.reducer';

export const accountReducer = createReducer(
  accountStateFactory(),
  (builder) => {
    builder
      .addCase(generateCustomerTokenAction, (state) => {
        state.loadingState.generateToken = true;
      })
      .addCase(socialLoginAction, (state) => {
        state.loadingState.generateToken = true;
      })
      .addCase(generateCustomerTokenSuccessAction, (state) => {
        state.loadingState.generateToken = false;
      })
      .addCase(generateCustomerTokenFail, (state) => {
        state.loadingState.generateToken = false;
      })
      .addCase(gotCustomerTokenInStorage, (state, action) => {
        state.token = action.payload.token;
      })
      .addCase(emptyCustomerTokenInStorage, (state) => {
        state.token = undefined;
        state.customer = undefined;
        state.isResolvedCustomerState = true;
      })
      .addCase(gotCustomerDetail, (state, action) => {
        state.customer = action.payload?.customer;
        state.isResolvedCustomerState = true;
      })
      .addCase(clearCustomerToken, (state) => {
        state.token = undefined;
        state.customer = undefined;
        state.isResolvedCustomerState = true;
      })
      .addCase(getRewardPointAfterAction, (state, action) => {
        state.reward_points = action.payload.reward_points;
      })
      .addCase(getStoreCreditAfterAction, (state, action) => {
        state.store_credit = action.payload.store_credit;
      })
      .addCase(getCustomerReviewAfterAction, (state, action) => {
        state.reviews = action.payload.reviews;
      });

    builder
      .addCase(registerCustomerByEmailPassword, (state) => {
        state.loadingState.createAccount = true;
      })
      .addCase(registerCustomerByEmailPasswordSuccess, (state, action) => {})
      .addCase(registerCustomerByEmailPasswordFail, (state) => {
        state.loadingState.createAccount = false;
      });

    customerAddressBuilderCallBack(builder);
    customerOrderBuilderCallBack(builder);
    mySizeBuilderCallBack(builder);
    myWishlistBuilderCallback(builder);
    customerInfoReducer(builder);
  }
);
