import { createAction } from '../../../util';

const PREFIX = 'wishlist';

const GOT_WISHLIST_DETAIL = 'GOT_WISHLIST_DETAIL';
export const gotWishlistDetail = createAction<{
  data: any;
}>(GOT_WISHLIST_DETAIL, PREFIX);

const REMOVED_PRODUCTS_FROM_WISHLIST_AFTER =
  'REMOVED_PRODUCTS_FROM_WISHLIST_AFTER';
export const removedProductsFromWishlist = createAction<{
  wishlist: any;
}>(REMOVED_PRODUCTS_FROM_WISHLIST_AFTER, PREFIX);

const UPDATED_PRODUCT_IN_WISHLIST = 'UPDATED_PRODUCT_IN_WISHLIST';
export const updatedProductInWishlist = createAction<{
  wishlist: any;
}>(UPDATED_PRODUCT_IN_WISHLIST, PREFIX);

const ADD_SIMPLE_PRODUCTS_TO_CART = 'ADD_SIMPLE_PRODUCT_TO_CART';
export const addSimpleProductsToCart = createAction<{}>(
  ADD_SIMPLE_PRODUCTS_TO_CART,
  PREFIX
);

const ADD_SIMPLE_PRODUCTS_TO_CART_AFTER = 'ADD_SIMPLE_PRODUCT_TO_CART_AFTER';
export const addSimpleProductsToCartAfter = createAction<{}>(
  ADD_SIMPLE_PRODUCTS_TO_CART_AFTER,
  PREFIX
);

const ADD_SIMPLE_PRODUCTS_TO_CART_ERROR = 'ADD_SIMPLE_PRODUCT_TO_CART_ERROR';
export const addSimpleProductsToCartError = createAction<{
  error: Error;
}>(ADD_SIMPLE_PRODUCTS_TO_CART_ERROR, PREFIX);

const ADD_CONFIGURABLE_PRODUCTS_TO_CART = 'ADD_CONFIGURABLE_PRODUCTS_TO_CART';
export const addConfigurableProductsToCart = createAction<{}>(
  ADD_CONFIGURABLE_PRODUCTS_TO_CART,
  PREFIX
);

const ADD_CONFIGURABLE_PRODUCTS_TO_CART_AFTER =
  'ADD_CONFIGURABLE_PRODUCTS_TO_CART_AFTER';
export const addConfigurableProductsToCartAfter = createAction<{}>(
  ADD_CONFIGURABLE_PRODUCTS_TO_CART_AFTER,
  PREFIX
);

const ADD_CONFIGURABLE_PRODUCTS_TO_CART_ERROR =
  'ADD_CONFIGURABLE_PRODUCTS_TO_CART_ERROR';
export const addConfigurableProductsToCartError = createAction<{
  error: Error;
}>(ADD_CONFIGURABLE_PRODUCTS_TO_CART_ERROR, PREFIX);
