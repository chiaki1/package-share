export interface WishlistState {
  wishlists: any[];
  loadingState: {
    getWishlistDetail: boolean;
  };
}

export const wishlistStateFactory = (): WishlistState => ({
  wishlists: [],
  loadingState: {
    getWishlistDetail: false,
  },
});
