import { createReducer } from '@reduxjs/toolkit';
import _ from 'lodash';
import { clearCustomerTokenAfter } from '../../account';
import {
  gotWishlistDetail,
  removedProductsFromWishlist,
  updatedProductInWishlist,
} from './actions';
import { wishlistStateFactory } from './state';

export const wishlistReducer = createReducer(
  wishlistStateFactory(),
  (builder) => {
    builder
      .addCase(gotWishlistDetail, (state, action) => {
        state.wishlists = action.payload.data;
      })
      .addCase(removedProductsFromWishlist, (state, action) => {
        state.wishlists = _.map(state.wishlists, (item) => {
          if (item.id === action.payload.wishlist.id) {
            return action.payload.wishlist;
          } else {
            return item;
          }
        });
      })
      .addCase(updatedProductInWishlist, (state, action) => {
        state.wishlists = _.map(state.wishlists, (item) => {
          if (item.id === action.payload.wishlist.id) {
            return action.payload.wishlist;
          } else {
            return item;
          }
        });
      })
      .addCase(clearCustomerTokenAfter, (state) => {
        state.wishlists = [];
      });
  }
);
