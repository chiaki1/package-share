import { WishlistState } from './state';

export const selectWishlist = (state: { wishlist: WishlistState }) =>
  state.wishlist;
