export interface WithWishlistActionsProps {
  actions: {
    getWishlistDetail: () => void;
  };
}

export interface WithWishlistStateProps {
  state: {
    wishlists: any[];
    isLoadingWishlist: boolean;
  };
}

export interface WithRemoveProductsFromWishlistProps {
  actions: {
    removeProductsFromWishlist: (
      wishlistId: string,
      wishlistItemsIds: string | string[]
    ) => void;
  };
}

export interface WithUpdateProductsInWishlistProps {
  actions: {
    updateProductsInWishlist: () => void;
  };
}
