import { useUpdateProductsInWishlist } from '../hook';
import { createUiHOC } from '@vjcspy/ui-extension';

export const withUpdateProductsInWishlist = createUiHOC(() => {
  return useUpdateProductsInWishlist();
}, 'withUpdateProductsInWishlist');
