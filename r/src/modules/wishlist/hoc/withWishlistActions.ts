import { useWishlistActions } from '../hook';
import { createUiHOC } from '@vjcspy/ui-extension';

export const withWishlistActions = createUiHOC(() => {
  return useWishlistActions();
}, 'withWishlistActions');
