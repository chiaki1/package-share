import { useWishlistState } from '../hook';
import { createUiHOC } from '@vjcspy/ui-extension';

export const withWishlistState = createUiHOC(() => {
  return useWishlistState();
}, 'withWishlistState');
