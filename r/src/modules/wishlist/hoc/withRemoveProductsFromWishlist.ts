import { useRemoveProductsFromWishlist } from '../hook';
import { createUiHOC } from '@vjcspy/ui-extension';

export const withRemoveProductsFromWishlist = createUiHOC(() => {
  return useRemoveProductsFromWishlist();
}, 'withRemoveProductsFromWishlist');
