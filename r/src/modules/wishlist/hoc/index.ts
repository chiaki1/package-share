import { withRemoveProductsFromWishlist } from './withRemoveProductsFromWishlist';
import { withUpdateProductsInWishlist } from './withUpdateProductsInWishlist';
import { withWishlistActions } from './withWishlistActions';
import { withWishlistState } from './withWislistState';

export const R_WISHLIST_HOCS = [
  withWishlistActions,
  withWishlistState,
  withRemoveProductsFromWishlist,
  withUpdateProductsInWishlist,
];
