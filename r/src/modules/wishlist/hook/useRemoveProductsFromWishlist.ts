import { useRemoveProductsFromWishlistMutation } from '@vjcspy/apollo';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { removedProductsFromWishlist } from '../store/actions';

export const useRemoveProductsFromWishlist = () => {
  const dispatch = useDispatch();

  const [query, res] = useRemoveProductsFromWishlistMutation();

  const removeProductsFromWishlist = useCallback(
    (wishlistId: string, wishlistItemsIds: string | string[]) => {
      query({
        variables: {
          wishlistId,
          wishlistItemsIds,
        },
      });
    },
    []
  );

  useEffect(() => {
    if (res.error) {
      console.warn('Could not remove product');
    }
    if (res.data?.removeProductsFromWishlist?.wishlist) {
      dispatch(
        removedProductsFromWishlist({
          wishlist: res.data.removeProductsFromWishlist?.wishlist,
        })
      );
    }
  }, [res.data, res.error]);

  return {
    actions: {
      removeProductsFromWishlist,
    },
  };
};
