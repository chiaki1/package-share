import { useSelector } from 'react-redux';
import { selectWishlist } from '../store/selector';

export const useWishlistState = () => {
  const wishlistState = useSelector(selectWishlist);

  const wishlists = wishlistState.wishlists;

  return {
    state: {
      wishlists,
    },
  };
};
