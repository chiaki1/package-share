import { useGetWishlistDetailLazyQuery } from '@vjcspy/apollo';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { gotWishlistDetail } from '../store/actions';

export const useWishlistActions = () => {
  const dispatch = useDispatch();

  const [getWishlistDetailQuery, getWishlistDetailRes] =
    useGetWishlistDetailLazyQuery({
      fetchPolicy: 'network-only',
    });

  const getWishlistDetail = useCallback(() => {
    getWishlistDetailQuery();
  }, []);

  useEffect(() => {
    if (getWishlistDetailRes.error) {
      console.warn('Could not get wishlist detail');
    }
    if (
      !!getWishlistDetailRes.data &&
      !!getWishlistDetailRes.data?.customer?.wishlists
    ) {
      dispatch(
        gotWishlistDetail({
          data: getWishlistDetailRes.data.customer?.wishlists,
        })
      );
    }
  }, [
    getWishlistDetailRes.data?.customer?.wishlists,
    getWishlistDetailRes.error,
  ]);

  return {
    actions: {
      getWishlistDetail,
    },
    state: {
      isLoadingWishlist: getWishlistDetailRes.loading,
    },
  };
};
