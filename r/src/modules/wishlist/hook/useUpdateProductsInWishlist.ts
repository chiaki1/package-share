import { useUpdateProductsInWishlistMutation } from '@vjcspy/apollo';
import { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { updatedProductInWishlist } from '../store/actions';

export const useUpdateProductsInWishlist = () => {
  const dispatch = useDispatch();

  const [query, res] = useUpdateProductsInWishlistMutation();

  const updateProductsInWishlist = useCallback((wishlistId, wishlistItems) => {
    query({
      variables: {
        wishlistId,
        wishlistItems,
      },
    });
  }, []);

  useEffect(() => {
    if (res.error) {
      console.warn('Could not remove product');
    }
    if (res.data?.updateProductsInWishlist?.wishlist) {
      dispatch(
        updatedProductInWishlist({
          wishlist: res.data.updateProductsInWishlist.wishlist,
        })
      );
    }
  }, [res.data, res.error]);

  return {
    actions: {
      updateProductsInWishlist,
    },
  };
};
