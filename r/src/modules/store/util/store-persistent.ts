import { AsyncPersistent } from '../../../util';

export const StorePersistent = new AsyncPersistent(undefined, 'chiaki_store');
