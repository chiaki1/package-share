import { createUiHOC } from '@vjcspy/ui-extension';
import { useSelector } from 'react-redux';
import { useMemo } from 'react';
import _ from 'lodash';
import { selectProductInfo } from '../../store';

export const withProductGallery = createUiHOC((props: any) => {
  // TODO: phải get được những attribute nào sẽ append image vào galery khi được chọn.
  const addToGalleryWhenHasAttributes = ['color'];

  if (!props.hasOwnProperty('product')) {
    console.info(
      'Could not found product data in props when install hoc withProductGallery'
    );
    return { gallery: [] };
  }

  const product: any = props.product;
  const productInfo: any = useSelector(selectProductInfo)(product['id']);

  const galleryData = useMemo(() => {
    let gallery: any[] = [];

    if (
      Array.isArray(product['media_gallery']) &&
      _.size(product.media_gallery) > 0
    ) {
      _.forEach(product.media_gallery, (g) => {
        if (g['disabled'] !== true) {
          gallery.push(g);
        }
      });
    }

    // Check if configurable
    if (product['__typename'] === 'ConfigurableProduct') {
      // Kiểm tra chọn những attribute được add vào gallery
      if (
        productInfo &&
        _.isObject(productInfo['configurable']) &&
        !_.isEmpty(productInfo.configurable['super_attribute']) &&
        productInfo['product']
      ) {
        const attributeIds = _.keys(
          productInfo.configurable['super_attribute']
        );

        _.forEach(attributeIds, (attrId) => {
          const option = _.find(
            productInfo.product['configurable_options'],
            (o) => o['attribute_id_v2'] == attrId
          );

          if (
            !!option &&
            _.includes(
              addToGalleryWhenHasAttributes,
              option['attribute_code']
            ) &&
            _.size(productInfo.configurable.variants) > 0
          ) {
            let isSelected = true;
            _.forEach(
              // @ts-ignore
              _.first(productInfo.configurable.variants).product.media_gallery,
              (g) => {
                if (g['disabled'] !== true) {
                  gallery.push({ ...g, isSelected });
                  isSelected = false;
                }
              }
            );
          }
        });
      }

      if (
        productInfo &&
        productInfo['configurable'] &&
        Array.isArray(productInfo.configurable['variants']) &&
        _.size(productInfo.configurable.variants) === 1
      ) {
        let isSelected = !_.find(gallery, (g) => g['isSelected'] === true);
        if (
          Array.isArray(
            // @ts-ignore
            _.first(productInfo.configurable?.variants).product?.media_gallery
          )
        ) {
          _.forEach(
            // @ts-ignore
            _.first(productInfo.configurable.variants).product.media_gallery,
            (g) => {
              if (g['disabled'] !== true) {
                gallery.push({ ...g, isSelected });
                isSelected = false;
              }
            }
          );
        }
      }
    }
    gallery = _.unionBy(gallery, (g) => g['url']);

    return {
      gallery,
    };
  }, [productInfo]);

  return { ...galleryData };
}, 'withProductGallery');
