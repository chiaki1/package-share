import { createUiHOC } from '@vjcspy/ui-extension';
import { useAggregationActions } from '../../hook/products/useAggregationActions';

export const withAggregationActions = createUiHOC(() => {
  return useAggregationActions();
}, 'withAggregationActions');
