import { createUiHOC } from '@vjcspy/ui-extension';
import { useFilterActions } from '../../hook/products/useFilterActions';

export const withStoreFilterActions = createUiHOC(() => {
  return useFilterActions();
}, 'withStoreFilterActions');
