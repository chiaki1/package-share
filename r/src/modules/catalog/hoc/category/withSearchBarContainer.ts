import { createUiHOC } from '@vjcspy/ui-extension';
import { useSearchBarContainer } from '../../hook/category/useSearchBarContainer';

export const withSearchBarContainer = createUiHOC(() => {
  return useSearchBarContainer();
}, 'withSearchBarContainer');
