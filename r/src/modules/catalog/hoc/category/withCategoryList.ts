import { createUiHOC } from '@vjcspy/ui-extension';
import { useCategoryList } from '../../hook/category/useCategoryList';

export const withCategoryList = createUiHOC(
  (props) => useCategoryList(props),
  'withCategoryList'
);
