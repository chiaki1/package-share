import { createUiHOC } from '@vjcspy/ui-extension';
import { useCategoryFromMgt } from '../../hook/category/useCategoryFromMgt';

export const withCategoryFromMgt = createUiHOC(
  (props) => useCategoryFromMgt(props),
  'withCategoryFromMgt'
);
