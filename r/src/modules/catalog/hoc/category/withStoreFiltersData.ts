import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { createUiHOC } from '@vjcspy/ui-extension';
import { selectFilters } from '../../store';
import { CatalogCategoryListingFilter } from '@vjcspy/apollo';

export const withStoreFiltersData = createUiHOC(() => {
  const storeFilters = useSelector(selectFilters);

  const filters: CatalogCategoryListingFilter[] | null = useMemo(() => {
    if (!storeFilters) {
      return null;
    } else if (Array.isArray(storeFilters)) {
      return [
        ...storeFilters.filter(
          (value: CatalogCategoryListingFilter) => value.code !== 'category_id'
        ),
      ];
    } else {
      return [];
    }
  }, [storeFilters]);

  return { filters };
}, 'withStoreFiltersData');
