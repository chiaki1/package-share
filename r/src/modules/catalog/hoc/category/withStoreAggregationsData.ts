import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import { createUiHOC } from '@vjcspy/ui-extension';
import { selectAggregations } from '../../store';

export const withStoreAggregationsData = createUiHOC(() => {
  const allAggregations = useSelector(selectAggregations);

  const aggregations: any = useMemo(
    () =>
      _.filter(
        allAggregations,
        (aggregation) =>
          !!aggregation['attribute_code'] &&
          aggregation['attribute_code'] !== 'category_id'
      ),
    [allAggregations]
  );

  return { aggregations };
}, 'withStoreAggregationsData');
