import { createUiHOC } from '@vjcspy/ui-extension';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { initProductInfo, selectProductInfo } from '../../store';
import { useConfigurableOption } from '../../hook/product/useConfigurableOption';

/**
 * Có thể gọi nhiều lần ở tất cả các component muốn lấy product info
 *
 * @type {UiHOC}
 */
export const withProductInfo = createUiHOC((props: { product: any }) => {
  const productInfo = useSelector(selectProductInfo)(props.product['id']);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!productInfo) {
      dispatch(initProductInfo({ product: props.product }));
    }
  }, [productInfo]);

  const configurableProductCtn = useConfigurableOption(
    productInfo,
    props.product
  );

  return {
    state: {
      productInfo,
    },
    fns: {
      configurable: {
        isOptionValueAvailable:
          configurableProductCtn.fns.isOptionValueAvailable,
        checkOption: configurableProductCtn.fns.checkOption,
      },
    },
    actions: {
      configurable: {
        toggleConfigurableOption:
          configurableProductCtn.actions.toggleConfigurableOptionAction,
      },
    },
  };
}, 'withProductInfo');
