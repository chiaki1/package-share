import { createUiHOC } from '@vjcspy/ui-extension';
import { useProductAdditionInfo } from '../../hook/product/useProductAdditionInfo';

export const withProductAdditionInfo = createUiHOC(() => {
  return useProductAdditionInfo();
}, 'withProductAdditionInfo');
