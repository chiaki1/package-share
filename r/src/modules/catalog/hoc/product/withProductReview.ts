import { createUiHOC } from '@vjcspy/ui-extension';
import { useProductReviews } from '../../hook/product/useProductReviews';

export const withProductReview = createUiHOC(() => {
  return useProductReviews();
}, 'withProductReview');
