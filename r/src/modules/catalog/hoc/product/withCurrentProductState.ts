import { createUiHOC } from '@vjcspy/ui-extension';
import { useCurrentProductState } from '../../hook/product/useCurrentProductState';

export const withCurrentProductState = createUiHOC(
  () => useCurrentProductState(),
  'withCurrentProductState'
);
