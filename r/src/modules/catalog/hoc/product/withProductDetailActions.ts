import { createUiHOC } from '@vjcspy/ui-extension';
import { useProductDetailActions } from '../../hook/product/useProductDetailActions';

export const withProductDetailActions = createUiHOC(
  () => useProductDetailActions(),
  'withProductDetailActions'
);
