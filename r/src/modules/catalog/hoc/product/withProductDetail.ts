import { createUiHOC } from '@vjcspy/ui-extension';
import { useProductDetail } from '../../hook/product/useProductDetail';

export const withProductDetail = createUiHOC(
  () => useProductDetail(),
  'withProductDetail'
);
