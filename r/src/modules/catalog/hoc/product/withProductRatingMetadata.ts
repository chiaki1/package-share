import { createUiHOC } from '@vjcspy/ui-extension';
import { useProductRatingMetadata } from '../../hook/product/useProductRatingMetadata';

export const withProductRatingMetadata = createUiHOC(() => {
  return useProductRatingMetadata();
}, 'withProductRatingMetadata');
