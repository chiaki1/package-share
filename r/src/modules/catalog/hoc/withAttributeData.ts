import { createUiHOC } from '@vjcspy/ui-extension';
import { useAttributeData } from '../hook/products/useAttributeData';

export const withAttributeData = createUiHOC((props) => {
  return useAttributeData(props['attributeCode']);
}, 'withAttributeData');
