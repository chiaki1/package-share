import { createUiHOC } from '@vjcspy/ui-extension';
import { useProductsContainer } from '../../hook/products/useProductsContainer';

export const withProductsContainer = createUiHOC(() => {
  return useProductsContainer();
}, 'withProductsContainer');
