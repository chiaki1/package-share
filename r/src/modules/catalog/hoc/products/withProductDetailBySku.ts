import { createUiHOC } from '@vjcspy/ui-extension';
import { useProductDetailBySku } from '../../hook/products/useProductDetailBySku';

export const withProductDetailBySku = createUiHOC((props) => {
  return useProductDetailBySku();
}, 'withProductDetailBySku');
