import { createUiHOC } from '@vjcspy/ui-extension';
import { useProductContainerActions } from '../../hook/products/useProductContainerActions';

export const withProductContainerActions = createUiHOC(
  () => useProductContainerActions(),
  'withProductContainerActions'
);
