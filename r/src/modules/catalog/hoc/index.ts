import { withAttributeData } from './withAttributeData';
import { withAggregationActions } from './category/withAggregationActions';
import { withProductGallery } from './category/withProductGallery';
import { withProductInfo } from './category/withProductInfo';
import { withStoreAggregationsData } from './category/withStoreAggregationsData';
import { withStoreFilterActions } from './category/withStoreFilterActions';
import { withStoreFiltersData } from './category/withStoreFiltersData';
import { withProductReview } from './product/withProductReview';
import { withProductRatingMetadata } from './product/withProductRatingMetadata';
import { withProductAdditionInfo } from './product/withProductAdditionInfo';
import { withCurrentProductState } from './product/withCurrentProductState';
import { withProductsContainer } from './products/withProductsContainer';
import { withProductsState } from './products/withProductsData';
import { withProductDetailBySku } from './products/withProductDetailBySku';
import { withSearchBarContainer } from './category/withSearchBarContainer';
import { withPriceFormat } from './product/withPriceFormat';
import { withProductDetail } from './product/withProductDetail';
import { withCategoryFromMgt } from './category/withCategoryFromMgt';
import { withCategoryList } from './category/withCategoryList';
import { withProductDetailActions } from './product/withProductDetailActions';
import { withProductContainerActions } from './products/withProductContainerActions';

export const R_CATALOG_HOC = [
  withAttributeData,
  withAggregationActions,
  withProductGallery,
  withProductInfo,
  withStoreAggregationsData,
  withStoreFilterActions,
  withStoreFiltersData,
  withProductReview,
  withProductRatingMetadata,
  withProductAdditionInfo,
  withCurrentProductState,
  withProductsContainer,
  withProductsState,
  withProductDetailBySku,
  withSearchBarContainer,
  withPriceFormat,
  withProductDetail,
  withCategoryFromMgt,
  withCategoryList,
  withProductDetailActions,
  withProductContainerActions,
];
