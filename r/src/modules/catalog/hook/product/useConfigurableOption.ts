import { useCallback } from 'react';
import _ from 'lodash';
import { useDispatch } from 'react-redux';
import { resolveVariants } from '../../util';
import { toggleConfigurableOption } from '../../store';

export const useConfigurableOption = (productInfo: any, product: any) => {
  const dispatch = useDispatch();

  /**
   * Kiểm tra xem option value có simple product tương ứng khả dụng hay không
   *
   * @type {(optionId: any, optionValue: any) => (boolean | boolean)}
   */
  const isOptionValueAvailable = useCallback(
    (optionId: any, optionValue: any) => {
      if (!productInfo || typeof productInfo.configurable === 'undefined') {
        return true;
      }

      let isAvailable = false;
      const d = { ...productInfo.configurable.super_attribute };
      delete d[optionId];
      if (_.isEmpty(d)) {
        return true;
      }

      const option = _.find(
        product['configurable_options'],
        (o: any) => o['attribute_id_v2'] == optionId
      );

      if (option) {
        const variants = resolveVariants(d, product);
        _.forEach(variants, (v: any) => {
          if (isAvailable) return false;

          const isExisted = _.find(
            v['attributes'],
            (attr: any) =>
              attr['code'] == option['attribute_code'] &&
              attr['value_index'] == optionValue
          );

          if (isExisted) {
            isAvailable = true;

            return false;
          }
        });
      }

      return isAvailable;
    },
    [productInfo]
  );

  const toggleConfigurableOptionAction = useCallback(
    (optionId: any, optionValue: any) => {
      dispatch(
        toggleConfigurableOption({
          productId: product['id'],
          optionId,
          optionValue,
        })
      );
    },
    [product?.id]
  );

  const checkOption = useCallback(() => {
    if (!productInfo || typeof productInfo.configurable === 'undefined') {
      return false;
    }
    let isCheck = true;
    if (productInfo?.configurable?.super_attribute) {
      product.configurable_options.map((option: any) => {
        if (
          typeof productInfo?.configurable?.super_attribute[
            option.attribute_id_v2
          ] === 'undefined'
        ) {
          isCheck = false;
        }
      });
    }
    return isCheck;
  }, [productInfo]);

  return {
    fns: { isOptionValueAvailable, checkOption },
    actions: { toggleConfigurableOptionAction },
  };
};
