import { useSelector } from 'react-redux';
import { selectProduct } from '../../store';
import { useEffect, useState } from 'react';
import { useGetProductAdditionInfomationLazyQuery } from '@vjcspy/apollo';
import _ from 'lodash';

export const useProductAdditionInfo = () => {
  const product = useSelector(selectProduct);
  const [productAdditionInfo, setProductAdditionInfo] = useState<any>();
  const [
    productAdditionInfoQuery,
    productAdditionInfoRes,
  ] = useGetProductAdditionInfomationLazyQuery();

  useEffect(() => {
    if (product?.sku) {
      productAdditionInfoQuery({
        variables: {
          sku: product.sku,
        },
      });
    }
  }, [product?.sku]);

  useEffect(() => {
    if (productAdditionInfoRes.error) {
      console.warn('Could not get product addition information');
    }

    if (
      _.isString(productAdditionInfoRes.data?.productAdditonInformation?.data)
    ) {
      try {
        setProductAdditionInfo(
          JSON.parse(
            productAdditionInfoRes.data!.productAdditonInformation!.data!
          )
        );
      } catch (e) {}
    }
  }, [productAdditionInfoRes.data, productAdditionInfoRes.error]);

  return {
    state: { productAdditionInfo },
  };
};
