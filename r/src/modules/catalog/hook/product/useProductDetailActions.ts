import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { setProductInfoQtyAction } from '../../store';

export const useProductDetailActions = () => {
  const dispatch = useDispatch();

  const setProductInfoQty = useCallback((productId: any, qty: number) => {
    dispatch(
      setProductInfoQtyAction({
        productId,
        qty,
      })
    );
  }, []);

  return {
    actions: {
      setProductInfoQty,
    },
  };
};
