import { useDispatch, useSelector } from 'react-redux';
import {
  createProductRatingAction,
  getRatingMetadataAfterAction,
  selectRatingMetadata,
} from '../../store';
import { useGetProductReviewRatingMetaDataLazyQuery } from '@vjcspy/apollo';
import { useCallback, useEffect } from 'react';

export const useProductRatingMetadata = () => {
  const dispatch = useDispatch();

  const ratingMetadata = useSelector(selectRatingMetadata);
  const [
    ratingMetadataQuery,
    ratingMetaDataResult,
  ] = useGetProductReviewRatingMetaDataLazyQuery();

  useEffect(() => {
    ratingMetadataQuery();
  }, []);

  useEffect(() => {
    if (ratingMetaDataResult?.error) {
      console.warn('Could not get rating metadata');
    }

    if (ratingMetaDataResult?.data) {
      dispatch(
        getRatingMetadataAfterAction({
          ratingMetadata:
            ratingMetaDataResult?.data.productReviewRatingsMetadata,
        })
      );
    }
  }, [ratingMetaDataResult?.data, ratingMetaDataResult?.error]);

  const createRating = useCallback(
    (rating: {
      nickname: string;
      summary: string;
      text: string;
      ratingInfo: any;
    }) => {
      dispatch(createProductRatingAction(rating));
    },
    []
  );

  return {
    state: {
      ratingMetadata,
    },
    actions: {
      createRating,
    },
  };
};
