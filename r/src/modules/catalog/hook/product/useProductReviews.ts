import { useCallback, useEffect, useMemo } from 'react';
import { useGetProductReviewsBySkuLazyQuery } from '@vjcspy/apollo';
import _ from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import {
  getProductReviewPageAction,
  getProductReviewPageAfterAction,
  selectProduct,
  selectProductReviewRequestPage,
  selectProductReviews,
} from '../../store';

export const useProductReviews = () => {
  const product = useSelector(selectProduct);
  const productReviewData = useSelector(selectProductReviews);
  const productReviewRequestPage = useSelector(selectProductReviewRequestPage);

  const dispatch = useDispatch();
  const [
    productReviewQuery,
    productReviewResult,
  ] = useGetProductReviewsBySkuLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (product?.sku) {
      productReviewQuery({
        variables: {
          sku: product.sku,
          pageSize: 50,
          currentPage: 1,
        },
      });
    }
  }, [
    product?.sku,
    productReviewRequestPage?.pageSize,
    productReviewRequestPage?.currentPage,
  ]);

  useEffect(() => {
    if (productReviewResult.error) {
      console.error(
        'Could not get product review data',
        productReviewResult.error
      );
    }
    if (
      productReviewResult.data &&
      _.isArray(productReviewResult.data.products?.items) &&
      _.first(productReviewResult.data.products?.items)
    ) {
      dispatch(
        getProductReviewPageAfterAction({
          productReviewData: _.first(productReviewResult.data.products?.items),
        })
      );
    }
  }, [productReviewResult?.data, productReviewResult?.error]);

  const doGetProductReviewNextPage = useCallback(
    (currentPage: number, pageSize = 5) => {
      dispatch(
        getProductReviewPageAction({
          currentPage,
          pageSize,
        })
      );
    },
    []
  );

  const productReviewCountInfo = useMemo(() => {
    const reviews = productReviewData?.reviews?.items ?? [];
    const count: any = {};
    if (Array.isArray(reviews)) {
      reviews.forEach((value: any) => {
        count[Math.ceil(parseFloat(value['average_rating']) / 20) + ''] =
          (count[Math.ceil(parseFloat(value['average_rating']) / 20) + ''] ??
            0) + 1;
      });
    }

    return { count };
  }, [productReviewData]);

  return {
    state: {
      productReviewData,
      productReviewCountInfo,
    },
    actions: {
      doGetProductReviewNextPage,
    },
  };
};
