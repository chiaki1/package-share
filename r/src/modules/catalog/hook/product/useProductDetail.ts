import { useProductDetailBySkuLazyQuery } from '@vjcspy/apollo';
import _ from 'lodash';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { gotProductData } from '../../store';

export const useProductDetail = () => {
  const [
    productDetailQuery,
    productDetailResponse,
  ] = useProductDetailBySkuLazyQuery();

  const [product, setProduct] = useState<any>();
  const dispatch = useDispatch();

  const queryProductDetailBySky = useCallback((sku: string) => {
    productDetailQuery({
      variables: {
        sku,
      },
    });
  }, []);

  useEffect(() => {
    if (productDetailResponse.error) {
      console.error('Could not query product detail by sku');
    }

    if (
      _.isArray(productDetailResponse.data?.products?.items) &&
      _.size(productDetailResponse.data?.products?.items) === 1
    ) {
      setProduct(_.first(productDetailResponse.data?.products?.items));
      dispatch(
        gotProductData({
          product: _.first(productDetailResponse.data?.products?.items),
        })
      );
    }
  }, [productDetailResponse]);

  return {
    actions: { queryProductDetailBySky },
    state: {
      product,
    },
  };
};
