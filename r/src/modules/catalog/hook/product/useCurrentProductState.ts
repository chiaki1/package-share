import { useSelector } from 'react-redux';
import { selectProduct, selectProductInfo } from '../../store';

export const useCurrentProductState = () => {
  const product = useSelector(selectProduct);
  const productInfo = useSelector(selectProductInfo)(product);

  return {
    state: {
      product,
      productInfo,
    },
  };
};
