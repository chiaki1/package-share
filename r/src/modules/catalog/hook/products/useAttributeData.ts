import { useEffect } from 'react';
import _ from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { selectAttribute, productsGotAttribute } from '../../store';
import { useGetNavigatorAttributeFilterDataLazyQuery } from '@vjcspy/apollo';

/**
 * Retrieve attribute data
 * @param attributeCode
 * @returns {{attribute: any}}
 */
export const useAttributeData = (attributeCode: string) => {
  const selectAttributeFn = useSelector(selectAttribute);
  const attributeInStore = selectAttributeFn(attributeCode);
  const dispatch = useDispatch();

  /*
   * IMPROVE PERFORMANCE/CODE QUALITY BY SAVE ATTRIBUTE TO STATE
   *
   * Lưu attribute data vào trong state -> query
   * Bởi vì sau khi query thì đã lưu attribute vào cache nên không cần thiết phải implement thêm cache ở đây
   * */
  const [attributeQuery, attributeRes] =
    useGetNavigatorAttributeFilterDataLazyQuery();

  useEffect(() => {
    if (!!attributeCode) {
      if (!attributeInStore) {
        attributeQuery({
          variables: {
            code: attributeCode,
          },
        });
      }
    }
  }, [attributeCode]);

  useEffect(() => {
    if (
      attributeRes.data &&
      Array.isArray(attributeRes.data.customAttributeMetadata?.items) &&
      _.size(attributeRes.data.customAttributeMetadata?.items) === 1
    ) {
      // @ts-ignore
      dispatch(
        productsGotAttribute({
          attribute: _.first(attributeRes.data!.customAttributeMetadata!.items),
        })
      );
    }
  }, [attributeRes.data]);

  return {
    attribute: attributeInStore,
  };
};
