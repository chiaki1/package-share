import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { productsToggleAggregationItem } from '../../store';

export const useAggregationActions = () => {
  const dispatch = useDispatch();

  const toggleAggregationItem = useCallback(
    (attributeCode: string, attributeValue: any) => {
      dispatch(
        productsToggleAggregationItem({
          attributeCode,
          attributeValue,
        })
      );
    },
    []
  );
  return {
    actions: { toggleAggregationItem },
  };
};
