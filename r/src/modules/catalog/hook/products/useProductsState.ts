import { useSelector } from 'react-redux';
import { selectAggregations, selectProducts } from '../../store';

export const useProductsState = () => {
  const products = useSelector(selectProducts);
  const aggregations = useSelector(selectAggregations);
  return {
    productsState: products,
    products,
    aggregations,
  };
};
