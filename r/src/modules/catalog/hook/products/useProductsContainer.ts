import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SortEnum, useGetCategoryListingDataLazyQuery } from '@vjcspy/apollo';
import {
  productsGotData,
  selectAggregations,
  selectProducts,
  selectSearchString,
} from '../../store';
import { useResolveProductsFilters } from './useResolveProductsFilters';
import _ from 'lodash';
import lodash from 'lodash';

export const useProductsContainer = () => {
  const { filters } = useResolveProductsFilters();
  const dispatch = useDispatch();
  const products = useSelector(selectProducts);
  const aggregations = useSelector(selectAggregations);
  const searchString = useSelector(selectSearchString);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(-1);
  const [totalCount, setTotalCount] = useState(-1);

  const [getCategoryListingData, getCategoryListingDataResponse] =
    useGetCategoryListingDataLazyQuery({
      fetchPolicy: 'cache-and-network',
    });

  const aggregationsWithoutCategory = useMemo(() => {
    return _.filter(
      aggregations,
      (aggregation) => aggregation['attribute_code'] !== 'category_id'
    );
  }, [aggregations]);

  // const debounceRunQueryGetList = useMemo(
  //   () =>
  //     lodash.debounce((variables: any) => {
  //       console.log('run query products', variables);
  //       getCategoryListingData({ variables });
  //     }, 50),
  //   []
  // );

  const debounceRunQueryGetList = useCallback(
    (variables) => getCategoryListingData({ variables }),
    []
  );

  const debounceLoadMorePage = useMemo(() => {
    return lodash.debounce(() => {
      setCurrentPage(currentPage + 1);
    }, 250);
  }, [currentPage]);

  const handleLoadMorePage = useCallback(() => {
    if (currentPage < totalPage || totalPage < 0) {
      if (!getCategoryListingDataResponse.loading) {
        debounceLoadMorePage();
      }
    }
  }, [currentPage, totalPage, getCategoryListingDataResponse.loading]);

  useEffect(() => {
    const isExistedCategoryFilter = _.find(
      filters,
      (fData) => fData.code === 'category_id'
    );

    if (isExistedCategoryFilter || !_.isEmpty(searchString)) {
      debounceRunQueryGetList({
        search: searchString ?? '',
        currentPage: currentPage,
        filters,
        pageSize: 10,
        sort: {
          position: SortEnum.Asc,
        },
      });
    }
  }, [filters, searchString, currentPage]);

  // save to store
  useEffect(() => {
    if (getCategoryListingDataResponse.error) {
      console.error('Could not query getCategoryListingDataResponse');
    }
    if (getCategoryListingDataResponse.data?.catalogCategoryListingData) {
      if (
        _.isNumber(
          getCategoryListingDataResponse.data.catalogCategoryListingData
            .page_info?.total_pages
        )
      ) {
        setTotalPage(
          getCategoryListingDataResponse.data.catalogCategoryListingData
            .page_info!.total_pages!
        );
        setTotalCount(
          getCategoryListingDataResponse.data.catalogCategoryListingData
            .total_count!
        );
      }

      dispatch(
        productsGotData({
          products:
            getCategoryListingDataResponse.data.catalogCategoryListingData
              ?.items!,
          aggregations:
            getCategoryListingDataResponse.data.catalogCategoryListingData
              ?.aggregations!,
          mergeWithExisting: true,
          pageInfo:
            getCategoryListingDataResponse.data.catalogCategoryListingData!
              .page_info!,
        })
      );
    }
  }, [
    getCategoryListingDataResponse.data,
    getCategoryListingDataResponse.error,
  ]);

  return {
    state: {
      products,
      aggregations: aggregationsWithoutCategory,
      isSearching: !!searchString,
      isLoading:
        currentPage < totalPage ||
        totalPage == -1 ||
        getCategoryListingDataResponse.loading,
      currentPage,
      totalPage,
      totalCount,
    },
    actions: {
      handleLoadMorePage,
    },
  };
};
