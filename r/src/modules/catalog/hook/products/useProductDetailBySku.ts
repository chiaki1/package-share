import { useCallback, useEffect } from 'react';
import {
  SortEnum,
  useProductDetailBySkuLazyQuery,
  useProductDetailLazyQuery,
} from '@vjcspy/apollo';
import { productsGotData } from '../../store';

export const useProductDetailBySku = () => {
  const [
    getProductDetailQuery,
    getProductDetailRes,
  ] = useProductDetailBySkuLazyQuery();

  const getProductDetailBySku = useCallback((sku) => {
    getProductDetailQuery({
      variables: {
        sku: sku,
      },
    });
  }, []);

  useEffect(() => {
    if (getProductDetailRes.error) {
      console.warn('Could not get product addition information');
    }

    console.log('data3232323', getProductDetailRes.data);
    // dispatch(
    //   productsGotData({
    //     products: getProductDetailRes.data?.items!,
    //     aggregations: getProductDetailRes.data?.catalogCategoryListingData
    //       ?.aggregations!,
    //   })
    // );
  }, [getProductDetailRes.data, getProductDetailRes.error]);

  return {
    actions: {
      getProductDetailBySku: getProductDetailBySku,
    },
    state: {
      productDetailData: getProductDetailRes.data,
    },
  };
};
