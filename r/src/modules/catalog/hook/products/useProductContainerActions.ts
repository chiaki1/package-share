import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { setPageFilterInfo } from '../../store';

export const useProductContainerActions = () => {
  const dispatch = useDispatch();
  const setFilterInfo = useCallback((pageFilterInfo: any) => {
    dispatch(
      setPageFilterInfo({
        pageFilterInfo,
      })
    );
  }, []);

  return {
    actions: {
      setFilterInfo,
    },
  };
};
