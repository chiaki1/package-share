import { useDomainContext } from '../../../domain';
import { useStoreContext } from '../../../store';
import { useGetChiakiConfigQuery } from '@vjcspy/apollo';
import { useEffect, useState } from 'react';
import { useCategoryList } from './useCategoryList';
import _ from 'lodash';
import { Registry } from '@vjcspy/chitility';

export const useCategoryFromMgt = (props: any) => {
  const categoryId = Registry.getInstance().registry('ROOT_CATEGORY_ID');
  const domainContextValue = useDomainContext();
  const storeContextValue = useStoreContext();

  const [configCategory, setConfigCategory] = useState({
    navigatorLevel: 2,
    isShowNavigatorTab: false,
  });

  const { data, error } = useGetChiakiConfigQuery({
    variables: {
      storeId: storeContextValue.storeData!.storeId,
      userId: domainContextValue.domainData.shopOwnerId,
      key: 'APP_NAVIGATOR_CFG',
    },
  });

  useEffect(() => {
    if (error) {
      console.error(
        'could not get useGetChiakiConfigQuery with key: APP_NAVIGATOR_CFG'
      );
    }
    if (data) {
      if (_.isArray(data?.chiakiConfig)) {
        const jsonData: any = _.first(data?.chiakiConfig);
        try {
          if (jsonData) {
            setConfigCategory(JSON.parse(jsonData.value));
          }
        } catch (e) {
          console.error(
            'Could not parse APP_NAVIGATOR_CFG data from chiaki config'
          );
        }
      } else {
        console.warn('Wrong data format APP_NAVIGATOR_CFG from chiaki config');
      }
    }
  }, [data, error]);

  const { state } = useCategoryList({ categoryId: categoryId, ...props });

  return {
    state: {
      configCategory,
      categoryList: state.categoryList,
    },
  };
};
