import {
  Aggregation,
  Attribute,
  CatalogCategoryListingFilter,
  CategoryTree,
  ProductInterface,
} from '@vjcspy/apollo';
import { ProductInfo } from '../store';

export interface WithStoreFilterActionsProps {
  actions: {
    removeFilterAction: (
      code: string,
      value: string,
      removeAllValue: boolean
    ) => void;
    addFilterAction: (code: string, value: string | string[]) => void;
    clearFilters: any;
    setSearchStringAction: any;
    removeSearchStringAction: any;
    addFilterNavigateCategory: (categoryId: string) => void;
  };
}

export interface WithStoreFiltersDataProps {
  filters: CatalogCategoryListingFilter[];
}

export interface WithSearchBarContainersProps {
  categories: any[];
  products: any[];
  message: string;
  handleSubmit: () => void;
  handleChange: (value: string) => void;
}

export interface WithProductsContainerProps {
  state: {
    products: any[];
    aggregations: any[];
    isSearching: boolean;
    isLoading: boolean;
    currentPage: number;
    totalPage: number;
    totalCount: number;
  };
  actions: {
    handleLoadMorePage: () => void;
  };
}

export interface WithAttributeDataProps {
  attribute: Attribute;
}

export interface WithCategoryListProps {
  state: {
    categoryList: CategoryTree;
  };
}

export interface WithCategoryFromMgtProps {
  state: {
    configCategory: any;
    categoryList: CategoryTree;
  };
}

export interface WithPriceFormatProps {
  priceFormat: (price: number) => string;
}

export interface WithProductInfoProps {
  state: {
    productInfo: any;
  };
  fns: {
    configurable: {
      isOptionValueAvailable: (optionId: any, optionValue: any) => boolean;
    };
  };
  actions: {
    configurable: {
      toggleConfigurableOption: (optionId: any, optionValue: any) => void;
    };
  };
}

export interface WithProductDetailProps {
  actions: { queryProductDetailBySky: (sku: string) => void };
  state: {
    product: ProductInterface;
  };
}

export interface WithProductDetailActionsProps {
  actions: {
    setProductInfoQty: (productId: any, qty: number) => void;
  };
}

export interface WithCurrentProductStateProps {
  state: {
    product: ProductInterface;
    productInfo: ProductInfo;
  };
}

export interface WithProductRatingMetadataProps {
  state: {
    ratingMetadata: any;
  };
  actions: {
    createRating: (rating: {
      nickname: string;
      summary: string;
      text: string;
      ratingInfo: any;
    }) => void;
  };
}

export interface WithProductReviewsProps {
  state: {
    productReviewData: any;
    productReviewCountInfo: any;
  };
  actions: {
    doGetProductReviewNextPage: (
      currentPage: number,
      pageSize?: number
    ) => void;
  };
}

export interface WithStoreAggregationsDataProps {
  aggregations: Aggregation[];
}

export interface WithAggregationActionsProps {
  actions: {
    toggleAggregationItem: (attributeCode: string, attributeValue: any) => void;
  };
}

export interface WithProductContainerActionsProps {
  actions: {
    setFilterInfo: (info: any) => void;
  };
}
