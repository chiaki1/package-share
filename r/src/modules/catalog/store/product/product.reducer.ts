import { createReducer } from '@reduxjs/toolkit';
import { productStateFactory } from './product.state';
import {
  getProductReviewPageAction,
  getProductReviewPageAfterAction,
  getRatingMetadataAfterAction,
  gotProductData,
} from './product.actions';

export const productReducer = createReducer(productStateFactory(), (builder) =>
  builder
    .addCase(gotProductData, (state, action) => {
      state.entity = action.payload.product;
    })
    .addCase(getProductReviewPageAfterAction, (state, action) => {
      if (typeof state.reviews === 'undefined') {
        state.reviews = {
          requestPage: {
            currentPage: 1,
            pageSize: 5,
          },
          data: action.payload.productReviewData,
        };
      } else {
        state.reviews.data = action.payload.productReviewData;
      }
    })
    .addCase(getProductReviewPageAction, (state, action) => {
      if (typeof state.reviews === 'undefined') {
        state.reviews = {
          requestPage: {
            currentPage: 1,
            pageSize: 5,
          },
        };
      } else {
        state.reviews.requestPage = {
          currentPage: action.payload.currentPage,
          pageSize: action.payload.pageSize,
        };
      }
    })
    .addCase(getRatingMetadataAfterAction, (state, action) => {
      state.ratingMetadata = action.payload.ratingMetadata;
    })
);
