import { ProductState } from './product.state';

export const selectProduct = (state: { product: ProductState }) =>
  state.product.entity;

export const selectProductReviews = (state: { product: ProductState }) =>
  state.product.reviews?.data;

export const selectProductReviewRequestPage = (state: {
  product: ProductState;
}) => state.product.reviews?.requestPage;

export const selectRatingMetadata = (state: { product: ProductState }) =>
  state.product.ratingMetadata;
