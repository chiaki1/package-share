import { createReducer } from '@reduxjs/toolkit';
import { ProductsStateFactory } from './products.state';
import {
  productsAddFilter,
  productsClearFilters,
  productsGetData,
  productsGotAttribute,
  productsGotCategoryData,
  productsGotData,
  productsRemoveFilter,
  productsResolvedFiltersData,
  productsToggleAggregationItem,
  removeSearchString,
  setPageFilterInfo,
  setSearchString,
} from './products.actions';
import _ from 'lodash';
import { Attribute, CatalogCategoryListingFilter } from '@vjcspy/apollo';

export const productsReducer = createReducer(
  ProductsStateFactory(),
  (builder) => {
    builder
      .addCase(productsGetData, (state) => {
        state.isUpdatingProducts = true;
      })
      .addCase(productsGotData, (state, action) => {
        if (!_.isArray(state.products)) {
          state.products = [];
        }

        if (action.payload.mergeWithExisting && action.payload.pageInfo) {
          state.products = _.filter(
            state.products,
            (p) =>
              p?.pageInfo?.current_page &&
              action.payload!.pageInfo!.current_page &&
              p?.pageInfo?.current_page !=
                action.payload!.pageInfo!.current_page
          );
          state.products.push(
            ..._.map(action.payload.products, (p: any) => {
              p['pageInfo'] = action.payload.pageInfo;

              return p;
            })
          );
          // _.uniqBy(state.products, (item) => item.sku);
        } else {
          state.products = action.payload.products;
        }

        state.aggregations = action.payload.aggregations;
        state.isUpdatingProducts = false;
      })
      .addCase(productsResolvedFiltersData, (state, action) => {
        state.filters = action.payload.filters;
      })
      .addCase(productsRemoveFilter, (state, action) => {
        if (_.isArray(state.filters)) {
          if (action.payload.removeAllValue) {
            state.filters = state.filters.filter(
              (filter) => filter.code !== action.payload.code
            );
          } else {
            const filterIndex = state.filters.findIndex(
              (filter) => filter.code === action.payload.code
            );

            if (filterIndex > -1) {
              if (state.filters[filterIndex].data.eq) {
                state.filters = state.filters.filter(
                  (filter) => filter.code !== action.payload.code
                );
              } else if (Array.isArray(state.filters[filterIndex].data.in)) {
                state.filters[filterIndex].data.in = state.filters[
                  filterIndex
                ].data!.in!.filter(
                  (value: any) => value != action.payload.value
                );
              }
            }
          }
        }
        state.isUpdatingProducts = true;
      })
      .addCase(productsToggleAggregationItem, (state, action) => {
        const attribute: Attribute | undefined = state.attributes.find(
          (a) => a.attribute_code === action.payload.attributeCode
        );
        /*
         * Không được tin tưởng filters trong state hiện tại bởi vì lúc resolve filters chưa có attribute data
         * Lúc add filter thì phải kiểm trả lại kiểu và set cho đúng
         * */

        if (!attribute) {
          return;
        }

        if (!Array.isArray(state.filters)) {
          state.filters = [];
        }
        let filter:
          | CatalogCategoryListingFilter
          | undefined = state.filters.find(
          (f) => f.code === action.payload.attributeCode
        );

        if (_.indexOf(['multiselect'], attribute.input_type) > -1) {
          if (filter) {
            if (Array.isArray(filter.data.in)) {
              if (
                _.indexOf(filter.data.in, action.payload.attributeValue) > -1
              ) {
                filter.data.in = _.filter(
                  filter.data.in,
                  (a) => a != action.payload.attributeValue
                );
              } else {
                filter.data.in.push(action.payload.attributeValue);
              }
            } else {
              const dataIn = [action.payload.attributeValue];
              if (typeof filter.data.eq !== 'undefined') {
                // first resolve filters
                if (filter.data.eq == action.payload.attributeValue) {
                  state.filters = _.filter(
                    state.filters,
                    (f) => f.code != action.payload.attributeCode
                  );
                } else {
                  dataIn.push(filter.data.eq);
                }
              }

              filter.data = {
                in: dataIn,
              };
            }
          } else {
            filter = {
              code: action.payload.attributeCode,
              data: { in: [action.payload.attributeValue] },
            };

            state.filters.push(filter);
          }
        } else {
          if (filter) {
            if (filter.data.eq == action.payload.attributeValue) {
              state.filters = _.filter(
                state.filters,
                (f) => f.code != action.payload.attributeCode
              );
            } else {
              filter.data = { eq: action.payload.attributeValue };
            }
          } else {
            filter = {
              code: action.payload.attributeCode,
              data: { eq: action.payload.attributeValue },
            };

            state.filters.push(filter);
          }
        }
        state.isUpdatingProducts = true;
      })
      .addCase(productsAddFilter, (state, action) => {
        if (!Array.isArray(state.filters)) {
          state.filters = [];
        }
        const filters = state.filters.filter(
          (f) => f.code !== action.payload.code
        );
        const data: any = {};
        if (_.isArray(action.payload.value)) {
          data['in'] = action.payload.value;
        } else {
          data['eq'] = action.payload.value;
        }

        filters.push({
          code: action.payload.code,
          data,
        });

        state.filters = filters;
        state.isUpdatingProducts = true;
      })
      .addCase(productsClearFilters, (state) => {
        state.filters = undefined;
        state.isUpdatingProducts = true;
      })
      .addCase(productsGotAttribute, (state, action) => {
        state.attributes.push(action.payload.attribute);

        state.attributes = _.uniqBy(state.attributes, 'attribute_code');
      })
      .addCase(productsGotCategoryData, (state, action) => {
        state.category = action.payload.category;
      })
      .addCase(setSearchString, (state, action) => {
        state.searchString = action.payload.searchString;

        // Hiện tại chưa support search trong category mà lúc nào cũng search all
        state.filters = undefined;
        state.products = [];
        state.aggregations = [];
      })
      .addCase(removeSearchString, (state) => {
        state.searchString = undefined;
        state.products = [];
        state.aggregations = [];
      })
      .addCase(setPageFilterInfo, (state, action) => {
        state.pageFilterInfo = {
          ...state.pageFilterInfo,
          ...action.payload.pageFilterInfo,
        };
      });
  }
);
