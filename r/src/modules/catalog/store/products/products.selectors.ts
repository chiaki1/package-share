import { ProductsState } from './products.state';
import { createSelector } from '@reduxjs/toolkit';
import _ from 'lodash';

export const selectAggregations = (state: { products: ProductsState }) =>
  state.products.aggregations;
export const selectFilters = (state: { products: ProductsState }) =>
  state.products.filters;

export const selectAttributes = (state: { products: ProductsState }) =>
  state.products.attributes;

export const selectAttribute = createSelector(selectAttributes, (attributes) =>
  _.memoize((attributeCode: string) =>
    attributes.find((a) => a.attribute_code === attributeCode)
  )
);

export const selectProducts = (state: { products: ProductsState }) =>
  state.products.products;

export const selectSearchString = (state: { products: ProductsState }) =>
  state.products.searchString;

export const selectIsUpdatingProducts = (state: { products: ProductsState }) =>
  state.products.isUpdatingProducts;

export const selectPageFilterInfo = (state: { products: ProductsState }) =>
  state.products.pageFilterInfo;
