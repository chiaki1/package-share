import {
  Attribute,
  CatalogCategoryListingFilter,
  SortEnum,
} from '@vjcspy/apollo';

export interface ProductsState {
  products: any[];
  aggregations: any[];
  searchString?: string;
  /*
   * Filters luôn nằm trên url,
   * Lúc đầu vào page thì resolve url cho vào state
   * Sau khi user thực hiện action thêm filter thì lại đẩy lên url
   */
  filters?: CatalogCategoryListingFilter[];
  attributes: Attribute[];
  category?: any;
  isUpdatingProducts?: boolean;
  pageFilterInfo: {
    sort: {
      position?: SortEnum;
      name?: SortEnum;
      price?: SortEnum;
    };
    currentPage: number;
  };
}

export const ProductsStateFactory = (): ProductsState => ({
  products: [],
  aggregations: [],
  attributes: [],
  pageFilterInfo: {
    sort: {
      position: SortEnum.Desc,
    },
    currentPage: 1,
  },
});
