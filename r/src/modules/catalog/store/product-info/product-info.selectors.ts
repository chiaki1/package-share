import { createSelector } from '@reduxjs/toolkit';
import { ProductInfoState } from './product-info.state';
import _ from 'lodash';

export const selectProductInfo = createSelector(
  (state: { productInfo: ProductInfoState }) => state.productInfo.products,
  (products) =>
    _.memoize((productOrId: any) =>
      _.find(products, (pInfo) =>
        typeof productOrId === 'object'
          ? pInfo.product['id'] == productOrId['id']
          : pInfo.product['id'] == productOrId
      )
    )
);
