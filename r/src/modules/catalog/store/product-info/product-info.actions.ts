import { createAction, generateAction } from '../../../../util';

const PREFIX = 'PRODUCT_INFO';

const INIT_PRODUCT_INFO = 'INIT_PRODUCT_INFO';
export const initProductInfo = createAction<{
  product: any;
}>(INIT_PRODUCT_INFO, PREFIX);

// CONFIGURABLE OPTIONS
const TOGGLE_CONFIGURABLE_OPTION = 'TOGGLE_CONFIGURABLE_OPTION';
export const toggleConfigurableOption = createAction<{
  productId: any;
  optionId: any;
  optionValue: any;
}>(TOGGLE_CONFIGURABLE_OPTION, PREFIX);

const setProductInfoQty = generateAction<{ productId: any; qty: number }, {}>(
  'SET_QTY',
  PREFIX
);
export const setProductInfoQtyAction = setProductInfoQty.ACTION;
export const setProductInfoQtyAfterAction = setProductInfoQty.AFTER;
export const setProductInfoQtyErrorAction = setProductInfoQty.ERROR;
