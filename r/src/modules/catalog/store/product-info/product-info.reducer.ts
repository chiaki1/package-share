import { createReducer } from '@reduxjs/toolkit';
import { ProductInfo, productInfoStateFactory } from './product-info.state';
import {
  initProductInfo,
  setProductInfoQtyAction,
  toggleConfigurableOption,
} from './product-info.actions';
import _ from 'lodash';
import { resolveProductPrice, resolveVariants } from '../../util';

export const productInfoReducer = createReducer(
  productInfoStateFactory(),
  (builder) =>
    builder
      .addCase(initProductInfo, (state, action) => {
        _.remove(
          state.products,
          (pInfo) => pInfo['id'] === action.payload.product['id']
        );

        const info: ProductInfo = {
          product: action.payload.product,
          id: action.payload.product['id'],
          qty: 1,
          priceRange: resolveProductPrice(action.payload.product),
        };

        if (action.payload.product['__typename'] === 'ConfigurableProduct') {
          info['configurable'] = {
            super_attribute: {},
          };
        }
        state.products.push(info);
      })
      .addCase(toggleConfigurableOption, (state, action) => {
        let productInfo: any = _.find(
          state.products,
          (pInfo: any) => pInfo.id == action.payload.productId
        );

        // Chắc chắn sẽ tồn tại productInfo.
        // Ngoài ra ở đây cũng không phải kiểm trà là configurable product hay không vì actions này chỉ xảy ra cho configurable product
        if (productInfo) {
          productInfo = { ...productInfo };
          if (!productInfo['configurable']) {
            productInfo.configurable = {
              super_attribute: {},
            };
          }

          // toggle option
          if (
            productInfo.configurable.super_attribute[action.payload.optionId] ==
            action.payload.optionValue
          ) {
            delete productInfo.configurable.super_attribute[
              action.payload.optionId
            ];
          } else {
            productInfo.configurable.super_attribute[action.payload.optionId] =
              action.payload.optionValue;
          }

          productInfo.priceRange = resolveProductPrice(
            productInfo.product,
            productInfo.configurable
          );
          productInfo.configurable['variants'] = resolveVariants(
            productInfo.configurable.super_attribute,
            productInfo.product
          );

          state.products = _.filter(
            state.products,
            (pInfo: any) => pInfo.id != action.payload.productId
          );

          state.products.push(productInfo);
        }
      })
      .addCase(setProductInfoQtyAction, (state, action) => {
        const productInfo: any = _.find(
          state.products,
          (pInfo) => pInfo.id == action.payload.productId
        );

        if (productInfo) {
          productInfo.qty = action.payload.qty;
        }
      })
);
