import _ from 'lodash';
import { CatalogCategoryListingFilter } from '@vjcspy/apollo';

export const queryToFilters = (query: any) => {
  const filters: CatalogCategoryListingFilter[] = [];
  if (_.isObject(query)) {
    _.forEach(query, (value, key) => {
      if (key === 'slug') {
        return true;
      }
      if (!_.isString(value) || value === '') {
        return true;
      }

      if (_.indexOf(value, ',') > -1) {
        filters.push({
          code: key,
          data: {
            in: _.split(value, ','),
          },
        });
      } else {
        filters.push({
          code: key,
          data: {
            eq: value,
          },
        });
      }
    });
  }
  return filters;
};
