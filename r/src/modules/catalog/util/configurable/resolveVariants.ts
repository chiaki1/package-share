import _ from 'lodash';

/**
 * Trả về variants cái mà thoả mãn với những option đã select
 *
 * @param configurableOptions
 * @param configurableProduct
 * @returns {any[]}
 */
export const resolveVariants = (
  configurableOptions: any,
  configurableProduct: any
) => {
  const variantsByOption: any = {};
  if (!Array.isArray(configurableProduct['variants'])) {
    console.error(
      'Could not resolve child products due to missing `variants` in configurable product'
    );
  }
  _.forEach(configurableOptions, (optionValue: any, optionId: any) => {
    variantsByOption[optionId] = _.filter(
      configurableProduct.variants,
      (variant: any) => {
        let isSatisfied = false;

        _.forEach(variant['attributes'], (variantAttr: any) => {
          const option = _.find(
            configurableProduct['configurable_options'],
            (o) => o['attribute_code'] == variantAttr['code']
          );

          if (option) {
            if (
              option['attribute_id_v2'] == optionId &&
              variantAttr['value_index'] == optionValue
            ) {
              isSatisfied = true;
            }
          }
        });

        return isSatisfied;
      }
    );
  });

  return _.intersectionBy(..._.values(variantsByOption), (variant: any) => {
    return variant['product']['id'];
  });
};
