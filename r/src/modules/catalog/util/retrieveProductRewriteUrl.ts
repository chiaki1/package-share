import _ from 'lodash';

export const retrieveProductRewriteUrl = (
  product: any,
  currentCategoryId?: number
) => {
  if (product && _.isArray(product['url_rewrites'])) {
    let urlRewriteData;
    if (typeof currentCategoryId === 'undefined') {
      urlRewriteData = _.find(
        product['url_rewrites'],
        (rw) => _.size(rw['parameters']) === 1
      );
    } else {
      urlRewriteData = _.find(product['url_rewrites'], (rw) => {
        if (_.isArray(rw['parameters'])) {
          const paramWithCat = _.find(
            rw['parameters'],
            (p) => p['name'] === 'category' && p['value'] == currentCategoryId
          );

          return !!paramWithCat;
        }
        return false;
      });
    }
    if (urlRewriteData) {
      return urlRewriteData['url'];
    } else {
      const fRewrite = _.first(product['url_rewrites']);
      if (fRewrite) {
        return fRewrite['url'];
      }
      console.warn(
        'cound not found url rewrite data',
        product,
        currentCategoryId
      );
    }
  } else {
    console.warn(
      'please check product data. Must have `url_rewrites` data',
      product
    );
  }

  return undefined;
};
