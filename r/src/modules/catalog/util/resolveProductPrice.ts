import { resolveVariants } from './configurable';
import _ from 'lodash';
import { PriceRange } from '@vjcspy/apollo';

/**
 * Trả về product price với input là buyRequest
 *
 * @param product
 * @param buyRequest
 * @returns {any}
 */
export const resolveProductPrice = (
  product: any,
  buyRequest?: {
    options?: any;
    super_attribute?: any;
    bundle_option?: any;
    bundle_option_qty?: any;
    super_group?: any;
  }
): PriceRange => {
  let price: any = product.price_range;

  if (
    product['__typename'] === 'ConfigurableProduct' &&
    buyRequest?.super_attribute
  ) {
    const variants = resolveVariants(buyRequest.super_attribute, product);
    if (_.size(variants) === 1) {
      const variant: any = _.first(variants);
      price = variant.product?.price_range ?? price;
    }
  }

  return price;
};
