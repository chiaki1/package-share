import { Cart } from '@vjcspy/apollo';
import { Registry } from '@vjcspy/chitility';
import _ from 'lodash';
import { CheckoutConstant } from '../constant';

export type CheckoutHookFn = (cart: Cart) => Promise<boolean>;
export interface CheckoutHookCfg {
  name: string;
  hookFn: CheckoutHookFn;
  priority: number;
  type: string;
}

export const addPlaceOrderHookFn = (
  name: string,
  hookFn: CheckoutHookFn,
  priority: number,
  type = 'after'
) => {
  let hookFns: CheckoutHookCfg[] =
    Registry.getInstance().registry(CheckoutConstant.CHECKOUT_HOOK_KEY) ?? [];
  hookFns = _.filter(hookFns, (fn) => fn.name !== name);
  hookFns.push({
    name,
    hookFn,
    priority,
    type,
  });

  hookFns = _.sortBy(hookFns, 'priority');
  Registry.getInstance().register(CheckoutConstant.CHECKOUT_HOOK_KEY, hookFns);
};
