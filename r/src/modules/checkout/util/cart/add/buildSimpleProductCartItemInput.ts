import { ProductInterface } from '@vjcspy/apollo';

export const buildSimpleProductCartItemInput = (
  product: ProductInterface,
  quantity = 1
) => {
  return {
    data: {
      quantity,
      sku: product.sku,
    },
  };
};
