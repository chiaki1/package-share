import _ from 'lodash';
import { resolveVariants } from '../../../../catalog';

export const buildConfigurableProductCartItemInput = (
  superAttribute: any,
  product: { sku: string; configurable_options: any[]; variants: any[] },
  quantity = 1
) => {
  const variants = resolveVariants(superAttribute, product);

  if (_.size(variants) === 1) {
    const variant = _.first(variants);
    if (variant.product) {
      return {
        parent_sku: product.sku,
        data: {
          quantity,
          sku: variant.product.sku,
        },
      };
    } else {
      console.warn('??? variant missing product data');
    }
  } else {
    console.warn(
      'Could not build configurable add to cart input',
      superAttribute,
      product
    );
  }

  return undefined;
};
