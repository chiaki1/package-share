import { AsyncPersistent } from '../../../util';

export const CheckoutPersistent = new AsyncPersistent(
  undefined,
  'chiaki_checkout'
);
