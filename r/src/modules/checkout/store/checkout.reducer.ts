import { combineReducers } from '@reduxjs/toolkit';
import { cartReducer } from './cart';

export const checkoutReducer = combineReducers({
  cart: cartReducer,
});
