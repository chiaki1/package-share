import {
  checkoutCartAddressSetBillingAddressAfter,
  getPaymentMethod,
  getPaymentMethodAfter,
  getPaymentMethodError,
  setPaymentMethod,
  setPaymentMethodAfter,
  setPaymentMethodError,
} from '../actions';
import {
  catchError,
  debounceTime,
  filter,
  map,
  mapTo,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import _ from 'lodash';
import { of } from 'rxjs';
import { UnknownResponseError } from '@vjcspy/chitility';
import { createEffect, ofType } from '../../../../../util';
import { graphqlFetchForCustomer } from '../../../../account';
import CartDetail from '../../../../graphql/schema/CartDetail';

const getPaymentMethod$ = createEffect((action$) =>
  action$.pipe(
    ofType(checkoutCartAddressSetBillingAddressAfter),
    mapTo(getPaymentMethod({}))
  )
);

const whenGetPaymentMethod$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(getPaymentMethod),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout.cart]),
    filter(([_action, cartState]) => _.isString(cartState.cart.id)),
    switchMap(([_action, cartState]) => {
      return fromPromise(
        graphqlFetchForCustomer({
          query: `query getAvailablePaymentMethod($cartId:String!){
    cart(cart_id: $cartId) {
        available_payment_methods {
            code
            title
        }
    }
}
`,
          variables: {
            cartId: cartState.cart.id,
          },
        })
      ).pipe(
        map((res) => {
          if (res?.cart) {
            return getPaymentMethodAfter({ cart: res.cart });
          } else {
            return getPaymentMethodError({
              error: new UnknownResponseError(
                'when get available payment method'
              ),
            });
          }
        }),
        catchError((error) => of(getPaymentMethodError({ error })))
      );
    })
  )
);

const whenSetPaymentMethod$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(setPaymentMethod),
    debounceTime(200),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout.cart]),
    filter(([_action, cartState]) => _.isString(cartState.cart.id)),
    switchMap(([action, cartState]) => {
      return fromPromise(
        graphqlFetchForCustomer({
          query: `
          mutation setPaymentMethod($cartId: String!, $code: String!){
    setPaymentMethodOnCart(input: {
        cart_id: $cartId
        payment_method: {
            code: $code
        }
    }) {
        cart {
            ${CartDetail}
        }
    }
}

          `,
          variables: {
            cartId: cartState.cart.id,
            code: action.payload.methodCode,
          },
        })
      ).pipe(
        map((res) => {
          if (
            _.isString(
              res.setPaymentMethodOnCart?.cart?.selected_payment_method?.code
            )
          ) {
            return setPaymentMethodAfter({
              cart: res.setPaymentMethodOnCart!.cart,
            });
          } else {
            return setPaymentMethodError({
              error: new UnknownResponseError('when set payment'),
            });
          }
        }),
        catchError((error) =>
          of(
            setPaymentMethodError({
              error,
            })
          )
        )
      );
    })
  )
);

export const CHECKOUT_CART_PAYMENT_METHOD = [
  getPaymentMethod$,
  whenGetPaymentMethod$,
  whenSetPaymentMethod$,
];
