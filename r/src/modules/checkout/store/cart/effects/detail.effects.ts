import {
  checkoutCartDetailAddCouponCodeAction,
  checkoutCartDetailAddCouponCodeAfterAction,
  checkoutCartDetailAddCouponCodeErrorAction,
  checkoutCartDetailRemoveCouponCodeAction,
  checkoutCartDetailRemoveCouponCodeAfterAction,
  checkoutCartDetailRemoveCouponCodeErrorAction,
  checkoutCartDetailRemoveItemAction,
  checkoutCartDetailRemoveItemAfterAction,
  checkoutCartDetailRemoveItemErrorAction,
  getCartDetail,
  getCartDetailAfter,
  getCartDetailError,
  updateCartItemAction,
  updateCartItemAfterAction,
  updateCartItemError,
} from '../actions';
import {
  catchError,
  debounceTime,
  map,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { RuntimeError } from '@vjcspy/chitility';
import { of } from 'rxjs';
import { createEffect, ofType } from '../../../../../util';
import { appRunTimeError } from '../../../../app';
import {
  AccountConstant,
  AccountPersistent,
  graphqlFetchForCustomer,
} from '../../../../account';
import CartDetail from '../../../../graphql/schema/CartDetail';
import { CheckoutConstant, CheckoutPersistent } from '../../../util';

const whenGetCartDetail$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(...[getCartDetail, updateCartItemError]),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout?.cart]),
    switchMap(([action, cart]) =>
      fromPromise(
        graphqlFetchForCustomer({
          // Lấy từ queries graphql. Nếu muốn sửa thì phải sửa trong folder queries trước
          query: `query getCartDetails($cartId: String!) {
    cart(cart_id: $cartId) {
        ${CartDetail}
    }
}
`,
          variables: {
            cartId:
              cart?.customerCartId ?? action.payload.cartId ?? cart.cart?.id,
          },
        })
      ).pipe(
        /*
         * TODO: handle khi get cart detail error
         * @see https://chiaki.atlassian.net/browse/KP-139
         * */
        map((data) => {
          if (data && data?.cart) {
            return getCartDetailAfter({
              cart: data.cart,
            });
          } else {
            return appRunTimeError({
              error: new RuntimeError('Wrong data format when get cart detail'),
            });
          }
        }),
        catchError((error: any) => {
          const clearDataWhenError = async () => {
            let currentErrorCount: any =
              parseInt(
                (await CheckoutPersistent.getItem(
                  CheckoutConstant.GET_CART_DETAIL_ERROR_COUNT
                )) ?? '0'
              ) ?? 0;

            if (currentErrorCount > 1) {
              await CheckoutPersistent.removeItem(
                CheckoutConstant.GUEST_CART_ID_KEY
              );
              await AccountPersistent.removeItem(AccountConstant.TOKEN_KEY);
            } else {
              await CheckoutPersistent.saveItem(
                CheckoutConstant.GET_CART_DETAIL_ERROR_COUNT,
                ++currentErrorCount
              );
            }
          };
          clearDataWhenError();
          return of(
            getCartDetailError({
              error,
            })
          );
        })
      )
    )
  )
);

const whenUpdateCartItem$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(updateCartItemAction),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout?.cart]),
    switchMap(([action, cart]) =>
      fromPromise(
        graphqlFetchForCustomer({
          query: `
         mutation updateItemInCart($cartId: String!, $itemId: Int!, $quantity: Float!) {
    updateCartItems(
        input: {
            cart_id: $cartId
            cart_items: [{ cart_item_id: $itemId, quantity: $quantity }]
        }
    ) {
        cart {
           ${CartDetail}
        }
    }
}
          `,
          variables: {
            cartId: cart?.customerCartId ?? action.payload.cartId,
            itemId: action.payload.cartItemId,
            quantity: action.payload.qty,
          },
        })
      ).pipe(
        map((data) => {
          if (data.updateCartItems?.cart) {
            return updateCartItemAfterAction({
              cart: data.updateCartItems.cart,
            });
          } else {
            return updateCartItemError({
              error: new RuntimeError('Wrong update cart item response data'),
              cartItemId: action.payload.cartItemId,
            });
          }
        }),
        catchError((error: any) =>
          of(
            updateCartItemError({
              error,
              cartItemId: action.payload.cartItemId,
            })
          )
        )
      )
    )
  )
);

const whenRemoveItemOnCart$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(checkoutCartDetailRemoveItemAction),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout?.cart]),
    switchMap(([action, cart]) =>
      fromPromise(
        graphqlFetchForCustomer({
          query: `mutation removeItemFromCart($cartId: String!, $itemId: Int!) {
    removeItemFromCart(input: { cart_id: $cartId, cart_item_id: $itemId })
    {
        cart{
            ${CartDetail}
        }
    }
}
`,
          variables: {
            cartId: cart?.customerCartId ?? action.payload.cartId,
            itemId: action.payload.cartItemId,
          },
        })
      ).pipe(
        map((data) => {
          if (data.removeItemFromCart?.cart) {
            return checkoutCartDetailRemoveItemAfterAction({
              cart: data.removeItemFromCart.cart,
            });
          } else {
            return checkoutCartDetailRemoveItemErrorAction({
              error: new RuntimeError('Wrong update cart item response data'),
              cartItemId: action.payload.cartItemId,
            });
          }
        }),
        catchError((error: any) =>
          of(
            checkoutCartDetailRemoveItemErrorAction({
              error,
              cartItemId: action.payload.cartItemId,
            })
          )
        )
      )
    )
  )
);

const whenAddCouponCode$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(checkoutCartDetailAddCouponCodeAction),
    debounceTime(500),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout?.cart]),
    switchMap(([action, cart]) =>
      fromPromise(
        graphqlFetchForCustomer({
          query: `
          mutation applyCouponToCart($cartId: String!,$couponCode: String!) {
    applyCouponToCart(
        input: {
            cart_id: $cartId,
            coupon_code: $couponCode
        }
    ) {
        cart {
            ${CartDetail}
        }
    }
}
`,
          variables: {
            cartId: cart?.customerCartId ?? action.payload.cartId,
            couponCode: action.payload.couponCode,
          },
        })
      ).pipe(
        map((data) => {
          if (data.applyCouponToCart?.cart) {
            return checkoutCartDetailAddCouponCodeAfterAction({
              cart: data.applyCouponToCart.cart,
            });
          } else {
            return checkoutCartDetailAddCouponCodeErrorAction({
              error: new RuntimeError('Wrong add coupon response data'),
            });
          }
        }),
        catchError((error: any) =>
          of(
            checkoutCartDetailAddCouponCodeErrorAction({
              error,
            })
          )
        )
      )
    )
  )
);

const whenRemoveCoupon$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(checkoutCartDetailRemoveCouponCodeAction),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout?.cart]),
    switchMap(([action, cart]) =>
      fromPromise(
        graphqlFetchForCustomer({
          query: `mutation removeCouponFromCart($cartId: String!) {
            removeCouponFromCart(input: {
              cart_id: $cartId
            }) {
              cart {
                  ${CartDetail}
                }
              }
          }
`,
          variables: {
            cartId: cart?.customerCartId ?? action.payload.cartId,
          },
        })
      ).pipe(
        map((data) => {
          if (
            !!data.removeCouponFromCart &&
            !!data?.removeCouponFromCart?.cart
          ) {
            return checkoutCartDetailRemoveCouponCodeAfterAction({
              cart: data.removeCouponFromCart.cart,
            });
          } else {
            return checkoutCartDetailRemoveCouponCodeErrorAction({
              error: new RuntimeError('Wrong remove coupon from cart'),
            });
          }
        }),
        catchError((error: any) =>
          of(
            checkoutCartDetailRemoveCouponCodeErrorAction({
              error,
            })
          )
        )
      )
    )
  )
);

export const CHECKOUT_CART_DETAIL_EFFECTS = [
  whenGetCartDetail$,
  whenUpdateCartItem$,
  whenRemoveItemOnCart$,
  whenAddCouponCode$,
  whenRemoveCoupon$,
];
