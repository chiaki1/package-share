import {
  addProductsToCart,
  addProductsToCartAfter,
  addProductsToCartTypeConfigurable,
  addProductsToCartTypeConfigurableAfter,
  addProductsToCartTypeConfigurableError,
  prepareProductAddToCartAction,
} from '../../actions';
import {
  catchError,
  filter,
  map,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import _ from 'lodash';
import { fromPromise } from 'rxjs/internal-compatibility';
import { EMPTY, of } from 'rxjs';
import { UnknownResponseError } from '@vjcspy/chitility';
import { buildConfigurableProductCartItemInput } from '../../../../util';
import { createEffect, ofType } from '../../../../../../util';
import { ProductInfoState } from '../../../../../catalog';
import { graphqlFetchForCustomer as graphqlFetch } from '../../../../../account';

const prepareProduct$ = createEffect((actions$, state$) =>
  actions$.pipe(
    ofType(prepareProductAddToCartAction),
    withLatestFrom(state$, (v1, v2) => [v1, v2.productInfo]),
    map((data) => {
      const action = data[0];
      const productInfoState: ProductInfoState = data[1];
      if (_.isArray(productInfoState?.products)) {
        const productInfo = _.find(
          productInfoState.products,
          (pI) => pI.product['id'] === action.payload.productId
        );

        if (productInfo && productInfo.configurable?.super_attribute) {
          const input = buildConfigurableProductCartItemInput(
            productInfo.configurable?.super_attribute,
            productInfo.product,
            productInfo.qty ?? action.payload.qty ?? 1
          );

          if (input) {
            return addProductsToCart({
              items: [
                {
                  product: productInfo.product,
                  input,
                },
              ],
            });
          }
        }
      } else {
        console.warn('Please resolve product info with hoc');
      }

      return EMPTY;
    })
  )
);

const beforeAddProducts$ = createEffect((action$) =>
  action$.pipe(
    ofType(addProductsToCart),
    map((action) => {
      return _.filter(action.payload.items, (_i) => {
        return (
          _i.product?.__typename === 'ConfigurableProduct' ||
          _i.product?.type === 'configurable'
        );
      });
    }),
    filter((simpleItems) => _.size(simpleItems) > 0),
    map((items) => {
      return addProductsToCartTypeConfigurable({ items });
    })
  )
);

const whenAddProductsToCart$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(addProductsToCartTypeConfigurable),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout.cart.cart]),
    filter(([action, cart]) => {
      if (_.isString(cart?.id)) {
        return true;
      } else {
        console.warn('please resolve cart data before add product to cart');
        return false;
      }
    }),
    switchMap(([action, cart]) => {
      const cartItems: any[] = [];
      _.forEach(action.payload.items, (_i) => {
        cartItems.push(_i.input);
      });

      const input = {
        cart_id: cart.id,
        cart_items: cartItems,
      };
      return fromPromise(
        graphqlFetch({
          query: `mutation addConfigurableProductsToCart($input: AddConfigurableProductsToCartInput) {
    addConfigurableProductsToCart(
        input: $input
    ) {
        cart {
            id
            items {
                id
                product {
                    sku
                    stock_status
                }
                quantity
            }
        }
    }
}
`,
          variables: {
            input,
          },
        })
      ).pipe(
        map((res) => {
          if (res?.addConfigurableProductsToCart?.cart?.id) {
            return addProductsToCartTypeConfigurableAfter({
              cartId: res?.addConfigurableProductsToCart?.cart?.id,
              items: action.payload.items,
            });
          } else {
            return addProductsToCartTypeConfigurableError({
              error: new UnknownResponseError(
                'add configurable products to cart'
              ),
              items: action.payload.items,
            });
          }
        }),
        catchError((error) =>
          of(
            addProductsToCartTypeConfigurableError({
              error,
              items: action.payload.items,
            })
          )
        )
      );
    })
  )
);

const beforeAddProductsToCartAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(addProductsToCartTypeConfigurableAfter),
    map((action) =>
      addProductsToCartAfter({
        cartId: action.payload.cartId,
      })
    )
  )
);

export const CHECKOUT_CART_ADD_CONFIGURABLE_EFFECTS = [
  prepareProduct$,
  beforeAddProducts$,
  whenAddProductsToCart$,
  beforeAddProductsToCartAfter$,
];
