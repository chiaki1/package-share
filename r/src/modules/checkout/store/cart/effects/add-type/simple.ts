import {
  catchError,
  filter,
  map,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import {
  addProductsToCart,
  addProductsToCartAfter,
  addProductsToCartTypeSimple,
  addProductsToCartTypeSimpleAfter,
  addProductsToCartTypeSimpleError,
  prepareProductAddToCartAction,
} from '../../actions';
import _ from 'lodash';
import { fromPromise } from 'rxjs/internal-compatibility';
import { EMPTY, of } from 'rxjs';
import { UnknownResponseError } from '@vjcspy/chitility';
import { createEffect, ofType } from '../../../../../../util';
import { graphqlFetchForCustomer } from '../../../../../account';
import { ProductInfoState } from '../../../../../catalog';
import { buildSimpleProductCartItemInput } from '../../../../util/cart/add/buildSimpleProductCartItemInput';

const prepareProduct$ = createEffect((actions$, state$) =>
  actions$.pipe(
    ofType(prepareProductAddToCartAction),
    withLatestFrom(state$, (v1, v2) => [v1, v2.productInfo]),
    map((data) => {
      const action = data[0];
      const productInfoState: ProductInfoState = data[1];
      if (_.isArray(productInfoState?.products)) {
        const productInfo = _.find(
          productInfoState.products,
          (pI) => pI.product['id'] === action.payload.productId
        );

        if (productInfo && productInfo.product.__typename === 'SimpleProduct') {
          const input = buildSimpleProductCartItemInput(
            productInfo.product,
            productInfo.qty ?? action.payload.qty ?? 1
          );
          if (input) {
            return addProductsToCart({
              items: [
                {
                  product: productInfo.product,
                  input,
                },
              ],
            });
          }
        }
      } else {
        console.warn('Please resolve product info with hoc');
      }

      return EMPTY;
    })
  )
);

const beforeAddProducts$ = createEffect((action$) =>
  action$.pipe(
    ofType(addProductsToCart),
    map((action) => {
      return _.filter(action.payload.items, (_i) => {
        return (
          _i.product?.__typename === 'SimpleProduct' ||
          _i.product?.type === 'simple'
        );
      });
    }),
    filter((simpleItems) => _.size(simpleItems) > 0),
    map((items) => {
      return addProductsToCartTypeSimple({ items });
    })
  )
);

const whenAddProductsToCart$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(addProductsToCartTypeSimple),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout.cart.cart]),
    filter(([action, cart]) => _.isString(cart?.id)),
    switchMap(([action, cart]) => {
      const cartItems: any[] = [];
      _.forEach(action.payload.items, (_i) => {
        cartItems.push(_i.input);
      });

      const input = {
        cart_id: cart.id,
        cart_items: cartItems,
      };
      return fromPromise(
        graphqlFetchForCustomer({
          query: `mutation addSimpleProductsToCart($input: AddSimpleProductsToCartInput) {
    addSimpleProductsToCart(
        input: $input
    ) {
        cart {
            id
            items {
                id
                product {
                    sku
                    stock_status
                }
                quantity
            }
        }
    }
}`,
          variables: {
            input,
          },
        })
      ).pipe(
        map((res) => {
          if (res?.addSimpleProductsToCart?.cart?.id) {
            return addProductsToCartTypeSimpleAfter({
              cartId: res?.addSimpleProductsToCart?.cart?.id,
              items: action.payload.items,
            });
          } else {
            return addProductsToCartTypeSimpleError({
              error: new UnknownResponseError('add simple products to cart'),
              items: action.payload.items,
            });
          }
        }),
        catchError((error) =>
          of(
            addProductsToCartTypeSimpleError({
              error,
              items: action.payload.items,
            })
          )
        )
      );
    })
  )
);

const beforeAddProductsToCartAfter$ = createEffect((action$) =>
  action$.pipe(
    ofType(addProductsToCartTypeSimpleAfter),
    map((action) =>
      addProductsToCartAfter({
        cartId: action.payload.cartId,
      })
    )
  )
);

export const CHECKOUT_CART_ADD_SIMPLE_EFFECTS = [
  prepareProduct$,
  beforeAddProducts$,
  whenAddProductsToCart$,
  beforeAddProductsToCartAfter$,
];
