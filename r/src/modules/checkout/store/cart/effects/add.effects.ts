import { addProductsToCartAfter, getCartDetail } from '../actions';
import { map } from 'rxjs/operators';
import { createEffect, ofType } from '../../../../../util';

const beforeGetCartDetail$ = createEffect((action$) =>
  action$.pipe(
    ofType(addProductsToCartAfter),
    map((action) =>
      getCartDetail({
        cartId: action.payload.cartId,
      })
    )
  )
);

export const CHECKOUT_CART_ADD_EFFECTS = [beforeGetCartDetail$];
