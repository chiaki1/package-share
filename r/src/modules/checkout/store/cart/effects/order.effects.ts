import {
  getCurrentCart,
  getUrlAction,
  getUrlAfterAction,
  getUrlErrorAction,
  placeOrder,
  placeOrderAfter,
  placeOrderError,
} from '../actions';
import {
  catchError,
  filter,
  map,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { of } from 'rxjs';
import _ from 'lodash';
import { UnknownResponseError } from '@vjcspy/chitility';
import { createEffect, ofType, proxyFetch } from '../../../../../util';
import {
  getCustomerDetail,
  graphqlFetchForCustomer,
} from '../../../../account';
import { fromArray } from 'rxjs/internal/observable/fromArray';

const whenPlaceOrder$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(placeOrder),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout.cart]),
    filter(
      ([action, cartState]) =>
        action.payload.resolveByEffect && _.isString(cartState.cart.id)
    ),
    switchMap(([_action, cartState]) => {
      return fromPromise(
        graphqlFetchForCustomer({
          query: `
          mutation placeOrder($cartId: String!) {
    placeOrder(input: {cart_id: $cartId}) {
        order {
            order_number
        }
    }
}
`,
          variables: {
            cartId: cartState.cart.id,
          },
        })
      ).pipe(
        map((res) => {
          if (res?.placeOrder?.order) {
            return placeOrderAfter({
              order: res!.placeOrder!.order,
            });
          } else {
            return placeOrderError({
              error: new UnknownResponseError('when place Order'),
            });
          }
        }),
        catchError((error) => of(placeOrderError({ error })))
      );
    })
  )
);

const whenGetPayUrl$ = createEffect((action$) =>
  action$.pipe(
    ofType(getUrlAction),
    switchMap((value) =>
      fromPromise(
        proxyFetch({
          type: 'get-pay-url',
          payload: {
            order_number: value.payload.order_number,
          },
        })
      ).pipe(
        map((url) => {
          console.log('url', url);
          return getUrlAfterAction({ url });
        }),
        catchError((error) => of(getUrlErrorAction({ error })))
      )
    )
  )
);

const afterPlaceOrder$ = createEffect((action$) =>
  action$.pipe(
    ofType(placeOrderAfter),
    switchMap(() => fromArray([getCurrentCart(), getCustomerDetail()]))
  )
);

export const CHECKOUT_ORDER_EFFECTS = [
  whenPlaceOrder$,
  afterPlaceOrder$,
  whenGetPayUrl$,
];
