import {
  applyCouponToCartAction,
  applyCouponToCartAfterAction,
  applyCouponToCartErrorAction,
  removeCouponFromCartAction,
  removeCouponFromCartAfterAction,
  removeCouponFromCartErrorAction,
} from '../actions';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { RuntimeError } from '@vjcspy/chitility';
import { createEffect, ofType } from '../../../../../util';
import { graphqlFetchForCustomer } from '../../../../account';

const whenApplyCoupon$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(applyCouponToCartAction),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout?.cart]),
    switchMap(([action, cart]) =>
      fromPromise(
        graphqlFetchForCustomer({
          query: `mutation applyCouponToCart($cart_id: String!, $coupon_code: String!) {
            applyCouponToCart(input: {
              cart_id: $cart_id,
              coupon_code: $coupon_code
            }) {
              cart {
                applied_coupons {
                  code
                }
                prices {
                    grand_total {
                        value
                        currency
                    }
                    subtotal_excluding_tax{
                        currency
                        value
                    }
                    subtotal_including_tax{
                        currency
                        value
                    }
                    discounts{
                        amount{
                            currency
                            value
                        }
                        label
                    }
                    gift_options{
                        gift_wrapping_for_items{
                            currency
                            value
                        }
                        gift_wrapping_for_order{
                            currency
                            value
                        }
                        printed_card{
                            currency
                            value
                        }
                    }
                }
              }
            }
          }
`,
          variables: {
            cart_id: action.payload.cart_id,
            coupon_code: action.payload.coupon_code,
          },
        })
      ).pipe(
        map((data) => {
          if (!!data?.applyCouponToCart && !!data?.applyCouponToCart?.cart) {
            return applyCouponToCartAfterAction({
              cart: data?.applyCouponToCart?.cart,
            });
          } else {
            return applyCouponToCartErrorAction({
              error: new RuntimeError('Wrong apply coupon to cart'),
            });
          }
        })
      )
    )
  )
);

const whenRemoveCoupon$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(removeCouponFromCartAction),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout?.cart]),
    switchMap(([action, cart]) =>
      fromPromise(
        graphqlFetchForCustomer({
          query: `mutation removeCouponFromCart($cart_id: String!) {
            removeCouponFromCart(input: {
              cart_id: $cart_id
            }) {
              cart {
                applied_coupons {
                  code
                }
                prices {
                    grand_total {
                        value
                        currency
                    }
                    subtotal_excluding_tax{
                        currency
                        value
                    }
                    subtotal_including_tax{
                        currency
                        value
                    }
                    discounts{
                        amount{
                            currency
                            value
                        }
                        label
                    }
                    gift_options{
                        gift_wrapping_for_items{
                            currency
                            value
                        }
                        gift_wrapping_for_order{
                            currency
                            value
                        }
                        printed_card{
                            currency
                            value
                        }
                    }
                }
              }
            }
          }
`,
          variables: {
            cart_id: action.payload.cart_id,
          },
        })
      ).pipe(
        map((data) => {
          if (
            !!data.removeCouponFromCart &&
            !!data?.removeCouponFromCart?.cart
          ) {
            return removeCouponFromCartAfterAction({
              cart: data.removeCouponFromCart.cart,
            });
          } else {
            return removeCouponFromCartErrorAction({
              error: new RuntimeError('Wrong remove coupon from cart'),
            });
          }
        })
      )
    )
  )
);

export const CHECKOUT_CART_COUPON = [whenApplyCoupon$, whenRemoveCoupon$];
