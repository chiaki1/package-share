import {
  setShippingMethod,
  setShippingMethodAfter,
  setShippingMethodError,
} from '../actions';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { of } from 'rxjs';
import { UnknownResponseError } from '@vjcspy/chitility';
import { createEffect, ofType } from '../../../../../util';
import { graphqlFetchForCustomer } from '../../../../account';
import CartDetail from '../../../../graphql/schema/CartDetail';

const whenSetShippingMethod$ = createEffect((action$, state$) =>
  action$.pipe(
    ofType(setShippingMethod),
    withLatestFrom(state$, (v1, v2) => [v1, v2.checkout.cart]),
    switchMap(([action, cartState]) => {
      return fromPromise(
        graphqlFetchForCustomer({
          query: `
        mutation setShippingMethodOnCart($cartId: String!, $carrierCode: String!, $methodCode:String!){
    setShippingMethodsOnCart(input: {
        cart_id: $cartId
        shipping_methods: [
            {
                carrier_code: $carrierCode
                method_code: $methodCode
            }
        ]
    }) {
        cart {
            ${CartDetail}
        }
    }
}
`,
          variables: {
            cartId: cartState.cart.id,
            carrierCode: action.payload.carrierCode,
            methodCode: action.payload.methodCode,
          },
        })
      ).pipe(
        map((res) => {
          if (res?.setShippingMethodsOnCart?.cart) {
            return setShippingMethodAfter({
              cart: res!.setShippingMethodsOnCart!.cart,
            });
          } else {
            return setShippingMethodError({
              error: new UnknownResponseError('when set shipping method'),
            });
          }
        }),
        catchError((error) =>
          of(
            setShippingMethodError({
              error,
            })
          )
        )
      );
    })
  )
);

export const CHECKOUT_CART_SHIPPING_METHOD_EFFECTS = [whenSetShippingMethod$];
