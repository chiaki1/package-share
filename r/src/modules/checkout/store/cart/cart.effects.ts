import {
  CHECKOUT_CART_ADD_EFFECTS,
  CHECKOUT_CART_ADD_SIMPLE_EFFECTS,
  CHECKOUT_CART_INIT_EFFECTS,
  CHECKOUT_CART_DETAIL_EFFECTS,
  CHECKOUT_CART_ADD_CONFIGURABLE_EFFECTS,
  CHECKOUT_CART_ADDRESS,
  CHECKOUT_CART_SHIPPING_METHOD_EFFECTS,
  CHECKOUT_ORDER_EFFECTS,
  CHECKOUT_CART_COUPON,
} from './effects';
import { CHECKOUT_CART_PAYMENT_METHOD } from './effects/payment-method.effects';
import { createEffect, ofType } from '../../../../util';
import { setPaymentMethodError } from './actions';
import { map } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

const setMessageWhenSetPaymentError$ = createEffect((action$) =>
  action$.pipe(
    ofType(setPaymentMethodError),
    map((action: any) => {
      console.log(action);
      return EMPTY;
    })
  )
);

export const CHECKOUT_CART_EFFECTS = [
  setMessageWhenSetPaymentError$,
  ...CHECKOUT_CART_INIT_EFFECTS,
  ...CHECKOUT_CART_DETAIL_EFFECTS,

  /* ADD TO CART */
  ...CHECKOUT_CART_ADD_EFFECTS,
  ...CHECKOUT_CART_ADD_SIMPLE_EFFECTS,
  ...CHECKOUT_CART_ADD_CONFIGURABLE_EFFECTS,

  /* ADDRESS */
  ...CHECKOUT_CART_ADDRESS,

  /* SHIPPING METHOD*/
  ...CHECKOUT_CART_SHIPPING_METHOD_EFFECTS,

  /* PAYMENT METHOD */
  ...CHECKOUT_CART_PAYMENT_METHOD,

  /* ORDER */
  ...CHECKOUT_ORDER_EFFECTS,

  ...CHECKOUT_CART_COUPON,
];
