import { createReducer } from '@reduxjs/toolkit';
import { CartStateFactory } from './cart.state';
import {
  getCartDetailAfter,
  getCurrentCart,
  gotCustomerCart,
  gotGuestCartInStorage,
  mergeGuestCartAfter,
} from './actions';
import {
  clearCartMessageAction,
  closeCartDetail,
  openCartDetail,
  setCartMessageAction,
} from './cart.actions';
import { checkoutCartDetailBuilderCallBack } from './reducers/details.reducer';
import { checkoutCartAddReducerBuilderCallback } from './reducers/add.reducer';
import { checkoutAddressBuilderCallback } from './reducers/address.reducer';
import { shippingMethodReducer } from './reducers/shipping-method.reducer';
import { paymentMethodReducer } from './reducers/payment-method.reducer';
import { orderReducer } from './reducers/order.reducer';
import { rewardPointReducer } from './reducers/reward.reducer';

export const cartReducer = createReducer(CartStateFactory(), (builder) => {
  builder
    .addCase(getCurrentCart, (state) => {
      state.isResolvedCart = false;
    })
    .addCase(gotGuestCartInStorage, (state, action) => {
      state.guestCartId = action.payload.cartId;
    })
    .addCase(getCartDetailAfter, (state, action) => {
      state.cart = action.payload.cart;
      state.isResolvedCart = true;
    })
    .addCase(gotCustomerCart, (state, action) => {
      state.customerCartId = action.payload.cartId;
    })
    .addCase(mergeGuestCartAfter, (state, action) => {
      state.mergeGuestCartId = action.payload.cartId;
      state.guestCartId = undefined;
    })
    .addCase(setCartMessageAction, (state, action) => {
      state.message = action.payload.message;
    })
    .addCase(clearCartMessageAction, (state) => {
      state.message = undefined;
    });

  builder
    .addCase(openCartDetail, (state) => {
      state.isCartOpen = true;
    })
    .addCase(closeCartDetail, (state) => {
      state.isCartOpen = false;
    });

  checkoutCartDetailBuilderCallBack(builder);
  checkoutCartAddReducerBuilderCallback(builder);
  checkoutAddressBuilderCallback(builder);
  shippingMethodReducer(builder);
  paymentMethodReducer(builder);
  orderReducer(builder);
  rewardPointReducer(builder);
});
