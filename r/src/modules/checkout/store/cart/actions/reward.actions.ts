import { generateAction } from '../../../../../util';

const setReward = generateAction<{}, { cart: any }>(
  'SET_REWARD_POINT_TO_CART',
  'CHECKOUT_PAYMENT'
);

export const setRewardPointToCartAction = setReward.ACTION;
export const setRewardPointToCartAfterAction = setReward.AFTER;
export const setRewardPointToCartErrorAction = setReward.ERROR;

const removeReward = generateAction<{}, { cart: any }>(
  'REMOVE_REWARD_PONINT_FROM_CART',
  'CHECKOUT_PAYMENT'
);

export const removeRewardPointFromCartAction = removeReward.ACTION;
export const removeRewardPointFromCartAfterAction = removeReward.AFTER;
export const removeRewardPointFromCartErrorAction = removeReward.ERROR;
