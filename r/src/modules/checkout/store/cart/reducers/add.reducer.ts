import {
  addProductsToCart,
  addProductsToCartTypeConfigurable,
  addProductsToCartTypeConfigurableAfter,
  addProductsToCartTypeConfigurableError,
  addProductsToCartTypeSimple,
  addProductsToCartTypeSimpleAfter,
  addProductsToCartTypeSimpleError,
  prepareProductAddToCartAction,
} from '../actions';
import { CartState } from '../cart.state';
import _ from 'lodash';
import { Draft } from 'immer';
import { createBuilderCallback } from '../../../../../util';
import { toggleConfigurableOption } from '../../../../catalog';

export const checkoutCartAddReducerBuilderCallback = createBuilderCallback<CartState>(
  (builder) =>
    builder
      .addCase(prepareProductAddToCartAction, (state, action) => {
        state.preparingProductIds = _.chain(state.preparingProductIds)
          .filter((pI) => pI.productId !== action.payload.productId)
          .push({
            productId: action.payload.productId,
            ts: +new Date(),
          })
          .uniqBy('productId')
          .value();
      })
      .addCase(addProductsToCart, (state, action) => {
        state.preparingProductIds =
          _.filter(
            state.preparingProductIds,
            (p) =>
              !_.chain(action.payload.items)
                .map((item) => item.product.id)
                .includes(p.productId)
          ) ?? [];
      })
      // Khi người dùng lựa chọn option thì loại bỏ trạng thái preparing
      .addCase(toggleConfigurableOption, (state, action) => {
        state.preparingProductIds = _.filter(
          state.preparingProductIds,
          (pId) => pId.productId != action.payload.productId
        );
      })
      /* Simple */
      .addCase(addProductsToCartTypeSimple, (state, action) => {
        addingState(state, action, 'simple');
      })
      .addCase(addProductsToCartTypeSimpleAfter, (state, action) => {
        clearProductIdAfterAdd(state, action, 'simple');
      })
      .addCase(addProductsToCartTypeSimpleError, (state, action) => {
        clearProductIdAfterAdd(state, action, 'simple');
      })
      /* Configurable */
      .addCase(addProductsToCartTypeConfigurable, (state, action) => {
        addingState(state, action, 'configurable');
      })
      .addCase(addProductsToCartTypeConfigurableAfter, (state, action) => {
        clearProductIdAfterAdd(state, action, 'configurable');
      })
      .addCase(addProductsToCartTypeConfigurableError, (state, action) => {
        clearProductIdAfterAdd(state, action, 'configurable');
      })
);

function addingState(
  state: Draft<CartState>,
  action: any,
  productType: 'simple' | 'configurable' | 'bundle' | 'grouped' | 'downloadable'
) {
  state.adding = state.adding ?? {};
  state.adding[productType] = state.adding[productType] ?? [];

  state.adding[productType]!.push(
    ..._.map(action.payload.items, (i: any) => i.product?.id)
  );
  state.adding[productType] = _.uniq(state.adding[productType]);
  state.adding[productType] = _.filter(
    state.adding[productType],
    (i: any) => !!i
  );
}

function clearProductIdAfterAdd(
  state: CartState,
  action: any,
  productType: 'simple' | 'configurable' | 'bundle' | 'grouped' | 'downloadable'
) {
  if (state.adding && _.isArray(state.adding[productType])) {
    state.adding![productType] = _.filter(
      state.adding![productType],
      (pId: any) => {
        // console.log(_.map(action.payload?.items, (i1: any) => i1.product?.id));
        return !_.includes(
          _.map(action.payload?.items, (i1: any) => i1.product?.id),
          pId
        );
      }
    );
  }
}
