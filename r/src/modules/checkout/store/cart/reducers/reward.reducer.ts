import { createBuilderCallback } from '../../../../../util';
import { CartState } from '../cart.state';
import {
  removeRewardPointFromCartAction,
  removeRewardPointFromCartAfterAction,
  removeRewardPointFromCartErrorAction,
  setRewardPointToCartAction,
  setRewardPointToCartAfterAction,
  setRewardPointToCartErrorAction,
} from '../actions';

export const rewardPointReducer = createBuilderCallback<CartState>(
  (builder) => {
    builder
      .addCase(setRewardPointToCartAction, (state) => {
        state.isUpdatingTotals = true;
      })
      .addCase(setRewardPointToCartErrorAction, (state) => {
        state.isUpdatingTotals = false;
      })
      .addCase(setRewardPointToCartAfterAction, (state, action) => {
        state.isUpdatingTotals = false;
        state.cart = action.payload.cart;
      });

    builder
      .addCase(removeRewardPointFromCartAction, (state) => {
        state.isUpdatingTotals = true;
      })
      .addCase(removeRewardPointFromCartErrorAction, (state) => {
        state.isUpdatingTotals = false;
      })
      .addCase(removeRewardPointFromCartAfterAction, (state, action) => {
        state.isUpdatingTotals = false;
        state.cart = action.payload.cart;
      });
  }
);
