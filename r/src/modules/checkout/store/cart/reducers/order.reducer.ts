import { createBuilderCallback } from '../../../../../util';
import { CartState } from '../cart.state';
import { placeOrder, placeOrderAfter, placeOrderError } from '../actions';

export const orderReducer = createBuilderCallback<CartState>((builder) => {
  builder
    .addCase(placeOrder, (state) => {
      state.isPlacingOrder = true;
    })
    .addCase(placeOrderAfter, (state, action) => {
      state.isPlacingOrder = false;
      state.completeOrderNumber = action.payload.order['order_number'];
    })
    .addCase(placeOrderError, (state) => {
      state.isPlacingOrder = false;
    });
});
