import { CartState } from '../cart.state';
import { createBuilderCallback } from '../../../../../util';
import { setShippingMethod, setShippingMethodAfter } from '../actions';

export const shippingMethodReducer = createBuilderCallback<CartState>(
  (builder) => {
    builder
      .addCase(setShippingMethod, (state) => {
        state.isUpdatingAddress = true;
        state.isUpdatingTotals = true;
      })
      .addCase(setShippingMethodAfter, (state, action) => {
        state.cart = action.payload.cart;
        state.isUpdatingAddress = false;
        state.isUpdatingTotals = false;
      });
  }
);
