import { createBuilderCallback } from '../../../../../util';
import { CartState } from '../cart.state';
import {
  checkoutCartAddressCancelEditAddressAction,
  checkoutCartAddressEditAddressAction,
  checkoutCartAddressSetBillingAddress,
  checkoutCartAddressSetBillingAddressAfter,
  checkoutCartAddressSetBillingAddressError,
  checkoutCartSetShippingAddress,
  checkoutCartSetShippingAddressAfter,
  checkoutCartSetShippingAddressError,
} from '../actions';
import {
  createNewCustomerAddressAction,
  updateCustomerAddressAction,
} from '../../../../account';

export const checkoutAddressBuilderCallback = createBuilderCallback<CartState>(
  (builder) => {
    builder.addCase(checkoutCartSetShippingAddress, (state) => {
      state.isUpdatingAddress = true;
    });
    builder.addCase(checkoutCartAddressSetBillingAddress, (state) => {
      state.isUpdatingAddress = true;
    });
    builder
      .addCase(checkoutCartSetShippingAddressAfter, (state, action) => {
        state.cart = action.payload.cart;
        state.isUpdatingAddress = false;
        state.editingAddressObj = undefined;
      })
      .addCase(checkoutCartSetShippingAddressError, (state) => {
        state.isUpdatingAddress = false;
        state.editingAddressObj = undefined;
      });
    builder
      .addCase(checkoutCartAddressSetBillingAddressAfter, (state, action) => {
        state.cart = action.payload.cart;
        state.isUpdatingAddress = false;
        state.editingAddressObj = undefined;
      })
      .addCase(checkoutCartAddressSetBillingAddressError, (state) => {
        state.isUpdatingAddress = false;
        state.editingAddressObj = undefined;
      });

    // update address
    builder
      .addCase(checkoutCartAddressEditAddressAction, (state, action) => {
        state.editingAddressObj = action.payload.address;
      })
      .addCase(checkoutCartAddressCancelEditAddressAction, (state) => {
        state.editingAddressObj = undefined;
      })
      .addCase(updateCustomerAddressAction, (state) => {
        state.isUpdatingAddress = true;
      })
      .addCase(createNewCustomerAddressAction, (state) => {
        state.isUpdatingAddress = true;
      });
  }
);
