import { CartState } from '../cart.state';
import {
  getPaymentMethodAfter,
  setPaymentMethod,
  setPaymentMethodAfter,
  setPaymentMethodError,
} from '../actions';
import _ from 'lodash';
import { createBuilderCallback } from '../../../../../util';

export const paymentMethodReducer = createBuilderCallback<CartState>(
  (builder) => {
    builder
      .addCase(getPaymentMethodAfter, (state, action) => {
        if (_.isArray(action.payload.cart.available_payment_methods)) {
          if (state.cart) {
            state.cart.available_payment_methods =
              action.payload.cart.available_payment_methods;
          }
        }
      })
      .addCase(setPaymentMethod, (state) => {
        state.isUpdatingTotals = true;
      })
      .addCase(setPaymentMethodAfter, (state, action) => {
        state.cart = action.payload.cart;
        state.isUpdatingTotals = false;
      })
      .addCase(setPaymentMethodError, (state) => {
        state.isUpdatingTotals = false;
      });
  }
);
