import { CartState } from './cart';

export interface CheckoutState {
  cart: CartState;
}
