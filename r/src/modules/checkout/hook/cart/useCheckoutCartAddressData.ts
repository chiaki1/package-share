import { useSelector } from 'react-redux';
import {
  selectEditingAddressObj,
  selectIsUpdatingAddress,
  selectQuoteShippingAddress,
} from '../../store';
import { useCallback, useMemo } from 'react';
import _ from 'lodash';

export const useCheckoutCartAddressData = () => {
  const quoteShippingAddress = useSelector(selectQuoteShippingAddress);
  const currentShippingAddress = useMemo(() => {
    return Array.isArray(quoteShippingAddress) &&
      _.size(quoteShippingAddress) === 1
      ? _.first(quoteShippingAddress)
      : undefined;
  }, [quoteShippingAddress]);

  const isSelectedCustomerAdd = useCallback(
    (customerAddId: any) => {
      return !!(
        currentShippingAddress &&
        currentShippingAddress['customer_address_id'] &&
        parseInt(currentShippingAddress['customer_address_id']) ===
          parseInt(customerAddId)
      );
    },
    [currentShippingAddress]
  );

  const isUpdatingAddress = useSelector(selectIsUpdatingAddress);

  // update address
  const editAddressObj = useSelector(selectEditingAddressObj);
  const isEditingAddress = useMemo(() => !!editAddressObj, [editAddressObj]);

  return {
    state: {
      currentShippingAddress,
      isUpdatingAddress,
      isEditingAddress,
      editAddressObj,
    },
    isSelectedCustomerAdd,
  };
};
