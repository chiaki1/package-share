import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import {
  applyCouponToCartAction,
  removeCouponFromCartAction,
} from '../../store';

export const useCouponActions = () => {
  const dispatch = useDispatch();
  const applyCouponToCart = useCallback(
    (cart_id: string, coupon_code: string) => {
      dispatch(
        applyCouponToCartAction({
          cart_id,
          coupon_code,
        })
      );
    },
    []
  );

  const removeCouponFromCart = useCallback((cart_id: string) => {
    dispatch(removeCouponFromCartAction({ cart_id }));
  }, []);

  return {
    actions: { applyCouponToCart, removeCouponFromCart },
  };
};
