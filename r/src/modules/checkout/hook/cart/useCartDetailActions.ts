import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import {
  checkoutCartDetailAddCouponCodeAction,
  checkoutCartDetailRemoveCouponCodeAction,
  checkoutCartDetailRemoveItemAction,
  updateCartItemAction,
} from '../../store';

export const useCartDetailActions = () => {
  const dispatch = useDispatch();

  const updateCartItem = useCallback(
    (updateCartItemInput: {
      cartId: string;
      cartItemId: number;
      qty: number;
    }) => {
      dispatch(updateCartItemAction(updateCartItemInput));
    },
    []
  );

  const removeItemFromCart = useCallback(
    (cartId: string, cartItemId: number) => {
      dispatch(
        checkoutCartDetailRemoveItemAction({
          cartId,
          cartItemId,
        })
      );
    },
    []
  );

  const addCouponCode = useCallback((cartId: string, couponCode: string) => {
    dispatch(
      checkoutCartDetailAddCouponCodeAction({
        cartId,
        couponCode,
      })
    );
  }, []);

  const removeCouponCode = useCallback((cartId: string) => {
    dispatch(
      checkoutCartDetailRemoveCouponCodeAction({
        cartId,
      })
    );
  }, []);

  return {
    actions: {
      updateCartItem,
      removeItemFromCart,
      addCouponCode,
      removeCouponCode,
    },
  };
};
