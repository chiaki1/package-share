import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { setShippingMethod } from '../../store';

export const useShippingMethodActions = () => {
  const dispatch = useDispatch();

  const setShippingMethodAction = useCallback(
    (carrierCode: string, methodCode: string) => {
      dispatch(
        setShippingMethod({
          methodCode,
          carrierCode,
        })
      );
    },
    []
  );

  return {
    actions: { setShippingMethodAction },
  };
};
