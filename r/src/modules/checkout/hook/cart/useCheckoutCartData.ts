import { useDispatch, useSelector } from 'react-redux';
import {
  closeCartDetail,
  isCartOpen,
  openCartDetail,
  selectAppliedRewardPoint,
  selectCart,
  selectCartItemUpdating,
  selectCouponCode,
  selectIsUpdatingCoupon,
  selectIsUpdatingTotals,
} from '../../store';
import { useCallback } from 'react';

export const useCheckoutCartData = () => {
  const dispatch = useDispatch();
  const cart = useSelector(selectCart);
  const cartItemUpdating = useSelector(selectCartItemUpdating);
  const isUpdatingCoupon = useSelector(selectIsUpdatingCoupon);
  const isUpdatingTotals = useSelector(selectIsUpdatingTotals);
  const couponCode = useSelector(selectCouponCode);
  const appliedRewardPoint = useSelector(selectAppliedRewardPoint);
  const isCartOpening = useSelector(isCartOpen);

  const openCart = useCallback(() => {
    dispatch(openCartDetail());
  }, []);
  const closeCart = useCallback(() => {
    dispatch(closeCartDetail());
  }, []);
  return {
    state: {
      cart,
      cartItemUpdating,
      isUpdatingCoupon,
      isUpdatingTotals,
      couponCode,
      appliedRewardPoint,
      isCartOpening,
    },
    actions: {
      openCart,
      closeCart,
    },
  };
};
