import { useSelector } from 'react-redux';
import {
  selectAvailableShippingMethod,
  selectSelectedShippingMethod,
} from '../../store';

export const useShippingMethodData = () => {
  const availableShippingMethod = useSelector(selectAvailableShippingMethod);
  const selectedShippingMethod = useSelector(selectSelectedShippingMethod);
  return {
    state: {
      availableShippingMethod,
      selectedShippingMethod,
    },
  };
};
