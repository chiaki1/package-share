import { useSelector } from 'react-redux';
import { selectIsPreparingProductId } from '../../store';

export const useSelectIsPreparing = () => {
  const isPreparingProductId = useSelector(selectIsPreparingProductId);

  return {
    isPreparingProductId,
  };
};
