import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  removeCouponFromCartErrorAction,
  removeRewardPointFromCartAction,
  removeRewardPointFromCartAfterAction,
  selectCart,
  setRewardPointToCartAction,
  setRewardPointToCartAfterAction,
  setRewardPointToCartErrorAction,
} from '../../store';
import {
  useApplyRewardPointToCartMutation,
  useRemoveRewardPointFromCartMutation,
} from '@vjcspy/apollo';

export const useRewardPointActions = () => {
  const dispatch = useDispatch();
  const cart = useSelector(selectCart);

  const [
    setRewardPointMutation,
    setRewardPointMutationRes,
  ] = useApplyRewardPointToCartMutation();

  const setRewardPointToCart = useCallback(() => {
    if (cart?.id) {
      dispatch(setRewardPointToCartAction());
      setRewardPointMutation({
        variables: {
          cartId: cart.id,
        },
      })
        .then()
        .catch();
    }
  }, [cart?.id]);

  useEffect(() => {
    if (setRewardPointMutationRes.error) {
      dispatch(
        setRewardPointToCartErrorAction({
          error: setRewardPointMutationRes.error,
        })
      );
    }

    if (setRewardPointMutationRes.data?.applyRewardPointsToCart?.cart) {
      dispatch(
        setRewardPointToCartAfterAction({
          cart: setRewardPointMutationRes.data?.applyRewardPointsToCart?.cart,
        })
      );
    }
  }, [setRewardPointMutationRes.error, setRewardPointMutationRes.data]);

  const [
    removeRewardPointMutation,
    removeRewardPointMutationRes,
  ] = useRemoveRewardPointFromCartMutation();
  const removeRewardPointFromCart = useCallback(() => {
    if (cart?.id) {
      dispatch(removeRewardPointFromCartAction());
      removeRewardPointMutation({
        variables: {
          cartId: cart.id,
        },
      })
        .then()
        .catch();
    }
  }, [cart?.id]);

  useEffect(() => {
    if (removeRewardPointMutationRes.error) {
      dispatch(
        removeCouponFromCartErrorAction({
          error: removeRewardPointMutationRes.error,
        })
      );
    }

    if (removeRewardPointMutationRes.data?.removeRewardPointsFromCart?.cart) {
      dispatch(
        removeRewardPointFromCartAfterAction({
          cart:
            removeRewardPointMutationRes.data?.removeRewardPointsFromCart?.cart,
        })
      );
    }
  }, [removeRewardPointMutationRes.error, removeRewardPointMutationRes.data]);

  return {
    actions: {
      setRewardPointToCart,
      removeRewardPointFromCart,
    },
  };
};
