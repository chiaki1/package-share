import { useSelector } from 'react-redux';
import { selectCartMessage } from '../../store';

export const useCartMessage = () => {
  const cartMessage = useSelector(selectCartMessage);
  return {
    state: {
      cartMessage,
    },
  };
};
