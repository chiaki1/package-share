import { useSelector } from 'react-redux';
import { selectCompleteOrderNumber, selectIsPlacingOrder } from '../../store';

export const useCheckoutOrderData = () => {
  const isPlacingOrder = useSelector(selectIsPlacingOrder);
  const completeOrderNumber = useSelector(selectCompleteOrderNumber);
  return {
    state: {
      isPlacingOrder,
      completeOrderNumber,
    },
  };
};
