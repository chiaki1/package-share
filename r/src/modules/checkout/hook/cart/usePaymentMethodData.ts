import { useSelector } from 'react-redux';
import {
  selectAvailablePaymentMethods,
  selectSelectedPaymentMethod,
} from '../../store';

export const usePaymentMethodData = () => {
  const availablePaymentMethods = useSelector(selectAvailablePaymentMethods);
  const selectedPaymentMethod = useSelector(selectSelectedPaymentMethod);

  return {
    state: {
      availablePaymentMethods,
      selectedPaymentMethod,
    },
  };
};
