import { useDispatch, useSelector } from 'react-redux';
import { useCallback, useEffect, useState } from 'react';
import {
  openCartDetail,
  prepareProductAddToCartAction,
  selectAddingProduct,
  selectCartItems,
} from '../../store';
import _ from 'lodash';
import { Registry } from '@vjcspy/chitility';

const ADD_TO_CART_CALL_BACK_KEY = 'ADD_TO_CART_CALL_BACK_KEY';

export const useAddToCartActions = () => {
  useEffect(() => {
    // console.log('mounted');
    return () => {
      // console.log('unmounted');
      Registry.getInstance().unregister(ADD_TO_CART_CALL_BACK_KEY);
    };
  }, []);
  const dispatch = useDispatch();
  const [currentProductId, setCurrentProductId] = useState<any>();
  const addingProduct = useSelector(selectAddingProduct);
  const cartItems = useSelector(selectCartItems);

  const prepareProductAddToCart = useCallback(
    (productId: number, callBack: () => void) => {
      Registry.getInstance().register(ADD_TO_CART_CALL_BACK_KEY, callBack);
      setCurrentProductId(productId);
      dispatch(
        prepareProductAddToCartAction({
          productId,
        })
      );
    },
    [currentProductId, setCurrentProductId]
  );

  useEffect(() => {
    let isAdding = false;
    // console.log('currentProductId', currentProductId);
    if (addingProduct && currentProductId) {
      _.forEach(addingProduct, (addingPrs) => {
        if (_.includes(addingPrs, currentProductId)) {
          isAdding = true;

          return false;
        }
      });
      // console.log(
      //   isAdding,
      //   Registry.getInstance().registry(ADD_TO_CART_CALL_BACK_KEY)
      // );
      if (
        !isAdding &&
        Registry.getInstance().registry(ADD_TO_CART_CALL_BACK_KEY)
      ) {
        const isExistingInCart = _.find(
          cartItems,
          (item: any) => item.product?.id == currentProductId
        );

        if (isExistingInCart) {
          Registry.getInstance().registry(ADD_TO_CART_CALL_BACK_KEY)();
          Registry.getInstance().unregister(ADD_TO_CART_CALL_BACK_KEY);
        }
      }
    }
  }, [addingProduct, currentProductId, cartItems]);

  const openCart = useCallback(() => {
    dispatch(openCartDetail());
  }, []);

  return {
    actions: { prepareProductAddToCart, openCart },
  };
};
