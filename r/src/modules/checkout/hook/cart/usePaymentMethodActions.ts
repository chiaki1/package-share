import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { setPaymentMethod } from '../../store';

export const usePaymentMethodActions = () => {
  const dispatch = useDispatch();

  const setPaymentMethodAction = useCallback((methodCode: string) => {
    dispatch(
      setPaymentMethod({
        methodCode,
      })
    );
  }, []);
  return {
    actions: {
      setPaymentMethodAction,
    },
  };
};
