import { useSelector } from 'react-redux';
import { selectIsResolvedCart } from '../../store';

export const useIsResolvedCart = () => {
  const isResolvedCart = useSelector(selectIsResolvedCart);

  return {
    state: {
      isResolvedCart,
    },
  };
};
