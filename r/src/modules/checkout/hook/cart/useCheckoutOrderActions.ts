import { useDispatch, useSelector } from 'react-redux';
import { useCallback, useEffect, useRef } from 'react';
import {
  placeOrder,
  placeOrderAfter,
  placeOrderError,
  selectCart,
  getUrlAction,
} from '../../store';
import { Cart, usePlaceOrderMutation } from '@vjcspy/apollo';
import { Registry } from '@vjcspy/chitility';
import { CheckoutConstant, CheckoutHookCfg } from '../../util';
import _ from 'lodash';

const resolveHooks = async (cart: Cart, type = 'after') => {
  let hookFns: CheckoutHookCfg[] = Registry.getInstance().registry(
    CheckoutConstant.CHECKOUT_HOOK_KEY
  );

  hookFns = _.filter(hookFns, (h) => h.type === type);
  if (Array.isArray(hookFns) && hookFns.length > 0) {
    for (let i = 0; i <= hookFns.length; i++) {
      const resolveNext = await hookFns[i].hookFn(cart);

      if (!resolveNext) {
        return;
      }
    }
  }
};

export const useCheckoutOrderActions = () => {
  const dispatch = useDispatch();
  const cart = useSelector(selectCart);
  const placeOrderAction = useCallback(() => {
    dispatch(
      placeOrder({
        resolveByEffect: true,
      })
    );
  }, []);

  const cartRef = useRef<any>();

  const [placeOrderMutation, placeOrderMutationRes] = usePlaceOrderMutation();

  const placeOrderWithHookAction = useCallback(async () => {
    try {
      if (cart?.id) {
        cartRef.current = cart;
        dispatch(
          placeOrder({
            resolveByEffect: false,
          })
        );
        await placeOrderMutation({
          variables: {
            cartId: cart.id,
          },
        });
      }
    } catch (e) {
      console.error('could not place order', e);
    }
  }, [cart]);

  useEffect(() => {
    if (placeOrderMutationRes.error) {
      dispatch(
        placeOrderError({
          error: placeOrderMutationRes.error,
        })
      );
    }

    if (placeOrderMutationRes.data?.placeOrder?.order.order_number) {
      dispatch(
        placeOrderAfter({
          order: placeOrderMutationRes.data?.placeOrder?.order,
        })
      );
      setTimeout(() => resolveHooks(cartRef.current), 100);
    }
  }, [placeOrderMutationRes.data, placeOrderMutationRes.error]);

  const getUrlVNPayAction = useCallback(() => {
    if (placeOrderMutationRes.data?.placeOrder?.order.order_number) {
      dispatch(
        getUrlAction({
          order_number: placeOrderMutationRes.data?.placeOrder?.order,
        })
      );
    }
  }, [placeOrderMutationRes.data?.placeOrder?.order]);

  return {
    actions: { placeOrderAction, placeOrderWithHookAction, getUrlVNPayAction },
  };
};
