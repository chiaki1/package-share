import { useSelector } from 'react-redux';
import { selectIsAddingProductId } from '../../store';

export const useAddToCartData = () => {
  const isAddingProductId = useSelector(selectIsAddingProductId);

  return { actions: { isAddingProductId } };
};
