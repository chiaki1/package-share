import { useSelectIsPreparing } from '../../hook/cart/useSelectIsPreparing';
import { createUiHOC } from '@vjcspy/ui-extension';

export const withSelectIsPreparing = createUiHOC(
  () => useSelectIsPreparing(),
  'withSelectIsPreparing'
);
