import { createUiHOC } from '@vjcspy/ui-extension';
import { usePaymentMethodData } from '../../hook';

export const withPaymentMethodData = createUiHOC(
  () => usePaymentMethodData(),
  'withPaymentMethodData'
);
