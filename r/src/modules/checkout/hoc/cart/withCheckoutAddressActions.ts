import { createUiHOC } from '@vjcspy/ui-extension';
import { useCheckoutCartAddressActions } from '../../hook';

export const withCheckoutAddressActions = createUiHOC(() => {
  return useCheckoutCartAddressActions();
}, 'withCheckoutAddressActions');
