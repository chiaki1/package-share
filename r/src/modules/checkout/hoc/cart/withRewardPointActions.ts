import { createUiHOC } from '@vjcspy/ui-extension';
import { useRewardPointActions } from '../../hook';

export const withRewardPointActions = createUiHOC(
  () => useRewardPointActions(),
  'withRewardPointActions'
);
