import { withCheckoutCartData } from './withCheckoutCartData';
import { withCheckoutAddressActions } from './withCheckoutAddressActions';
import { withShippingMethodData } from './withShippingMethodData';
import { withShippingMethodActions } from './withShippingMethodActions';
import { withPaymentMethodData } from './withPaymentMethodData';
import { withPaymentMethodActions } from './withPaymentMethodActions';
import { withCheckoutOrderActions } from './withCheckoutOrderActions';
import { withAddToCartActions } from './withAddToCartActions';
import { withSelectIsPreparing } from './withSelectIsPreparing';
import { withIsResolvedCart } from './withIsResolvedCart';
import { withCartDetailActions } from './withCartDetailActions';
import { withCouponActions } from './withCouponActions';
import { withCheckoutAddressData } from './withCheckoutAddressData';
import { withAddToCartData } from './withAddToCartData';
import { withCheckoutOrderData } from './withCheckoutOrderData';
import { withCartMessage } from './withCartMessage';
import { withRewardPointActions } from './withRewardPointActions';
import { withCheckoutCartMessageActions } from './withCheckoutCartMessageActions';

export const CHECKOUT_CART_HOCS = [
  withCheckoutCartData,
  withAddToCartActions,
  withCheckoutAddressActions,
  withCheckoutAddressData,
  withShippingMethodData,
  withShippingMethodActions,
  withPaymentMethodData,
  withPaymentMethodActions,
  withCheckoutOrderActions,
  withSelectIsPreparing,
  withIsResolvedCart,
  withCartDetailActions,
  withCouponActions,
  withAddToCartData,
  withCheckoutOrderData,
  withCartMessage,
  withRewardPointActions,
  withCheckoutCartMessageActions,
];
