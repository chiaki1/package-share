import { createUiHOC } from '@vjcspy/ui-extension';
import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { clearCartMessageAction, setCartMessageAction } from '../../store';

export const withCheckoutCartMessageActions = createUiHOC(() => {
  const dispatch = useDispatch();

  const setMessage = useCallback((message) => {
    dispatch(
      setCartMessageAction({
        message,
      })
    );
  }, []);

  const clearMessage = useCallback(() => {
    dispatch(clearCartMessageAction());
  }, []);

  return {
    actions: {
      setMessage,
      clearMessage,
    },
  };
}, 'withCheckoutCartMessageActions');
