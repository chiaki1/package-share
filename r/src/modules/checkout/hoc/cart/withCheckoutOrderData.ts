import { createUiHOC } from '@vjcspy/ui-extension';
import { useCheckoutOrderData } from '../../hook/cart/useCheckoutOrderData';

export const withCheckoutOrderData = createUiHOC(
  () => useCheckoutOrderData(),
  'withCheckoutOrderData'
);
