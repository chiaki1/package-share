import { createUiHOC } from '@vjcspy/ui-extension';
import { useShippingMethodActions } from '../../hook';

export const withShippingMethodActions = createUiHOC(
  () => useShippingMethodActions(),
  'withShippingMethodActions'
);
