import { createUiHOC } from '@vjcspy/ui-extension';
import { usePaymentMethodActions } from '../../hook';

export const withPaymentMethodActions = createUiHOC(
  () => usePaymentMethodActions(),
  'withPaymentMethodActions'
);
