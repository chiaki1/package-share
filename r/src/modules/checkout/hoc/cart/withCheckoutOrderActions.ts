import { createUiHOC } from '@vjcspy/ui-extension';
import { useCheckoutOrderActions } from '../../hook';

export const withCheckoutOrderActions = createUiHOC(
  () => useCheckoutOrderActions(),
  'withCheckoutOrderActions'
);
