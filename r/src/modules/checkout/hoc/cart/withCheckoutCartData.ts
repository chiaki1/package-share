import { createUiHOC } from '@vjcspy/ui-extension';
import { useCheckoutCartData } from '../../hook';
import { useSelector } from 'react-redux';
import { selectIsResolvedCart } from '../../store';

export const withCheckoutCartData = createUiHOC(() => {
  const isResolvedCart = useSelector(selectIsResolvedCart);
  const { state, actions } = useCheckoutCartData();
  return {
    state: {
      isResolvedCart,
      ...state,
    },
    actions: {
      ...actions,
    },
  };
}, 'withCheckoutCartData');
