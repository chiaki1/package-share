import { createUiHOC } from '@vjcspy/ui-extension';
import { useIsResolvedCart } from '../../hook/cart/useIsResolvedCart';

export const withIsResolvedCart = createUiHOC(
  () => useIsResolvedCart(),
  'withIsResolvedCart'
);
