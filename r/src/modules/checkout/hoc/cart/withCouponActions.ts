import { createUiHOC } from '@vjcspy/ui-extension';
import { useCouponActions } from '../../hook';

export const withCouponActions = createUiHOC(
  () => useCouponActions(),
  'withCouponActions'
);
