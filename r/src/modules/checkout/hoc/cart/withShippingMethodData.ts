import { createUiHOC } from '@vjcspy/ui-extension';
import { useShippingMethodData } from '../../hook';

export const withShippingMethodData = createUiHOC(() => {
  return useShippingMethodData();
}, 'withShippingMethodData');
