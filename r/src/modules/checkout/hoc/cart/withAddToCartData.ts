import { createUiHOC } from '@vjcspy/ui-extension';
import { useAddToCartData } from '../../hook';

export const withAddToCartData = createUiHOC(
  () => useAddToCartData(),
  'withAddToCartData'
);
