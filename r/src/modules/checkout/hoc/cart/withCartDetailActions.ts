import { createUiHOC } from '@vjcspy/ui-extension';
import { useCartDetailActions } from '../../hook';

export const withCartDetailActions = createUiHOC(() => {
  return useCartDetailActions();
}, 'withCartDetailActions');
