import { createUiHOC } from '@vjcspy/ui-extension';
import { useAddToCartActions } from '../../hook';

export const withAddToCartActions = createUiHOC(() => {
  return useAddToCartActions();
}, 'withAddToCartActions');
