import { createUiHOC } from '@vjcspy/ui-extension';
import { useCartMessage } from '../../hook/cart/useCartMessage';

export const withCartMessage = createUiHOC(
  () => useCartMessage(),
  'withCartMessage'
);
