import { createUiHOC } from '@vjcspy/ui-extension';
import { useCheckoutCartAddressData } from '../../hook';

export const withCheckoutAddressData = createUiHOC(
  () => useCheckoutCartAddressData(),
  'withCheckoutAddressData'
);
