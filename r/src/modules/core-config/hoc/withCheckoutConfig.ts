import { createUiHOC } from '@vjcspy/ui-extension';
import { useCheckoutConfig } from '../hook/useCheckoutConfig';

export const withCheckoutConfig = createUiHOC(() => {
  return useCheckoutConfig();
}, 'withCheckoutConfig');
