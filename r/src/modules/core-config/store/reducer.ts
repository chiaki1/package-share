import { createReducer } from '@reduxjs/toolkit';
import { CoreConfigFactory } from './state';
import { getCheckoutConfigAfter } from './actions';

export const configReducer = createReducer(CoreConfigFactory(), (builder) => {
  builder.addCase(getCheckoutConfigAfter, (state, action) => {
    state.checkout = action.payload.checkout;
  });
});
