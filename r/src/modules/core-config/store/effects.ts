import {
  getCheckoutConfig,
  getCheckoutConfigAfter,
  getCheckoutConfigError,
} from './actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { of } from 'rxjs';
import { createEffect, ofType, proxyFetch } from '../../../util';

const getCheckoutConfig$ = createEffect((action$) =>
  action$.pipe(
    ofType(getCheckoutConfig),
    switchMap(() =>
      fromPromise(
        proxyFetch({
          type: 'get-checkout-config',
        })
      ).pipe(
        map((res: any) => {
          return getCheckoutConfigAfter({ checkout: res });
        }),
        catchError((error) => of(getCheckoutConfigError({ error })))
      )
    )
  )
);

export const R_CONFIG_EFFECTS = [getCheckoutConfig$];
