import { getCheckoutConfig } from './store';
import { UiManager } from '@vjcspy/ui-extension';
import { R_CONFIG_HOC } from './hoc';

export * from './store';
export * from './types';

export const INIT_CONFIG_ACTIONS = [getCheckoutConfig];

UiManager.config({
  uiHOCs: [...R_CONFIG_HOC],
  extensionConfigs: [],
});
