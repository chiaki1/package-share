import { graphqlRequest } from '@vjcspy/apollo';
import { Registry, RuntimeError } from '@vjcspy/chitility';
import { R_DEFAULT_VALUE } from '../values';
import _ from 'lodash';

export const graphqlFetch = (
  opts: { query: string; variables?: any },
  headers: any = {}
): Promise<any> => {
  const endpoint = Registry.getInstance().registry(
    R_DEFAULT_VALUE.GRAPHQL_DEFAULT_URL_KEY
  );
  if (!_.isString(endpoint)) {
    throw new RuntimeError('Please define PROXY_DEFAULT_END_POINT in registry');
  }

  const defaultHeaders = Registry.getInstance().registry(
    R_DEFAULT_VALUE.GRAPHQL_DEFAULT_HEADER_KEY
  );

  return graphqlRequest(
    opts,
    Object.assign(headers, defaultHeaders ?? {}),
    endpoint
  );
};
