import { EMPTY, Observable } from 'rxjs';
import { ActionsObservable, StateObservable } from 'redux-observable';
import { filter } from 'rxjs/operators';

export const createEffect = (
  effect: (
    action$: ActionsObservable<{ type: string }>,
    stateObservable: StateObservable<any>
  ) => Observable<{ type: string } | Observable<never>>
) => {
  return (
    action$: ActionsObservable<{ type: string }>,
    stateObservable: StateObservable<any>
  ): Observable<{ type: string }> =>
    effect(action$, stateObservable).pipe(filter((a) => a !== EMPTY)) as any;
};
