import { graphqlFetch } from './graphql-fetch';
import { Registry } from '@vjcspy/chitility';
import { R_DEFAULT_VALUE } from '../values';
import _ from 'lodash';

export const graphQLFetchWithAdditionHeaders = async (
  opts: { query: string; variables?: any },
  additional: string[],
  headers: any = {}
): Promise<any> => {
  const additionalHeaderResolved = Registry.getInstance().registry(
    R_DEFAULT_VALUE.GRAPHQL_RESOLVE_ADDITIONAL_HEADER_KEY
  );

  const customHeader: any = {};
  const asyncFuncs: any[] = [];
  if (typeof additionalHeaderResolved === 'object') {
    _.forEach(additional, (key: string) => {
      if (
        additionalHeaderResolved.hasOwnProperty(key) &&
        typeof additionalHeaderResolved[key] === 'function'
      ) {
        asyncFuncs.push(
          new Promise(async (resolve) => {
            const v = await additionalHeaderResolved[key]();
            if (_.isString(v)) {
              return resolve({
                key,
                value: v,
              });
            }

            return resolve(undefined);
          })
        );
      }
    });
  }
  const data = await Promise.all(asyncFuncs);

  _.forEach(data, (additionHeaderData: any) => {
    if (additionHeaderData) {
      if (additionHeaderData['key'] && additionHeaderData['value']) {
        const { key, value } = additionHeaderData;
        customHeader[key] = value;
      }
    }
  });

  return graphqlFetch(opts, Object.assign({}, headers, customHeader));
};
