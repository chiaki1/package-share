import _ from 'lodash';
import { proxyRequest, Registry, RuntimeError } from '@vjcspy/chitility';
import { R_DEFAULT_VALUE } from '../values';

export const proxyFetch = async (action: {
  type: string;
  payload?: any;
}): Promise<any> => {
  const proxyEndpoint = Registry.getInstance().registry(
    R_DEFAULT_VALUE.PROXY_DEFAULT_URL_KEY
  );
  if (!_.isString(proxyEndpoint)) {
    throw new RuntimeError('Please define PROXY_DEFAULT_END_POINT in registry');
  }

  return proxyRequest(proxyEndpoint, action);
};
